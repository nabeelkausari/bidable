var path = require('path'),
  express = require('express'),
  bodyParser = require('body-parser'),
  cors = require('cors');


// Create global app object
var app = express();

app.use(cors());

// Normal express config defaults
app.use(require('morgan')('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname + '/build')));

app.get('*', function(req, res) {
  res.sendFile(path.join(__dirname + '/build/index.html'));
});

const port = parseInt(process.env.PORT, 10) || 8083

app.listen(port, err => {
  if (err) return console.log(err);
  console.log(`server listening on port ${port}`)
})
