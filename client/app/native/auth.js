import { AsyncStorage } from "react-native";
import axios from 'axios';

import {HOST} from "../src/constants"
import {
  AUTH_BUYER,
  AUTH_BUYER_ERROR,
  AUTH_BUYER_SUCCESS,
  LOGOUT_BUYER,
} from "../src/containers/auth/buyer/types"

export const USER_KEY = "auth-demo-key";

export const isSignedIn = () => dispatch => {
  dispatch({ type: AUTH_BUYER })
  AsyncStorage.getItem(USER_KEY)
    .then(res => {
      if (res !== null) {
        getAuth(res, dispatch)
      } else {
        dispatch(logoutBuyer())
      }
    })
    .catch(() => dispatch({ type: AUTH_BUYER_ERROR }));
}

export const getMobileHeaders = async () => {
  let jwt = await AsyncStorage.getItem(USER_KEY)
  return { headers: { authorization: jwt }}
}

const getAuth = (jwt, dispatch) => {
  return axios.post(`${HOST}/api/getMe`, { jwt })
    .then(res => {
      if (!res.data.user) {
        dispatch(logoutBuyer())
      } else {
        dispatch({ type: AUTH_BUYER_SUCCESS, payload: res.data.user })
      }
    })
    .catch(() => dispatch({ type: AUTH_BUYER_ERROR }))
}

export const logoutBuyer = () => dispatch => {
  AsyncStorage.removeItem(USER_KEY)
    .then(() => dispatch({ type: LOGOUT_BUYER }))
}

export const loginBuyer = ({ email, password }) => dispatch => {
  dispatch({ type: AUTH_BUYER })
  axios.post(`${HOST}/api/buyer/login`, { email, password })
    .then(res => doLogin(dispatch, res))
    .catch(err => dispatch({
      type: AUTH_BUYER_ERROR,
      payload: err.response && err.response.data
    }))
}

const doLogin = (dispatch, res) => {
  dispatch({ type: AUTH_BUYER_SUCCESS, payload: res.data.user })
  AsyncStorage.setItem(USER_KEY, res.data.jwt)
}
