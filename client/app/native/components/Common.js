import React from 'react';
import { View, Image } from 'react-native';
import { Badge, Text } from 'react-native-elements';
const LOGO = require('../../src/assets/img/brand/logo2x.png');

export const LogoTitle = () => <View style={{ flex: 1, alignItems: 'center', justifyContent: "center" }}>
  <Image
    source={LOGO}
    style={{ width: 120, height: 25, margin: 10 }}
  />
</View>


export const BuyerStatus = ({ liveStatus }) => {
  switch (liveStatus) {
    case "upcoming":
      return <Badge containerStyle={{ backgroundColor: '#fecb00'}}>
        <Text style={{ fontWeight: 'bold', fontSize: 12 }}>Upcoming</Text>
      </Badge>
    case "past":
      return <Badge containerStyle={{ backgroundColor: 'lightgrey'}}>
        <Text style={{ fontWeight: 'bold', fontSize: 12 }}>Completed</Text>
      </Badge>
    case "live":
      return <Badge containerStyle={{ backgroundColor: '#4dbd73'}}>
        <Text style={{ fontWeight: 'bold', fontSize: 12, color: '#fff' }}>LIVE</Text>
      </Badge>
    default:
      return null
  }
}

export const renderAlert = (error) => {
  if (error) {
    return (
      <View style={{ backgroundColor: 'pink', marginTop:10 }}>
        <Text style={{ padding: 10, textAlign: 'center' }}>{error}</Text>
      </View>
    )
  }
}
