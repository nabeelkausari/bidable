import React from 'react';
import { Input } from 'react-native-elements'

export default props => {
  const {
    input: { onChange, ...restInput },
    meta: { touched, error },
    ...restProps
  } = props;
  return (
    <Input
      onChangeText={onChange}
      errorMessage={touched && error ? error : null}
      keyboardAppearance='light'
      autoFocus={false}
      autoCapitalize='none'
      autoCorrect={false}
      {...restInput}
      {...restProps}
    />
  )
}
