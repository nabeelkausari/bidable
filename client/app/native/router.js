import React from "react";
import { Platform, StatusBar } from "react-native";
import {
  StackNavigator,
  TabNavigator,
  createSwitchNavigator,
  createBottomTabNavigator, createMaterialTopTabNavigator
} from "react-navigation";

import Icon from 'react-native-vector-icons/FontAwesome';

import Forgot from "./screens/ForgotPassword";
import Reset from "./screens/ResetPassword";
import Auth from "./screens/Auth";
import Home from "./screens/Home";
import Profile from "./screens/Profile";
import AuctionDetails from "./screens/Auctions/Detail";
import LotDetails from "./screens/Lots/Lot";
import Reports from "./screens/Reports";
import MySales from "./screens/Reports/MySales";
import MyQuotations from "./screens/Reports/MyQuotations";
import MyBids from "./screens/Reports/MyBids";
import MyBid from "./screens/Reports/MyBidView";

const headerStyle = {
  marginTop: 0
};

const setTabs = props => {
  if (Platform.OS === "android") {
    return createMaterialTopTabNavigator(props, {
      tabBarOptions: {
        style: {
          paddingTop: 0,
          backgroundColor: '#0F214B'
        }
      }
    })
  } else {
    return createBottomTabNavigator(props, {
      tabBarOptions: {
        style: {
          paddingTop: 0,
        }
      }
    })
  }
}

export const SignedOut = StackNavigator({
  Auth: {
    screen: Auth,
    navigationOptions: {
      header: null
    }
  },
  Forgot: {
    screen: Forgot,
    navigationOptions: {
      title: "Forgot Password",
      headerStyle
    }
  },
  Reset: {
    screen: Reset,
    navigationOptions: {
      title: "Reset Password",
      headerStyle
    }
  }
});

const darkHeaderStyle = {
  headerStyle: {
    backgroundColor: '#0F214B'
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontWeight: 'bold',
  },
}


const Auctions = StackNavigator({
  Home: {
    screen: Home,
    navigationOptions: darkHeaderStyle
  },
  Detail: {
    screen: AuctionDetails,
    navigationOptions: darkHeaderStyle,
  },
  Lot: {
    screen: LotDetails,
    navigationOptions: darkHeaderStyle,
  }
});

Auctions.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  let swipeEnabled = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
    swipeEnabled = false;
  }

  return {
    tabBarVisible,
    swipeEnabled
  };
};

const ProfileScreen = StackNavigator({
  Profile: {
    screen: Profile,
    navigationOptions: {
      title: "Profile",
      ...darkHeaderStyle
    }
  }
})

const ReportScreen = StackNavigator({
  Reports: {
    screen: Reports,
    navigationOptions: {
      title: "Reports",
      ...darkHeaderStyle
    }
  },
  MyBids: {
    screen: MyBids,
    navigationOptions: {
      title: "My Bids",
      ...darkHeaderStyle
    }
  },
  MyBid: {
    screen: MyBid,
    navigationOptions: {
      title: "My Bid",
      ...darkHeaderStyle
    }
  },
  MyQuotations: {
    screen: MyQuotations,
    navigationOptions: {
      title: "My Quotations",
      ...darkHeaderStyle
    }
  },
  MySales: {
    screen: MySales,
    navigationOptions: {
      title: "My Sales",
      ...darkHeaderStyle
    }
  }
})

ReportScreen.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  let swipeEnabled = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
    swipeEnabled = false;
  }

  return {
    tabBarVisible,
    swipeEnabled
  };
};

export const SignedIn = setTabs(
  {
    Home: {
      screen: Auctions,
      navigationOptions: {
        tabBarLabel: "Home",
        tabBarIcon: ({ tintColor }) => (
          <Icon
            name='home'
            size={24}
            color={tintColor}
          />
        ),
        title: "Home",
      }
    },
    Reports: {
      screen: ReportScreen,
      navigationOptions: {
        tabBarLabel: "Reports",
        tabBarIcon: ({ tintColor }) => (
          <Icon
            name='file'
            size={24}
            color={tintColor}
          />
        )
      }
    },
    Profile: {
      screen: ProfileScreen,
      navigationOptions: {
        tabBarLabel: "Profile",
        tabBarIcon: ({ tintColor }) => (
          <Icon
            name='user'
            size={24}
            color={tintColor}
          />
        )
      }
    }
  }
);

export const createRootNavigator = (signedIn = false) => {
  return createSwitchNavigator(
    {
      SignedIn: SignedIn,
      SignedOut: SignedOut
    },
    {
      initialRouteName: signedIn ? "SignedIn" : "SignedOut"
    }
  );
};
