import React from 'react';
import { View } from 'react-native';
import { Field, reduxForm } from 'redux-form';
import { Button } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome';

import {styles} from "../screens/styles/Auth"
import renderInput from "../components/FormInput";
import { emailRegEx } from "../../src/constants";

const validate = formProps => {
  const errors = {};

  if (!formProps.email) errors.email = 'Please enter your email address'
  if (formProps.email && !emailRegEx.test(formProps.email)) errors.email = 'Please enter a valid email'
  return errors;
}

const ForgotForm = props => {
  const { isLoading, handleSubmit, handleFormSubmit, renderAlert } = props;
  return (
    <View style={styles.formContainer}>
      <Field
        component={renderInput}
        name="email"
        leftIcon={
          <Icon
            name='envelope-o'
            color='rgba(0, 0, 0, 0.38)'
            size={25}
            style={{backgroundColor: 'transparent'}}
          />
        }
        keyboardType='email-address'
        returnKeyType='next'
        inputStyle={{marginLeft: 10}}
        placeholder={'Email'}
        blurOnSubmit={true}
        containerStyle={{marginTop: 16, borderBottomColor: 'rgba(0, 0, 0, 0.38)'}}
      />
      {renderAlert()}
      <Button
        buttonStyle={styles.loginButton}
        containerStyle={{marginTop: 32, flex: 0}}
        activeOpacity={0.8}
        title={'Send OTP'}
        onPress={handleSubmit(handleFormSubmit)}
        titleStyle={styles.loginTextButton}
        loading={isLoading}
        disabled={isLoading}
      />
    </View>
  )
}

export default reduxForm({form: 'forgotPasswordBuyer', validate })(ForgotForm)
