import React from 'react';
import { View } from 'react-native';
import { Field, reduxForm } from 'redux-form';
import { Button } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome';
import SimpleIcon from 'react-native-vector-icons/SimpleLineIcons';

import {styles} from "../screens/styles/Auth"
import renderInput from "../components/FormInput";
import { emailRegEx } from "../../src/constants";

const validate = formProps => {
  const errors = {};

  if (!formProps.email) errors.email = 'Please enter an email'
  if (!formProps.password) errors.password = 'Please enter a password'
  if (formProps.email && !emailRegEx.test(formProps.email)) errors.email = 'Please enter a valid email'
  return errors;
}

const LoginForm = props => {
  const { isLoading, handleSubmit, handleFormSubmit, renderAlert } = props;
  return (
    <View style={styles.formContainer}>
      <Field
        component={renderInput}
        name="email"
        leftIcon={
          <Icon
            name='envelope-o'
            color='rgba(0, 0, 0, 0.38)'
            size={25}
            style={{backgroundColor: 'transparent'}}
          />
        }
        keyboardType='email-address'
        returnKeyType='next'
        inputStyle={{marginLeft: 10}}
        placeholder={'Email'}
        containerStyle={{borderBottomColor: 'rgba(0, 0, 0, 0.38)'}}
      />
      <Field
        component={renderInput}
        name="password"
        leftIcon={
          <SimpleIcon
            name='lock'
            color='rgba(0, 0, 0, 0.38)'
            size={25}
            style={{backgroundColor: 'transparent'}}
          />
        }
        secureTextEntry={true}
        returnKeyType='done'
        inputStyle={{marginLeft: 10}}
        placeholder={'Password'}
        blurOnSubmit={true}
        containerStyle={{marginTop: 16, borderBottomColor: 'rgba(0, 0, 0, 0.38)'}}
      />
      {renderAlert()}
      <Button
        buttonStyle={styles.loginButton}
        containerStyle={{marginTop: 32, flex: 0}}
        activeOpacity={0.8}
        title={'LOGIN'}
        onPress={handleSubmit(handleFormSubmit)}
        titleStyle={styles.loginTextButton}
        loading={isLoading}
        disabled={isLoading}
      />
    </View>
  )
}

export default reduxForm({form: 'loginBuyer', validate })(LoginForm)
