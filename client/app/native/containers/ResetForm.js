import React from 'react';
import { View } from 'react-native';
import { Field, reduxForm } from 'redux-form';
import { Button } from 'react-native-elements'
import SimpleIcon from 'react-native-vector-icons/SimpleLineIcons';

import {styles} from "../screens/styles/Auth"
import renderInput from "../components/FormInput";

const validate = formProps => {
  const errors = {};

  if (!formProps.otp) errors.otp = 'Please enter the OTP'
  if (!formProps.password) errors.password = 'Please enter a password'
  if (!formProps.confirmPassword) errors.confirmPassword = 'Please enter a password confirmation'

  if (formProps.password && formProps.password.length < 6) errors.password = 'Password should be at least 6 digit'
  if (formProps.password !== formProps.confirmPassword) {
    errors.password = 'Passwords must match'
  }

  return errors;
}

const ResetForm = props => {
  const { isLoading, handleSubmit, handleFormSubmit, renderAlert } = props;
  return (
    <View style={styles.formContainer}>
      <Field
        component={renderInput}
        name="otp"
        leftIcon={
          <SimpleIcon
            name='key'
            color='rgba(0, 0, 0, 0.38)'
            size={25}
            style={{backgroundColor: 'transparent'}}
          />
        }
        keyboardType='default'
        returnKeyType='next'
        inputStyle={{marginLeft: 10}}
        placeholder={'OTP'}
        containerStyle={{marginTop: 16, borderBottomColor: 'rgba(0, 0, 0, 0.38)'}}
      />
      <Field
        component={renderInput}
        name="password"
        leftIcon={
          <SimpleIcon
            name='lock'
            color='rgba(0, 0, 0, 0.38)'
            size={25}
            style={{backgroundColor: 'transparent'}}
          />
        }
        keyboardType='default'
        secureTextEntry={true}
        returnKeyType='next'
        inputStyle={{marginLeft: 10}}
        placeholder={'New Password'}
        containerStyle={{marginTop: 16, borderBottomColor: 'rgba(0, 0, 0, 0.38)'}}
      />
      <Field
        component={renderInput}
        name="confirmPassword"
        leftIcon={
          <SimpleIcon
            name='lock'
            color='rgba(0, 0, 0, 0.38)'
            size={25}
            style={{backgroundColor: 'transparent'}}
          />
        }
        keyboardType='default'
        secureTextEntry={true}
        returnKeyType='done'
        inputStyle={{marginLeft: 10}}
        placeholder={'Confirm New Password'}
        blurOnSubmit={true}
        containerStyle={{marginTop: 16, borderBottomColor: 'rgba(0, 0, 0, 0.38)'}}
      />
      {renderAlert()}
      <Button
        buttonStyle={styles.loginButton}
        containerStyle={{marginTop: 32, flex: 0}}
        activeOpacity={0.8}
        title={'Reset Password'}
        onPress={handleSubmit(handleFormSubmit)}
        titleStyle={styles.loginTextButton}
        loading={isLoading}
        disabled={isLoading}
      />
    </View>
  )
}

export default reduxForm({form: 'resetPasswordBuyer', validate })(ResetForm)
