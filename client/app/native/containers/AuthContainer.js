import { connect } from 'react-redux';

import { loginBuyer, logoutBuyer } from "../auth";
import { registerBuyer } from "../../src/containers/auth/buyer/actions";
import { forgotPasswordOTP, resetPasswordOTP } from "../../src/containers/auth/common/actions";

function mapStateToProps(state) {
  return {
    ...state.buyerAuth,
    ...state.auth
  }
}

export default connect(mapStateToProps, {
  loginBuyer, logoutBuyer, registerBuyer,
  forgotPasswordOTP, resetPasswordOTP
})
