import React from 'react';
import { View } from 'react-native';
import { Field, reduxForm } from 'redux-form';
import { Button } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome';
import SimpleIcon from 'react-native-vector-icons/SimpleLineIcons';

import {styles} from "../screens/styles/Auth"
import renderInput from "../components/FormInput";
import { validate } from "../../src/containers/auth/validations"

const RegisterForm = props => {
  const { isLoading, handleSubmit, handleFormSubmit, renderAlert } = props;
  return (
    <View style={styles.formContainer}>
      <Field
        component={renderInput}
        name="email"
        leftIcon={
          <Icon
            name='envelope-o'
            color='rgba(0, 0, 0, 0.38)'
            size={25}
            style={{backgroundColor: 'transparent'}}
          />
        }
        keyboardType='email-address'
        returnKeyType='next'
        inputStyle={{marginLeft: 10}}
        placeholder={'Email'}
        containerStyle={{marginTop: 16, borderBottomColor: 'rgba(0, 0, 0, 0.38)'}}
      />
      <Field
        component={renderInput}
        name="mobile"
        leftIcon={
          <Icon
            name='phone'
            color='rgba(0, 0, 0, 0.38)'
            size={25}
            style={{backgroundColor: 'transparent'}}
          />
        }
        keyboardType='phone-pad'
        returnKeyType='next'
        inputStyle={{marginLeft: 10}}
        placeholder={'Mobile No'}
        containerStyle={{marginTop: 16, borderBottomColor: 'rgba(0, 0, 0, 0.38)'}}
      />
      <Field
        component={renderInput}
        name="password"
        leftIcon={
          <SimpleIcon
            name='lock'
            color='rgba(0, 0, 0, 0.38)'
            size={25}
            style={{backgroundColor: 'transparent'}}
          />
        }
        secureTextEntry={true}
        returnKeyType='next'
        inputStyle={{marginLeft: 10}}
        placeholder={'Password'}
        containerStyle={{marginTop: 16, borderBottomColor: 'rgba(0, 0, 0, 0.38)'}}
      />
      <Field
        component={renderInput}
        name="confirmPassword"
        leftIcon={
          <SimpleIcon
            name='lock'
            color='rgba(0, 0, 0, 0.38)'
            size={25}
            style={{backgroundColor: 'transparent'}}
          />
        }
        secureTextEntry={true}
        returnKeyType='done'
        inputStyle={{marginLeft: 10}}
        placeholder={'Confirm Password'}
        blurOnSubmit={true}
        containerStyle={{marginTop: 16, borderBottomColor: 'rgba(0, 0, 0, 0.38)'}}
      />
      {renderAlert()}
      <Button
        buttonStyle={styles.loginButton}
        containerStyle={{marginTop: 32, flex: 0}}
        activeOpacity={0.8}
        title={'Sign Up'}
        onPress={handleSubmit(handleFormSubmit)}
        titleStyle={styles.loginTextButton}
        loading={isLoading}
        disabled={isLoading}
      />
    </View>
  )
}

export default reduxForm({form: 'registerBuyer', validate })(RegisterForm)
