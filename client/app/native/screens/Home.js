import React, { Component } from 'react';
import { View, FlatList, Image, StatusBar } from 'react-native';
import { Card, Button, Text } from "react-native-elements";
import { join, truncate } from 'lodash';

import {getMobileHeaders} from "../auth"
import HomeContainer from '../../src/containers/auctions'
import {getAuctionLiveStatus, readableTime, shortDate} from "../../src/constants"
import {BuyerStatus, LogoTitle} from "../components/Common"


class Home extends Component {
  static navigationOptions = {
    // headerTitle instead of title
    headerTitle: <LogoTitle />,
  };

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData = () => {
    return getMobileHeaders()
      .then(headers => this.props.getAuctions(headers))
  }

  _onRefresh() {
    this.setState({refreshing: true});
    this.fetchData().then(() => {
      this.setState({refreshing: false});
    });
  }

  _keyExtractor = item => item.auctionId;

  render() {
    const { auctions } = this.props;
    if (!auctions) return <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text style={{ fontSize: 16, fontWeight: 'bold', marginBottom: 10 }}>No Auctions Found!</Text>
      <Button
        title="Reload Again"
        onPress={this._onRefresh.bind(this)}
      />
    </View>;

    return (
      <View style={{ flex: 1 }}>
        <StatusBar
          barStyle="light-content"
          backgroundColor="#0F214B"
          animated={true}
        />
        <FlatList
          data={auctions}
          keyExtractor={this._keyExtractor}
          renderItem={props => AuctionCard({ ...props, ...this.props})}
          refreshing={this.state.refreshing}
          onRefresh={this._onRefresh.bind(this)}
        />
      </View>
    )
  }
}

const AuctionCard = ({ item, ...props }) => {
  const {
    _id, company,
    inspectionDetails: { from, to, timings: [time]},
    lots, auctionId, liveStatus,
    auctionDate, auctionTime
  } = item;
  return <Card title={auctionId}>
    <View style={{ position: 'absolute', top: 2, right: 0 }}>{BuyerStatus({ liveStatus })}</View>
    <Text style={{ marginBottom: 5 }}>
      <Text style={{ fontWeight: 'bold' }}>Company: </Text>{company}
    </Text>
    <Text style={{ marginBottom: 5 }}>
      <Text style={{ fontWeight: 'bold' }}>Inspection Date: </Text>{shortDate(from)} - {shortDate(to)}
    </Text>
    <Text style={{ marginBottom: 5 }}>
      <Text style={{ fontWeight: 'bold' }}>Inspection Timings: </Text>({readableTime(time.from)} - {readableTime(time.to)})
    </Text>
    <Text style={{ marginBottom: 5 }}>
      <Text style={{ fontWeight: 'bold' }}>Auction Date: </Text>{shortDate(auctionDate)} ({readableTime(auctionTime.start)} - {readableTime(auctionTime.end)})
    </Text>
    <Text style={{ marginBottom: 10 }}>
      <Text style={{ fontWeight: 'bold' }}>Lots: </Text>{truncate(join(lots.map(lot => lot.product.name), ", "))}
    </Text>
    <Button
      backgroundColor="#03A9F4"
      title="VIEW NOW"
      onPress={() => props.navigation.navigate('Detail', {auctionId, _id})}
    />
  </Card>
}

export default HomeContainer(Home)
