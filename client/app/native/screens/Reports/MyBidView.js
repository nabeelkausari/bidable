import React, { Component } from 'react';
import { FlatList, ScrollView } from 'react-native'
import { ListItem, Divider, Text, Card } from 'react-native-elements'

import {price, readableTime, shortDate, regularTime} from "../../../src/constants"

class MyBid extends Component {

  _keyExtractor = item => item._id;

  render() {
    const {auction: {auctionId}, lotNo, auctionDate, auctionTime, bids } = this.props.navigation.state.params;
    return (
      <ScrollView style={{ flex: 1 }}>
        <Divider style={{ backgroundColor: 'transparent', height: 20 }} />
        <Card>
          <Text style={{ marginBottom: 5 }}>
            <Text style={{ fontWeight: 'bold' }}>Auction ID: </Text>{auctionId}
          </Text>
          <Text style={{ marginBottom: 5 }}>
            <Text style={{ fontWeight: 'bold' }}>Lot No: </Text>{lotNo}
          </Text>
          <Text style={{ marginBottom: 5 }}>
            <Text style={{ fontWeight: 'bold' }}>Auction Date: </Text>{shortDate(auctionDate)} ({readableTime(auctionTime.start)} - {readableTime(auctionTime.end)})
          </Text>
        </Card>

        <Card title="All Bids">
          <FlatList
            data={bids}
            keyExtractor={this._keyExtractor}
            renderItem={({ item }) => {
              return <ListItem
                title={`${price(item.price)} (${regularTime(item.createdAt)})`}
                bottomDivider
                badge={{value: "You"}}
              />
            }}
          />
        </Card>
      </ScrollView>
    )
  }
}

export default MyBid
