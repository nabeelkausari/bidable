import React, { Component } from 'react';
import { ScrollView } from 'react-native'
import { ListItem, Divider, Text } from 'react-native-elements'

export default class MySales extends Component {
  render() {
    return (
      <ScrollView style={{ flex: 1 }}>
        <Divider style={{ backgroundColor: 'transparent', height: 20 }} />
        <Text>My Sales</Text>
      </ScrollView>
    )
  }
}
