import React, { Component } from 'react';
import { FlatList, View } from 'react-native'
import { ListItem, Divider, Text, Button } from 'react-native-elements'
import {getMobileHeaders} from "../../auth"

import LotsContainer from '../../../src/containers/lots'
import {getDateTime, price} from "../../../src/constants"


class MyBids extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };
  }
  componentDidMount() {
    this.fetchData();
  }

  fetchData = () => {
    return getMobileHeaders()
      .then(headers => this.props.getMyBids(headers))
  }

  _onRefresh() {
    this.setState({refreshing: true});
    this.fetchData().then(() => {
      this.setState({refreshing: false});
    });
  }

  _keyExtractor = item => item.auction.auctionId + item.lotNo;


  render() {
    const { myBids } = this.props;
    if (!myBids) return <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text style={{ fontSize: 16, fontWeight: 'bold', marginBottom: 10 }}>No Bids Found!</Text>
      <Button
        title="Reload Again"
        onPress={this._onRefresh.bind(this)}
      />
    </View>;

    return (
      <View style={{ flex: 1 }}>
        <Divider style={{ backgroundColor: 'transparent', height: 20 }} />
        <FlatList
          data={myBids}
          keyExtractor={this._keyExtractor}
          renderItem={({ item }) => {
            return <ListItem
              title={`${item.auction.auctionId} #${item.lotNo}`}
              subtitle={"Your Highest Bid: " + price(item.bids[0].price)}
              chevron
              bottomDivider
              onPress={() => this.props.navigation.navigate('MyBid', item)}
              badge={getWinningStatus(item)}
            />
          }}
          refreshing={this.state.refreshing}
          onRefresh={this._onRefresh.bind(this)}
        />
      </View>
    )
  }
}

const getWinningStatus = lot => {
  let highestBidder = lot.bids[0]._id === lot.highestBid.id
  let value;
  if (lot.liveStatus !== "past") {
    value = "-"
  } else {
    value = highestBidder ? "Won" : "Lost"
  }
  return {
    value,
    containerStyle: {
      backgroundColor: value === "Won" ? "green" : "grey"
    }
  }
}

export default LotsContainer(MyBids)
