import React, { Component } from 'react';
import { ScrollView } from 'react-native'
import { ListItem, Divider } from 'react-native-elements'

import { LogoTitle  } from "../../components/Common"

export default class Reports extends Component {
  static navigationOptions = {
    // headerTitle instead of title
    headerTitle: <LogoTitle />,
  };

  state = {
    list: [
      {
        title: 'My Bids',
        url: 'MyBids'
      },
      {
        title: 'My Quotations',
        url: 'MyQuotations'
      },
      {
        title: 'My Sales',
        url: 'MySales'
      },
    ]
  }

  render() {
    return (
      <ScrollView style={{ flex: 1 }}>
        <Divider style={{ backgroundColor: 'transparent', height: 20 }} />
        {
          this.state.list.map((item, i) => (
            <ListItem
              key={i}
              title={item.title}
              chevron
              bottomDivider
              onPress={() => this.props.navigation.navigate(item.url)}
            />
          ))
        }
      </ScrollView>
    )
  }
}
