import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  View,
  Text,
  ImageBackground,
  Dimensions,
  LayoutAnimation,
  UIManager,
  KeyboardAvoidingView,
  Image,
  Alert,
  ScrollView
} from 'react-native';

import ResetForm from '../containers/ResetForm';
import AuthContainer from '../containers/AuthContainer';
import { styles } from "./styles/Auth";

const LOGO = require('../../src/assets/img/brand/logo2x.png');

// Enable LayoutAnimation on Android
UIManager.setLayoutAnimationEnabledExperimental
&& UIManager.setLayoutAnimationEnabledExperimental(true);

const TabSelector = ({ selected }) => {
  return (
    <View style={styles.selectorContainer}>
      <View style={selected && styles.selected}/>
    </View>
  );
};

TabSelector.propTypes = {
  selected: PropTypes.bool.isRequired,
};

class ResetPassword extends Component {
  state= {
    loading: false,
    showError: false
  }

  handleResetPassword = ({ otp, password }) => {
    this.setState({ loading: true })
    this.props.resetPasswordOTP({
      otp, password,
      redirect: this.confirmRedirection.bind(this)
    });
  }

  confirmRedirection = () => {
    Alert.alert(
      'Password Changed',
      'Your password has been changed successfully, kindly login with your new password',
      [
        // {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'Done', onPress: () => this.props.navigation.navigate("Auth")},
      ],
      { cancelable: false }
    )
  }

  renderAlert = () => {
    if (this.props.resetPassOTPErr) {
      return (
        <View style={{ backgroundColor: 'red', margin: 15, padding: 10, borderRadius:10, marginBottom: 0 }}>
          <Text style={{ color: '#fff', textAlign: 'center' }}>Error! {this.props.resetPassOTPErr}</Text>
        </View>
      )
    }
  }

  render() {
    const {isLoading} = this.state;
    return (
      <ScrollView style={styles.container}>
        <View style={styles.bgImage}>
          <View style={styles.titleContainer}>
            <View style={styles.logoContainer}>
              <Image style={styles.logo} source={LOGO}/>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Text style={[styles.categoryText, styles.selectedCategoryText]}>Forgot Password?</Text>
            </View>
          </View>
          <KeyboardAvoidingView contentContainerStyle={styles.resetContainer} behavior='position'>
            <ResetForm
              handleFormSubmit={this.handleResetPassword}
              renderAlert={this.renderAlert.bind(this)}
              isLoading={isLoading}
            />
          </KeyboardAvoidingView>
        </View>
      </ScrollView>
    );
  }
}

export default AuthContainer(ResetPassword)
