import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Text, View, TextInput } from "react-native";
import { Card, Button, Badge, ListItem, Icon } from "react-native-elements";
import {Decimal} from 'decimal.js';

import { reduxForm, Field } from 'redux-form';
import { setMyBid, clearMyBid } from "../../../src/containers/lots/actions"
import {price} from "../../../src/constants"
import {styles} from "../../screens/styles/Auth"

const validate = formProps => {
  const errors = {};
  if (!formProps.myBid) errors.myBid = 'Please enter the bid price'
  return errors;
}

class BidFormContainer extends Component {
  state = {
    nextPrice: 0
  }

  setNextPrice = () => {
    const {highestBid, minimumIncrement} = this.props.lot;
    const hb = new Decimal(highestBid.price || 0);
    let nextPrice = hb.add(minimumIncrement);
    this.setState({ nextPrice })
    this.props.setMyBid(nextPrice)
  }

  setStartingPrice = () => {
    const {startingPrice} = this.props.lot;
    let nextPrice = Number(startingPrice);
    this.setState({ nextPrice })
    this.props.setMyBid(nextPrice)
  }

  clearNextPrice = () => {
    this.setState({ nextPrice: 0 })
    this.props.clearMyBid()
  }

  componentWillUnmount() {
    this.props.clearMyBid()
  }

  doSubmit = props => {
    this.props.handleFormSubmit(props)
    this.clearNextPrice()
  }

  render() {
    return <BidFormBounded
      nextPrice={this.state.nextPrice}
      {...this.props}
      doSubmit={this.doSubmit.bind(this)}
      clearNextPrice={this.clearNextPrice.bind(this)}
      setNextPrice={this.setNextPrice.bind(this)}
      setStartingPrice={this.setStartingPrice.bind(this)}
    />
  }
}

const BidForm = props => {
  const { nextPrice, lot } = props;
  const {minimumIncrement, startingPrice} = lot
  return (
    <View>
      <View style={{ marginTop: 15 }}>
        <View>
          <Text style={{ fontWeight: 'bold', fontSize: 18, color: 'gray', textAlign: 'center' }}>Your Bid Price</Text>
          <View style={{ flexDirection: 'row', marginTop: 20 }}>
            <View>
              {
                props.lot.highestBid  && props.lot.highestBid.price
                  ? <Button
                    onPress={props.setNextPrice}
                    title={`+ ${price(minimumIncrement)}`}
                    titleStyle={{ fontWeight: 'bold', fontSize: 14 }}
                    buttonStyle={{
                      padding: 5,
                      borderRadius: 5,
                      borderTopRightRadius: 0,
                      borderBottomRightRadius: 0
                    }}
                  />
                  : <Button
                    onPress={props.setStartingPrice}
                    title={price(startingPrice)}
                    titleStyle={{ fontWeight: 'bold', fontSize: 14 }}
                    buttonStyle={{
                      padding: 6,
                      borderRadius: 5,
                      borderTopRightRadius: 0,
                      borderBottomRightRadius: 0
                    }}
                  />
              }
            </View>
            <TextInput style={{ fontWeight: 'bold', backgroundColor: '#eee', flex: 1, paddingLeft: 10 }} editable={false} value={nextPrice && price(nextPrice)}/>
            <Field label="Your Bid" name="myBid" component={hiddenInput} type="number" placeholder="Enter your bid price..." />
            <View>
              <Button
                onPress={() => {
                  props.clearNextPrice();
                  props.reset();
                }}
                title="Clear"
                titleStyle={{ fontWeight: 'bold', fontSize: 14, color: '#000' }}
                buttonStyle={{
                  padding: 5,
                  backgroundColor: 'orange',
                  borderRadius: 5,
                  borderTopLeftRadius: 0,
                  borderBottomLeftRadius: 0
                }}
              />
            </View>
          </View>
        </View>
        <View>
          <Button
            containerStyle={{marginTop: 20, flex: 0}}
            title={'PLACE YOUR BID'}
            onPress={props.handleSubmit(props.doSubmit)}
            titleStyle={styles.loginTextButton}
            buttonStyle={{ padding: 5 }}
          />
        </View>
      </View>
      {props.renderAlert()}
    </View>
  )
}

const hiddenInput = props => {
  const {
    input: { onChange, ...restInput },
    meta: { touched, error },
    ...restProps
  } = props;
  return (
    <TextInput
      style={{ display: 'none' }}
      editable={false}
      onChangeText={onChange}
      autoFocus={false}
      autoCapitalize='none'
      autoCorrect={false}
      {...restInput}
      {...restProps}
    />
  )
}

const BidFormBounded = reduxForm({
  form: 'mobileBidForm',
  validate,
  enableReinitialize: true
})(BidForm)

export default connect(state => ({
  initialValues: state.lots.bidForm
}), { setMyBid, clearMyBid })(BidFormContainer)
