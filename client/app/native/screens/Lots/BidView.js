import React, { Component } from "react";
import { join, truncate } from 'lodash';
import io from 'socket.io-client';
import moment from 'moment';
import { Table, Row, Rows } from 'react-native-table-component';
import {CountDownText} from 'react-native-countdown-timer-text';

import { ScrollView, Text, Linking, View, RefreshControl, FlatList, ListView } from "react-native";
import { Card, Button, Badge, ListItem } from "react-native-elements";

import { getMobileHeaders } from "../../auth";
import {
  price,
  HOST,
  hasPaidEMD,
  regularTime,
  getDateTime
} from "../../../src/constants"
import {BuyerStatus, renderAlert} from "../../components/Common"
import BidForm from './BidForm';
import QuotationForm from './QuotationForm';

class Lot extends Component {
  static navigationOptions = ({ navigation }) => {
    const { auction, lotNo } = navigation.state.params;
    return {
      title: `${auction.auctionId} #${lotNo}`,
    }
  };

  constructor(props) {
    super(props);
    let connectionOptions =  {
      "transports" : ["websocket"]
    };
    this.state = { showTimer: true }
    this.socket = io(HOST, connectionOptions);
    this.socket.on('bidCreated', data => {
      if (this.props.selectedLot && data.lotId === this.props.selectedLot._id) {
        // this.notify(data)
        this.props.updateSelectedLot(data)
      }
    });
  }

  handleSubmitBid = ({ myBid }) => {
    getMobileHeaders()
      .then(headers => this.props.createBid({
        myBid, lotId: this.props.selectedLot._id
      }, headers))
  }

  handleQuoteSubmit = ({ quotePrice }) => {
    getMobileHeaders()
      .then(headers => this.props.createQuotation({
        quotePrice,
        lotId: this.props.selectedLot._id
      }, headers))
  }

  componentDidMount() {
    this.fetchData();
  }

  componentWillUnmount() {
    this.socket.close()
    this.props.removeSelectedLot()
  }

  userWinner = () => {
    const { lotBids, user } = this.props;
    return (lotBids && lotBids.length && lotBids[0].bidder === user._id)
  }

  showBidForm = () => {
    const { selectedLot : {liveStatus, emdList}, user } = this.props;
    if (!user) return;
    const isLive = liveStatus === "live";
    const [emd] = emdList.filter(e => e.user === user._id);
    return isLive && emd && emd.status === "paid";
  }

  showQuotationForm = () => {
    let emdPaid = this.showBidForm();
    const { highestBid } = this.props.selectedLot;
    return emdPaid && (!highestBid || !highestBid.price)
  }

  fetchData = () => {
    const {lotNo, auction} = this.props.navigation.state.params
    return getMobileHeaders()
      .then(headers => this.props.getLot({ lotNo, auctionId: auction._id }, headers))
  }

  render() {
    const { selectedLot, lotBids, user, myQuotation } = this.props;
    if (!selectedLot) return null;

    const { auction: { auctionId }, liveStatus, lotNo, highestBid, emdAmount, secondsLeft } = selectedLot;
    const isLive = liveStatus === "live";
    const isUpcoming = liveStatus === "upcoming";
    const [emd] = selectedLot.emdList.filter(e => e.user === user._id);
    return (
      <View
        style={{ flex: 1 }}
      >
        <Card title={`LOT NO: ${lotNo}`}>
          <View style={{ position: 'absolute', top: 2, right: 0 }}>{BuyerStatus({ liveStatus })}</View>
          {
            this.showBidForm()
              ? (
                <View style={styles.timerContainer}>
                  <CountDownText
                    style={styles.timer}
                    countType='seconds'
                    auto={true}
                    afterEnd={this.fetchData.bind(this)}
                    timeLeft={secondsLeft}
                    step={-1}
                    intervalText={(sec) => timerFormat(sec)}
                  />
                </View>
              )
              : null
          }
          {
            isUpcoming
              ? <UpcomingLotBid emdAmount={emdAmount} emd={emd} {...this.props} />
              : <LiveLotBid highestBid={highestBid} winner={this.userWinner} />
          }
          {
            this.showBidForm()
              ? <BidForm
                lot={selectedLot}
                handleFormSubmit={this.handleSubmitBid}
                renderAlert={() => renderAlert(this.props.createBidError)}
              />
              : null
          }
          {!isLive && this.userWinner() ? <View>
              <Text style={{ textAlign: 'center', marginTop: 10, fontSize: 24 }}>Congratulations! You are the winner.</Text>
            </View>
            : null}
        </Card>
        {
          this.showQuotationForm() && <Card title="Quotation">
            <QuotationForm
              quotation={myQuotation}
              handleFormSubmit={this.handleQuoteSubmit}
              renderAlert={() => renderAlert(this.props.createQuotationError)}
            />
          </Card>
        }
        {
          lotBids && lotBids.length && user
            ? <BidList bids={lotBids} user={user} />
            : null
        }
      </View>
    )
  }
}

const UpcomingLotBid = ({ emdAmount, selectedLot, user, emd }) => (
  <View style={styles.highestBid}>
    <Text>EMD Amount: {price(emdAmount)}</Text>
    {!hasPaidEMD(selectedLot, user) ? (
      <Button
        backgroundColor="#03A9F4"
        title="Pay EMD"
        onPress={() => this.goToEMDPage()}
      />
    ) : null}
  </View>
)

// (
//   emd.approval === "success"
//     ? <View><Text>EMD amount</Text> <Badge value={emd.status}/></View>
//     : <View><Text>We have received your EMD form and your request is</Text> <Badge value={emd.approval}/></View>
// )

const LiveLotBid = ({ highestBid, winner }) => (
  <View style={[styles.highestBid, winner() && {backgroundColor: '#4dbd73'}]}>
    <Text style={[styles.highestBidText, winner() && {color: '#fff'}]}>Highest Bid</Text>
    <View>{highestBid && highestBid.price
      ?<Text style={[styles.highestBidPrice, winner() && {color: '#fff'}]}>{price(highestBid.price)}</Text>
      :<Text style={{ ...styles.highestBidPrice, color: "#4dbd73" }}>No Bids Yet</Text>}
    </View>
  </View>
)

const BidList = ({ bids, user }) => {
  const tableData = bids.map(bid => ([
    <Text style={{fontWeight: 'bold', paddingLeft: 10 }}>{price(bid.price)}</Text>,
    regularTime(bid.createdAt),
    bid.bidder === user._id
      ? <View style={{borderRadius: 4, backgroundColor: "#4dbd73"}}><Text style={styles.yourBid}>You</Text></View>
      : <Text style={{textAlign: 'center'}}>Others</Text>
  ]))
  return (
    <Card>
      <Table borderStyle={{borderWidth: 0, borderColor: '#c8e1ff'}}>
        <Row data={['Bid Price', 'Bidding Time', 'Bidder']} style={{ padding: 5, height: 40, backgroundColor: '#f1f8ff' }} textStyle={{ fontWeight: 'bold', margin: 6 }}/>
        <Rows data={tableData} textStyle={{ margin: 6 }}/>
      </Table>
    </Card>
  )
}



const timerFormat = (seconds) => {
  let sec = (seconds % 60);
  let min = (Math.floor(seconds / 60));
  return <Text style={{ fontWeight: 'bold' }}>{min}m : {sec}s</Text>
}

const styles = {
  yourBid: {
    paddingVertical: 2,
    paddingHorizontal: 10,
    color: "#fff",
    fontWeight: "bold",
    display: 'flex',
    textAlign: 'center'
  },
  highestBid: {
    backgroundColor: "#f1f3f5",
    paddingHorizontal: 20,
    paddingTop: 10,
    paddingBottom: 0,
    borderRadius: 5,
  },
  highestBidText: {
    textAlign: 'center',
    fontSize: 18
  },
  highestBidPrice: {
    textAlign: 'center',
    fontSize: 24,
    padding: 10,
    fontWeight: 'bold'
  },
  timerContainer: {
    alignItems: 'center',
    marginBottom: 15
  },
  timer: {
    paddingVertical: 4,
    paddingHorizontal: 10,
    backgroundColor: "#4dbd73",
    color: "#fff",
    borderRadius: 10,
    fontWeight: "bold",
    fontSize: 20
  }
}

export default Lot
