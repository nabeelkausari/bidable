import React, { Component } from "react";
import { join, truncate } from 'lodash';

import { ScrollView, Text, Linking, View, RefreshControl, FlatList, ListItem, ListView } from "react-native";
import { Card, Button } from "react-native-elements";

import LotContainer from '../../../src/containers/lots/index'
import { getMobileHeaders } from "../../auth";
import {shortDate, readableTime, price} from "../../../src/constants"
import BidView from './BidView';

class Lot extends Component {
  static navigationOptions = ({ navigation }) => {
    const { auction, lotNo } = navigation.state.params;
    return {
      title: `${auction.auctionId} #${lotNo}`,
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };
  }

  componentDidMount() {
    this.fetchData();
  }

  componentDidUpdate(prevProps) {
    const { lotLoading, selectedLot, getLotBids, getMyQuotation } = this.props;
    let lotTriggered = prevProps.lotLoading !== lotLoading;
    let lotLoaded = lotLoading === false;
    if (lotTriggered && lotLoaded) {
      getMobileHeaders().then(headers => getLotBids(selectedLot._id, headers))
      getMobileHeaders().then(headers => getMyQuotation(selectedLot._id, headers))
      this.setState({ lotLoading: false })
    }
  }

  fetchData = () => {
    const {lotNo, auction} = this.props.navigation.state.params
    return getMobileHeaders()
      .then(headers => this.props.getLot({ lotNo, auctionId: auction._id }, headers))
  }

  _onRefresh() {
    this.setState({refreshing: true});
    this.fetchData().then(() => {
      this.setState({refreshing: false});
    });
  }
  render() {
    const { selectedLot } = this.props;
    if (!selectedLot) return null;
    return (
      <ScrollView
        style={{ flex: 1 }}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh.bind(this)}
          />
        }
      >
        <BidView {...this.props} />
        <LotCard {...this.props} lot={selectedLot} />

      </ScrollView>
    )
  }
}

const LotCard = ({ lot }) => {
  const {
    _id, lotNo, product: { name }, auction,
    startingPrice, bidPer,
    quantity, unit, emdAmount,
    auctionDate, auctionTime
  } = lot;
  return <Card>
    <Text style={{ marginBottom: 5 }}>
      <Text style={{ fontWeight: 'bold' }}>Auction ID: </Text>{auction.auctionId}
    </Text>
    <Text style={{ marginBottom: 5 }}>
      <Text style={{ fontWeight: 'bold' }}>Lot No: </Text>{lotNo}
    </Text>
    <Text style={{ marginBottom: 5 }}>
      <Text style={{ fontWeight: 'bold' }}>Product Name: </Text>{name}
    </Text>
    <Text style={{ marginBottom: 5 }}>
      <Text style={{ fontWeight: 'bold' }}>Starting Price: </Text>{price(startingPrice)} (Per {bidPer})
    </Text>
    <Text style={{ marginBottom: 5 }}>
      <Text style={{ fontWeight: 'bold' }}>Qty (Approx): </Text>{quantity} {unit}
    </Text>
    <Text style={{ marginBottom: 5 }}>
      <Text style={{ fontWeight: 'bold' }}>EMD Amount: </Text>{price(emdAmount)}
    </Text>
    <Text style={{ marginBottom: 10 }}>
      <Text style={{ fontWeight: 'bold' }}>Auction starts: </Text>{shortDate(auctionDate)} ({readableTime(auctionTime.start)})
    </Text>
  </Card>
}

export default LotContainer(Lot)
