import React from 'react';
import { reduxForm, Field } from 'redux-form';
import renderField from "../../components/FormInput"

import { View, TextInput } from "react-native";
import { Button, Text } from "react-native-elements";
import {price} from "../../../src/constants"
import {styles} from "../styles/Auth"

const validate = formProps => {
  const errors = {};
  if (!formProps.quotePrice || formProps.quotePrice <= 0) errors.quotePrice = 'Please enter the quotation price'
  return errors;
}

const QuotationForm = props => {
  return (
    <View>
      <View style={{ marginTop: 5 }}>
        <View>
          {
            props.quotation && <View style={{ marginBottom: 10 }}>
              <Text style={{ textAlign: 'center', fontSize: 20, fontWeight: 'bold'}}>My Quotation: {price(props.quotation.price)}</Text>
            </View>
          }
          <Text>You can quote a price below the starting price, if any bids are placed, quotations are not valid.</Text>
          <View style={{ marginTop: 10 }}>
            <Field hideLabel name="quotePrice" component={renderField} type="number" placeholder="Enter your quotation..." />
            <Button
              containerStyle={{marginTop: 20, flex: 0}}
              title={'Place Quotation'}
              onPress={props.handleSubmit(props.handleFormSubmit)}
              titleStyle={styles.loginTextButton}
              buttonStyle={{ padding: 5 }}
            />
          </View>
        </View>
      </View>
      {props.renderAlert()}
    </View>
  )
}

export default reduxForm({
  form: 'mobileQuotationForm',
  validate
})(QuotationForm)
