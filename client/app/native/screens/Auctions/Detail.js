import React, { Component } from "react";
import { join, truncate } from 'lodash';

import { ScrollView, Text, Linking, View, RefreshControl, FlatList, ListItem, ListView } from "react-native";
import { Card, Button } from "react-native-elements";

import DetailContainer from '../../../src/containers/auctions/buyer'
import { getMobileHeaders } from "../../auth";
import {shortDate, readableTime, price} from "../../../src/constants"
import {BuyerStatus} from "../../components/Common"

class Detail extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: `Auction ${navigation.state.params.auctionId}`,
  });

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };
  }

  componentDidMount() {
    this.fetchData();
  }
  fetchData = () => {
    return getMobileHeaders()
      .then(headers => this.props.getAuction(this.props.navigation.state.params._id, headers))
  }

  _onRefresh() {
    this.setState({refreshing: true});
    this.fetchData().then(() => {
      this.setState({refreshing: false});
    });
  }
  render() {
    const { auction } = this.props;
    if (!auction) return null;
    console.log('auction: ', auction)
    return (
      <ScrollView
        style={{ flex: 1 }}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh.bind(this)}
          />
        }
      >
        <AuctionCard {...this.props} />

        <View style={{ flex: 1, marginBottom: 20 }}>
          {auction.lots.map(lot => <LotCard key={lot._id} {...this.props} lot={lot}/>)}
        </View>
      </ScrollView>
    )
  }
}

const AuctionCard = ({ auction }) => {
  const {
    company,
    inspectionDetails: { from, to, timings: [time]},
    auctionId,
    auctionDate, auctionTime
  } = auction;
  return <Card>
    <Text style={{ marginBottom: 5 }}>
      <Text style={{ fontWeight: 'bold' }}>Auction ID: </Text>{auctionId}
    </Text>
    <Text style={{ marginBottom: 5 }}>
      <Text style={{ fontWeight: 'bold' }}>Company: </Text>{company}
    </Text>
    <Text style={{ marginBottom: 5 }}>
      <Text style={{ fontWeight: 'bold' }}>Inspection Date: </Text>{shortDate(from)} - {shortDate(to)}
    </Text>
    <Text style={{ marginBottom: 5 }}>
      <Text style={{ fontWeight: 'bold' }}>Inspection Timings: </Text>({readableTime(time.from)} - {readableTime(time.to)})
    </Text>
    <Text style={{ marginBottom: 5 }}>
      <Text style={{ fontWeight: 'bold' }}>Auction Date: </Text>{shortDate(auctionDate)} ({readableTime(auctionTime.start)} - {readableTime(auctionTime.end)})
    </Text>
  </Card>
}

const LotCard = ({ lot, ...props }) => {
  const {
    _id, lotNo, product: { name },
    startingPrice, bidPer,
    quantity, unit, emdAmount,
    auctionDate, auctionTime, liveStatus
  } = lot;
  return <Card title={`LOT NO: ${lotNo}`}>
    <View style={{ position: 'absolute', top: 2, right: 0 }}>{BuyerStatus({ liveStatus })}</View>
    <Text style={{ marginBottom: 5 }}>
      <Text style={{ fontWeight: 'bold' }}>Product Name: </Text>{name}
    </Text>
    <Text style={{ marginBottom: 5 }}>
      <Text style={{ fontWeight: 'bold' }}>Starting Price: </Text>{price(startingPrice)} (Per {bidPer})
    </Text>
    <Text style={{ marginBottom: 5 }}>
      <Text style={{ fontWeight: 'bold' }}>Qty (Approx): </Text>{quantity} {unit}
    </Text>
    <Text style={{ marginBottom: 5 }}>
      <Text style={{ fontWeight: 'bold' }}>EMD Amount: </Text>{price(emdAmount)}
    </Text>
    <Text style={{ marginBottom: 10 }}>
      <Text style={{ fontWeight: 'bold' }}>Auction starts: </Text>{shortDate(auctionDate)} ({readableTime(auctionTime.start)})
    </Text>

    <Button
      backgroundColor="#03A9F4"
      title="Enter"
      onPress={() => props.navigation.navigate('Lot', {_id, auction: props.auction, lotNo })}
    />
  </Card>
}

export default DetailContainer(Detail)
