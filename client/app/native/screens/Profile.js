import React, { Component } from "react";
import { View, StatusBar, Image } from "react-native";
import { Card, Button, Text } from "react-native-elements";

import AuthContainer from '../containers/AuthContainer';
import { LogoTitle } from "../components/Common"

class Profile extends Component {
  static navigationOptions = {
    // headerTitle instead of title
    headerTitle: <LogoTitle />,
  };

  render() {
    const { navigation, user, logoutBuyer }  =  this.props;
    return (
      <View style={{ flex: 1 }}>
        {/*<StatusBar barStyle="light-content" />*/}
        <Card title={`USER ID: ${user.userId}`}>
          {/*<View*/}
            {/*style={{*/}
              {/*backgroundColor: "#bcbec1",*/}
              {/*alignItems: "center",*/}
              {/*justifyContent: "center",*/}
              {/*width: 80,*/}
              {/*height: 80,*/}
              {/*borderRadius: 40,*/}
              {/*alignSelf: "center",*/}
              {/*marginBottom: 20*/}
            {/*}}*/}
          {/*>*/}
            {/*<Text style={{ color: "white", fontSize: 28 }}>JD</Text>*/}
          {/*</View>*/}
          <Button
            backgroundColor="#03A9F4"
            title="SIGN OUT"
            onPress={logoutBuyer}
          />
        </Card>
      </View>
    )
  }
}

export default AuthContainer(Profile)
