import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  ImageBackground,
  Dimensions,
  LayoutAnimation,
  UIManager,
  KeyboardAvoidingView,
  Image,
  Alert,
  ScrollView
} from 'react-native';

import ForgotForm from '../containers/ForgotForm';
import AuthContainer from '../containers/AuthContainer';
import { styles } from "./styles/Auth";

const LOGO = require('../../src/assets/img/brand/logo2x.png');

class ForgotPassword extends Component {
  state= { loading: false }

  handleForgotPassword = ({ email }) => {
    this.setState({ loading: true })
    this.props.forgotPasswordOTP({
      email,
      redirect: this.confirmRedirection.bind(this)
    });
  }

  confirmRedirection = () => {
    Alert.alert(
      'OTP Sent',
      'You’ll receive an OTP in your email inbox shortly to reset your password',
      [
        // {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'OK', onPress: () => this.props.navigation.navigate("Reset")},
      ],
      { cancelable: false }
    )
  }

  renderAlert = () => {
    if (this.props.forgotPassOTPErr) {
      return (
        <View style={{ backgroundColor: 'red', margin: 15, padding: 10, borderRadius:10, marginBottom: 0 }}>
          <Text style={{ color: '#fff', textAlign: 'center' }}>Error! {this.props.forgotPassOTPErr}</Text>
        </View>
      )
    }
  }

  render() {
    const {isLoading} = this.state;
    return (
      <ScrollView style={styles.container}>
        <View style={styles.forgotBg}>
          <KeyboardAvoidingView contentContainerStyle={styles.resetContainer} behavior='position'>
            <View style={styles.titleContainer}>
              <View style={styles.logoContainer}>
                <Image style={styles.logo} source={LOGO}/>
              </View>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Text style={[styles.categoryText, styles.selectedCategoryText]}>Forgot Password?</Text>
            </View>
            <ForgotForm
              handleFormSubmit={this.handleForgotPassword}
              renderAlert={this.renderAlert}
              isLoading={isLoading}
            />
          </KeyboardAvoidingView>
        </View>
      </ScrollView>
    );
  }
}

export default AuthContainer(ForgotPassword)
