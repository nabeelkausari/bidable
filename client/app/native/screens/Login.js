import React, { Component } from "react";
import { Text, View } from "react-native";
import { Button} from "react-native-elements";

import LoginForm from '../containers/LoginForm';
import AuthContainer from '../containers/AuthContainer';

class Login extends Component {
  handleFormSubmit = ({ email, password }) => {
    this.props.loginBuyer({
      email,
      password,
      redirect: () => this.props.navigation.navigate("SignedIn")
    });
  }

  renderAlert = () => {
    if (this.props.authBuyerError) {
      return (
        <View style={{ backgroundColor: 'red', margin: 15, padding: 10, borderRadius:10, marginBottom: 0 }}>
          <Text style={{ color: '#fff', textAlign: 'center' }}>Oops! {this.props.authBuyerError}</Text>
        </View>
      )
    }
  }

  render() {
    return (
      <View style={{ paddingVertical: 20 }}>
        <LoginForm
          handleFormSubmit={this.handleFormSubmit}
          renderAlert={this.renderAlert.bind(this)}
        />
        <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
          <Button
            buttonStyle={{ marginTop: 20 }}
            backgroundColor="transparent"
            textStyle={{ color: "#1D4FC1" }}
            title="Register"
            onPress={() => this.props.navigation.navigate("Register")}
          />
          <Button
            buttonStyle={{ marginTop: 20 }}
            backgroundColor="transparent"
            textStyle={{ color: "#1D4FC1" }}
            title="Forgot Password?"
            onPress={() => this.props.navigation.navigate("Forgot")}
          />
        </View>
      </View>
    )
  }
}

export default AuthContainer(Login)
