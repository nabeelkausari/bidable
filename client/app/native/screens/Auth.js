import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  View,
  Text,
  ImageBackground,
  Dimensions,
  LayoutAnimation,
  UIManager,
  KeyboardAvoidingView,
  Image,
  Alert,
  ScrollView
} from 'react-native';
import { Input, Button } from 'react-native-elements'

import LoginForm from '../containers/LoginForm';
import RegisterForm from '../containers/RegisterForm';
import AuthContainer from '../containers/AuthContainer';
import { styles } from "./styles/Auth";

const LOGO = require('../../src/assets/img/brand/logo2x.png');

// Enable LayoutAnimation on Android
UIManager.setLayoutAnimationEnabledExperimental
&& UIManager.setLayoutAnimationEnabledExperimental(true);

const TabSelector = ({ selected }) => {
  return (
    <View style={styles.selectorContainer}>
      <View style={selected && styles.selected}/>
    </View>
  );
};

TabSelector.propTypes = {
  selected: PropTypes.bool.isRequired,
};

class Auth extends Component {

  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      fontLoaded: false,
      selectedCategory: 0,
      isLoading: false,
      isEmailValid: true,
      isPasswordValid: true,
      isConfirmationValid: true,
    };

    this.selectCategory = this.selectCategory.bind(this);
  }

  selectCategory(selectedCategory) {
    LayoutAnimation.easeInEaseOut();
    this.setState({
      selectedCategory,
      isLoading: false,
    });
  }

  handleLogin = ({ email, password }) => {
    this.props.loginBuyer({
      email,
      password,
      redirect: this.doLogin.bind(this)
    });
  }

  handleRegister = ({ email, mobile, password }) => {
    this.setState({ isLoading: true });
    this.props.registerBuyer({
      email,
      password,
      mobile,
      redirect: this.confirmRedirection.bind(this)
    });
  }

  doLogin = () => {
    LayoutAnimation.easeInEaseOut();
    this.setState({isLoading: false});
    this.props.navigation.navigate("SignedIn")
  }

  confirmRedirection = () => {
    Alert.alert(
      'Confirm Email',
      'You’ll receive a confirmation email in your inbox with a link to activate your account',
      [
        // {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'OK', onPress: () => this.props.navigation.navigate("Auth")},
      ],
      { cancelable: false }
    )
  }

  renderAlert = () => {
    if (this.props.authBuyerError) {
      return (
        <View style={{ backgroundColor: 'red', margin: 15, padding: 10, borderRadius:10, marginBottom: 0 }}>
          <Text style={{ color: '#fff', textAlign: 'center' }}>Error! {this.props.authBuyerError}</Text>
        </View>
      )
    }
  }

  render() {
    const {
      selectedCategory,
      isLoading,
    } = this.state;
    const isLoginPage = selectedCategory === 0;
    const isSignUpPage = selectedCategory === 1;
    return (
      <ScrollView style={styles.container}>
        <View style={styles.bgImage}>
          <KeyboardAvoidingView contentContainerStyle={styles.loginContainer} behavior='position'>
            <View style={styles.titleContainer}>
              <View style={styles.logoContainer}>
                <Image style={styles.logo} source={LOGO}/>
              </View>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Button
                disabled={isLoading}
                clear
                activeOpacity={0.7}
                onPress={() => this.selectCategory(0)}
                containerStyle={{flex: 1}}
                titleStyle={[styles.categoryText, isLoginPage && styles.selectedCategoryText]}
                title={'Login'}
              />
              <Button
                disabled={isLoading}
                clear
                activeOpacity={0.7}
                onPress={() => this.selectCategory(1)}
                containerStyle={{flex: 1}}
                titleStyle={[styles.categoryText, isSignUpPage && styles.selectedCategoryText]}
                title={'Sign up'}
              />
            </View>
            <View style={styles.rowSelector}>
              <TabSelector selected={isLoginPage}/>
              <TabSelector selected={isSignUpPage}/>
            </View>
            {isLoginPage && <LoginForm
              handleFormSubmit={this.handleLogin}
              renderAlert={this.renderAlert}
              isLoading={isLoading}
            />}
            {isSignUpPage && <RegisterForm
              handleFormSubmit={this.handleRegister}
              renderAlert={this.renderAlert}
              isLoading={isLoading}
            />}
          </KeyboardAvoidingView>
          <View style={styles.helpContainer}>
            <Button
              title={'Forgot Password ?'}
              titleStyle={{color: 'white'}}
              buttonStyle={{backgroundColor: 'transparent'}}
              underlayColor='transparent'
              onPress={() => this.props.navigation.navigate('Forgot')}
            />
          </View>
        </View>
      </ScrollView>
    );
  }
}

export default AuthContainer(Auth)
