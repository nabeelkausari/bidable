import React from "react";
import { connect } from "react-redux";
import { createRootNavigator } from "./router";
import { isSignedIn } from "./auth";

class App extends React.Component {
  state = {
    signedIn: false,
    checkedSignIn: false
  }

  componentDidMount() {
    this.props.isSignedIn()
  }

  componentDidUpdate(prevProps) {
    if (prevProps.buyerAuthenticated !== this.props.buyerAuthenticated) {
      this.setState({ checkedSignIn: true, signedIn: this.props.buyerAuthenticated })
    }
  }

  render() {
    const { checkedSignIn, signedIn } = this.state;
    // If we haven't checked AsyncStorage yet, don't render anything (better ways to do this)
    if (!checkedSignIn) {
      return null;
    }

    const Layout = createRootNavigator(signedIn);
    return <Layout />;
  }
}

function mapStateToProps(state) {
  return {
    ...state.buyerAuth
  }
}

export default connect(mapStateToProps, { isSignedIn })(App)
