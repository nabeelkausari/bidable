import {
  SellerDashboard,
  SellerMyAuctions,
  SellerCreateAuction,
  SellerEditAuction,
  SellerAuctionDetails,
} from './components';

import SellerSales from './components/Seller/Sales/List';
import SellerQuotations from './components/Seller/Quotations/List';

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/seller/dashboard', name: 'Seller Dashboard', component: SellerDashboard },
  { exact: true, path: '/seller/auctions', name: 'My Auctions List', component: SellerMyAuctions },
  { path: '/seller/create-auction', name: 'Seller Create Auction', component: SellerCreateAuction },
  { path: '/seller/edit-auction/:auctionId', name: 'Seller Edit Auction', component: SellerEditAuction },
  { exact: true, path: '/seller/auctions/:auctionId', name: 'Seller Auction Details', component: SellerAuctionDetails },
  { exact: true, path: '/seller/sales', name: 'Seller Sales', component: SellerSales },
  { exact: true, path: '/seller/quotations', name: 'Seller Quotations', component: SellerQuotations },
];

export default routes;
