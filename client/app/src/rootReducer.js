import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import authReducer from './containers/auth/common/reducer';
import adminAuthReducer from './containers/auth/admin/reducer';
import buyerAuthReducer from './containers/auth/buyer/reducer';
import sellerAuthReducer from './containers/auth/seller/reducer';
import usersReducer from './containers/users/reducer';
import lotsReducer from './containers/lots/reducer';
import auctionsReducer from './containers/auctions/reducer';
import dashboardReducer from './containers/dashboard/reducer';
import emdReducer from './containers/emd/reducer';
import marketingReducer from './containers/marketing/reducer';
import salesReducer from './containers/sales/reducer';
import quotationsReducer from './containers/quotations/reducer';

export default combineReducers({
  form: formReducer,
  auth: authReducer,
  adminAuth: adminAuthReducer,
  buyerAuth: buyerAuthReducer,
  sellerAuth: sellerAuthReducer,
  users: usersReducer,
  lots: lotsReducer,
  auctions: auctionsReducer,
  dashboard: dashboardReducer,
  emd: emdReducer,
  marketing: marketingReducer,
  sales: salesReducer,
  quotations: quotationsReducer,
});
