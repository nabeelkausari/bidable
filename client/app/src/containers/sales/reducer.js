import {
  GET_BUYER_SALES, GET_BUYER_SALES_ERROR, GET_BUYER_SALES_SUCCESS,
  GET_SELLER_SALES, GET_SELLER_SALES_ERROR, GET_SELLER_SALES_SUCCESS
} from "./types";

export default (state = {}, { type, payload }) => {
  switch (type) {

    case GET_SELLER_SALES:
      return { ...state, sellerSalesLoading: true, sellerSalesError: null };
    case GET_SELLER_SALES_SUCCESS:
      return { ...state, sellerSalesLoading: false, sellerSales: payload };
    case GET_SELLER_SALES_ERROR:
      return { ...state, sellerSalesLoading: false, sellerSalesError: payload && payload.error };

    case GET_BUYER_SALES:
      return { ...state, buyerSalesLoading: true, buyerSalesError: null };
    case GET_BUYER_SALES_SUCCESS:
      return { ...state, buyerSalesLoading: false, buyerSales: payload };
    case GET_BUYER_SALES_ERROR:
      return { ...state, buyerSalesLoading: false, buyerSalesError: payload && payload.error };

    default:
      return state;
  }
}
