import { connect } from 'react-redux';
import { getBuyerSales, getSellerSales } from './actions';

function mapStateToProps(state) {
  return {
    ...state.sales
  }
}

export default connect(mapStateToProps, { getBuyerSales, getSellerSales })
