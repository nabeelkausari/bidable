import axios from 'axios';

import { HOST } from "../../constants";
import {
  GET_BUYER_SALES, GET_BUYER_SALES_ERROR, GET_BUYER_SALES_SUCCESS,
  GET_SELLER_SALES, GET_SELLER_SALES_ERROR, GET_SELLER_SALES_SUCCESS
} from "./types";

export const getSellerSales = (authHeaders) => dispatch => {
  dispatch({ type: GET_SELLER_SALES })
  axios.get(`${HOST}/api/seller/sales`, authHeaders)
    .then(res => dispatch({ type: GET_SELLER_SALES_SUCCESS, payload: res.data}))
    .catch(err => dispatch({ type: GET_SELLER_SALES_ERROR, payload: err.response && err.response.data}))
}

export const getBuyerSales = (authHeaders) => dispatch => {
  dispatch({ type: GET_BUYER_SALES })
  axios.get(`${HOST}/api/buyer/sales`, authHeaders)
    .then(res => dispatch({ type: GET_BUYER_SALES_SUCCESS, payload: res.data}))
    .catch(err => dispatch({ type: GET_BUYER_SALES_ERROR, payload: err.response && err.response.data}))
}
