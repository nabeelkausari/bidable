import axios from 'axios';

import { HOST } from "../../constants";
import {
  BID_MADE, CREATE_BID, CREATE_BID_ERROR, CREATE_BID_SUCCESS,
  GET_LOT_SUCCESS, GET_LOT_ERROR, GET_LOT, GET_MY_BIDS, GET_MY_BIDS_ERROR, GET_MY_BIDS_SUCCESS,
  GET_SALE_INTIMATIONS, GET_SALE_INTIMATIONS_ERROR, GET_SALE_INTIMATIONS_SUCCESS, SET_MY_BID, CLEAR_MY_BID,
  GET_LOT_BIDS, GET_LOT_BIDS_ERROR, GET_LOT_BIDS_SUCCESS, REMOVE_SELECTED_LOT, CREATE_QUOTATION,
  CREATE_QUOTATION_ERROR,
  CREATE_QUOTATION_SUCCESS, GET_MY_QUOTATION, GET_MY_QUOTATION_ERROR, GET_MY_QUOTATION_SUCCESS,
  GET_MY_QUOTATIONS, GET_MY_QUOTATIONS_ERROR, GET_MY_QUOTATIONS_SUCCESS
} from "./types";

export const setMyBid = price => {
  return { type: SET_MY_BID, payload: price }
}

export const clearMyBid = () => {
  return { type: CLEAR_MY_BID }
}

export const createQuotation = ({lotId, quotePrice, redirect}, authHeaders) => dispatch => {
  dispatch({ type: CREATE_QUOTATION })
  axios.post(`${HOST}/api/quotations`, {lotId, quotePrice}, authHeaders)
    .then(res => {
      dispatch({ type: CREATE_QUOTATION_SUCCESS, payload: res.data})
      if (redirect) redirect()
    })
    .catch(err => dispatch({ type: CREATE_QUOTATION_ERROR, payload: err.response && err.response.data}))
}

export const getMyQuotation = (lotId, authHeaders) => dispatch => {
  dispatch({ type: GET_MY_QUOTATION })
  axios.get(`${HOST}/api/quotations/${lotId}`, authHeaders)
    .then(res => dispatch({ type: GET_MY_QUOTATION_SUCCESS, payload: res.data}))
    .catch(err => dispatch({ type: GET_MY_QUOTATION_ERROR, payload: err.response && err.response.data}))
}

export const getMyQuotations = (authHeaders) => dispatch => {
  dispatch({ type: GET_MY_QUOTATIONS })
  axios.get(`${HOST}/api/quotations`, authHeaders)
    .then(res => dispatch({ type: GET_MY_QUOTATIONS_SUCCESS, payload: res.data}))
    .catch(err => dispatch({ type: GET_MY_QUOTATIONS_ERROR, payload: err.response && err.response.data}))
}

export const createBid = ({ myBid, lotId }, authHeaders) => dispatch => {
  dispatch({ type: CREATE_BID })
  axios.post(`${HOST}/api/bids`, {bidPrice: myBid, lotId}, authHeaders)
    .then(res => dispatch({ type: CREATE_BID_SUCCESS, payload: res.data}))
    .catch(err => dispatch({ type: CREATE_BID_ERROR, payload: err.response && err.response.data}))
}

export const getMyBids = (authHeaders) => dispatch => {
  dispatch({ type: GET_MY_BIDS })
  axios.get(`${HOST}/api/bids`, authHeaders)
    .then(res => dispatch({ type: GET_MY_BIDS_SUCCESS, payload: res.data}))
    .catch(err => dispatch({ type: GET_MY_BIDS_ERROR, payload: err.response && err.response.data}))
}

export const getLotBids = (lotId, authHeaders) => dispatch => {
  dispatch({ type: GET_LOT_BIDS })
  axios.get(`${HOST}/api/bids/${lotId}`, authHeaders)
    .then(res => dispatch({ type: GET_LOT_BIDS_SUCCESS, payload: res.data}))
    .catch(err => dispatch({ type: GET_LOT_BIDS_ERROR, payload: err.response && err.response.data}))
}

export const getLot = ({ auctionId, lotNo }, authHeaders) => dispatch => {
  dispatch({ type: GET_LOT })
  axios.get(`${HOST}/api/lots/${auctionId}/${lotNo}`, authHeaders)
    .then(res => dispatch({ type: GET_LOT_SUCCESS, payload: res.data}))
    .catch(err => dispatch({ type: GET_LOT_ERROR, payload: err.response && err.response.data}))
}

export const getLotById = (lotId, authHeaders) => dispatch => {
  dispatch({ type: GET_LOT })
  axios.get(`${HOST}/api/lotId/${lotId}`, authHeaders)
    .then(res => dispatch({ type: GET_LOT_SUCCESS, payload: res.data}))
    .catch(err => dispatch({ type: GET_LOT_ERROR, payload: err.response && err.response.data}))
}

export const updateSelectedLot = data => {
  return { type: BID_MADE, payload: data }
}

export const removeSelectedLot = () => {
  return { type: REMOVE_SELECTED_LOT }
}

export const getSaleIntimations = (authHeaders) => dispatch => {
  dispatch({ type: GET_SALE_INTIMATIONS })
  axios.get(`${HOST}/api/buyer/sales`, authHeaders)
    .then(res => dispatch({ type: GET_SALE_INTIMATIONS_SUCCESS, payload: res.data}))
    .catch(err => dispatch({ type: GET_SALE_INTIMATIONS_ERROR, payload: err.response && err.response.data}))
}
