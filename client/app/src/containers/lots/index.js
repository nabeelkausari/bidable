import { connect } from 'react-redux';
import { createBid, getLot, updateSelectedLot, getMyBids,
  getSaleIntimations, getLotBids, removeSelectedLot, createQuotation,
  getMyQuotation, getMyQuotations
} from './actions';
import { selectEMDLot } from "../emd/actions"

function mapStateToProps(state) {
  return {
    ...state.lots,
    initialValues: state.lots,
    ...state.buyerAuth
  }
}

export default connect(mapStateToProps, {
  createBid, getLot, updateSelectedLot, getMyBids, getSaleIntimations, getLotBids,
  selectEMDLot, removeSelectedLot, createQuotation, getMyQuotation, getMyQuotations
})
