import {
  CREATE_BID, CREATE_BID_SUCCESS, CREATE_BID_ERROR,
  GET_LOT, GET_LOT_ERROR, GET_LOT_SUCCESS, BID_MADE, GET_MY_BIDS, GET_MY_BIDS_SUCCESS, GET_MY_BIDS_ERROR,
  GET_SALE_INTIMATIONS, GET_SALE_INTIMATIONS_SUCCESS, GET_SALE_INTIMATIONS_ERROR, SET_MY_BID, CLEAR_MY_BID,
  GET_LOT_BIDS_SUCCESS, GET_LOT_BIDS_ERROR, GET_LOT_BIDS, REMOVE_SELECTED_LOT,
  CREATE_QUOTATION_SUCCESS, CREATE_QUOTATION_ERROR, CREATE_QUOTATION,
  GET_MY_QUOTATION_SUCCESS, GET_MY_QUOTATION_ERROR, GET_MY_QUOTATION,
  GET_MY_QUOTATIONS_SUCCESS, GET_MY_QUOTATIONS_ERROR, GET_MY_QUOTATIONS
} from "./types";

import { getLiveStatus } from "../../constants"

export default (state = {}, { type, payload }) => {
  switch (type) {
    case CREATE_BID:
      return { ...state, createBidLoading: true, createBidError: null };
    case CREATE_BID_SUCCESS:
      return { ...state, createBidLoading: false, createBidSuccess: true };
    case CREATE_BID_ERROR:
      return { ...state, createBidLoading: false, createBidError: handleDuplicateBids(payload.error), createBidSuccess: false };

    case CREATE_QUOTATION:
      return { ...state, createQuotationLoading: true, createQuotationError: null };
    case CREATE_QUOTATION_SUCCESS:
      return { ...state, createQuotationLoading: false, myQuotation: payload };
    case CREATE_QUOTATION_ERROR:
      return { ...state, createQuotationLoading: false, createQuotationError: payload.error };

    case GET_MY_QUOTATION:
      return { ...state, myQuotationLoading: true, myQuotationError: null };
    case GET_MY_QUOTATION_SUCCESS:
      return { ...state, myQuotationLoading: false, myQuotation: payload };
    case GET_MY_QUOTATION_ERROR:
      return { ...state, myQuotationLoading: false, myQuotationError: payload.error };

    case GET_MY_QUOTATIONS:
      return { ...state, myQuotationsLoading: true, myQuotationsError: null };
    case GET_MY_QUOTATIONS_SUCCESS:
      return { ...state, myQuotationsLoading: false, myQuotations: payload };
    case GET_MY_QUOTATIONS_ERROR:
      return { ...state, myQuotationsLoading: false, myQuotationsError: payload.error };

    case SET_MY_BID:
      return { ...state, bidForm: { myBid: payload } };
    case CLEAR_MY_BID:
      return { ...state, bidForm: { myBid: 0 } };
    case REMOVE_SELECTED_LOT:
      return { ...state, selectedLot: null };

    case GET_LOT:
      return { ...state, lotLoading: true, lotError: null };
    case GET_LOT_SUCCESS:
      return { ...state, lotLoading: false, selectedLot: updateSelectedLot(state, payload)};
    case GET_LOT_ERROR:
      return { ...state, lotLoading: false, lotError: payload.error };

    case BID_MADE:
      return {
        ...state,
        selectedLot: updateSelectedLot(state, payload.lotUpdates),
        lotBids: updateLotBids(state, payload.bidUpdates)
      };

    case GET_MY_BIDS:
      return { ...state, myBidsLoading: true, myBidsError: null };
    case GET_MY_BIDS_SUCCESS:
      return { ...state, myBidsLoading: false, myBids: payload };
    case GET_MY_BIDS_ERROR:
      return { ...state, myBidsLoading: false, myBidsError: payload.error };

    case GET_LOT_BIDS:
      return { ...state, lotBidsLoading: true, lotBidsError: null };
    case GET_LOT_BIDS_SUCCESS:
      return { ...state, lotBidsLoading: false, lotBids: payload };
    case GET_LOT_BIDS_ERROR:
      return { ...state, lotBidsLoading: false, lotBidsError: payload.error };

    case GET_SALE_INTIMATIONS:
      return { ...state, saleIntimationsLoading: true, saleIntimationsError: null };
    case GET_SALE_INTIMATIONS_SUCCESS:
      return { ...state, saleIntimationsLoading: false, saleIntimations: payload };
    case GET_SALE_INTIMATIONS_ERROR:
      return { ...state, saleIntimationsLoading: false, saleIntimationsError: payload && payload.error };

    default:
      return state;
  }
}

const updateSelectedLot = (state, payload) => {
  return {
    ...state.selectedLot,
    ...payload,
  }
}

const updateLotBids = (state, payload) => {
  return [
    payload,
    ...state.lotBids.slice(0, 4),
  ];
}

const handleDuplicateBids = (error) => {
  if (error && error.code && error.code === 11000) {
    return "Oh No! Someone else placed the bid before you!"
  } else {
    return error
  }
}
