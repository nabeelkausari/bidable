import { findIndex } from 'lodash';
import {
  SUBMIT_AUCTION,
  SUBMIT_AUCTION_ERROR,
  SUBMIT_AUCTION_SUCCESS,
  DELETE_AUCTION,
  DELETE_AUCTION_ERROR,
  DELETE_AUCTION_SUCCESS,
  CREATE_AUCTION,
  CREATE_AUCTION_ERROR,
  CREATE_AUCTION_SUCCESS,
  GET_MY_AUCTIONS,
  GET_MY_AUCTIONS_ERROR,
  GET_MY_AUCTIONS_SUCCESS,
  GET_AUCTIONS,
  GET_AUCTIONS_ERROR,
  GET_AUCTIONS_SUCCESS,
  SELECT_AUCTION,
  SELECT_AUCTION_TO_EDIT,
  GET_SELECTED_AUCTION,
  GET_SELECTED_AUCTION_ERROR,
  GET_SELECTED_AUCTION_SUCCESS,
  UPDATE_AUCTION,
  UPDATE_AUCTION_SUCCESS,
  UPDATE_AUCTION_ERROR,
  GET_AUCTION_TO_EDIT,
  GET_AUCTION_TO_EDIT_ERROR,
  GET_AUCTION_TO_EDIT_SUCCESS,
  CREATE_LOT,
  CREATE_LOT_SUCCESS,
  CREATE_LOT_ERROR,
  SELECT_LOT_TO_EDIT,
  UPDATE_LOT,
  UPDATE_LOT_SUCCESS,
  UPDATE_LOT_ERROR,
  DELETE_LOT,
  DELETE_LOT_SUCCESS,
  DELETE_LOT_ERROR,
  GET_AUCTION,
  GET_AUCTION_ERROR,
  GET_AUCTION_SUCCESS,
  APPROVE_AUCTION_SUCCESS,
  APPROVE_AUCTION_ERROR,
  APPROVE_AUCTION
} from "./types"

const initialState = {
  lotToCreate: { serviceFee: true }
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case GET_AUCTIONS:
      return { ...state, auctionsLoading: true, auctionsError: null };
    case GET_AUCTIONS_SUCCESS:
      return { ...state, auctionsLoading: false, auctions: payload };
    case GET_AUCTIONS_ERROR:
      return { ...state, auctionsLoading: false, auctionsError: payload };

    case GET_AUCTION:
      return { ...state, auctionLoading: true, auctionError: null };
    case GET_AUCTION_SUCCESS:
      return { ...state,
        auctionLoading: false,
        auction: payload
      };
    case GET_AUCTION_ERROR:
      return { ...state, auctionLoading: false, auctionError: payload };

    case APPROVE_AUCTION:
      return { ...state, approveAuctionLoading: true, approveAuctionError: null };
    case APPROVE_AUCTION_SUCCESS:
      return { ...state, approveAuctionLoading: false };
    case APPROVE_AUCTION_ERROR:
      return { ...state, approveAuctionLoading: false, approveAuctionError: payload };

    case CREATE_AUCTION:
      return { ...state, createAuctionLoading: true, createAuctionError: null };
    case CREATE_AUCTION_SUCCESS:
      return { ...state, createAuctionLoading: false };
    case CREATE_AUCTION_ERROR:
      return { ...state, createAuctionLoading: false, createAuctionError: payload };

    case GET_MY_AUCTIONS:
      return { ...state, myAuctionsLoading: true, myAuctionsError: null };
    case GET_MY_AUCTIONS_SUCCESS:
      return { ...state, myAuctionsLoading: false, myAuctions: payload };
    case GET_MY_AUCTIONS_ERROR:
      return { ...state, myAuctionsLoading: false, myAuctionsError: payload };

    case SUBMIT_AUCTION:
      return { ...state, submitAuctionLoading: true, submitAuctionError: null };
    case SUBMIT_AUCTION_SUCCESS:
      return { ...state, submitAuctionLoading: false,
        selectedAuction: {...state.selectedAuction, status: payload.status }
      };
    case SUBMIT_AUCTION_ERROR:
      return { ...state, submitAuctionLoading: false, submitAuctionError: payload };

    case DELETE_AUCTION:
      return { ...state, deleteAuctionLoading: true, deleteAuctionError: null };
    case DELETE_AUCTION_SUCCESS:
      return { ...state, deleteAuctionLoading: false};
    case DELETE_AUCTION_ERROR:
      return { ...state, deleteAuctionLoading: false, deleteAuctionError: payload };

    case SELECT_AUCTION:
      return { ...state, selectedAuction: payload };

    case SELECT_AUCTION_TO_EDIT:
      return { ...state, auctionToEdit: payload };

    case SELECT_LOT_TO_EDIT:
      return { ...state, lotToEdit: payload };

    case GET_SELECTED_AUCTION:
      return { ...state, getAuctionLoading: true, getAuctionError: null };
    case GET_SELECTED_AUCTION_SUCCESS:
      return { ...state, getAuctionLoading: false, selectedAuction: payload };
    case GET_SELECTED_AUCTION_ERROR:
      return { ...state, getAuctionLoading: false, getAuctionError: payload };

    case GET_AUCTION_TO_EDIT:
      return { ...state, getAuctionToEditLoading: true, getAuctionToEditError: null };
    case GET_AUCTION_TO_EDIT_SUCCESS:
      return { ...state, getAuctionToEditLoading: false, auctionToEdit: payload };
    case GET_AUCTION_TO_EDIT_ERROR:
      return { ...state, getAuctionToEditLoading: false, getAuctionToEditError: payload };

    case UPDATE_AUCTION:
      return { ...state, updateAuctionLoading: true, updateAuctionError: null };
    case UPDATE_AUCTION_SUCCESS:
      return { ...state, updateAuctionLoading: false };
    case UPDATE_AUCTION_ERROR:
      return { ...state, updateAuctionLoading: false, updateAuctionError: payload };

    case CREATE_LOT:
      return { ...state, createLotLoading: true, createLotError: null };
    case CREATE_LOT_SUCCESS:
      return { ...state, createLotLoading: false };
    case CREATE_LOT_ERROR:
      return { ...state, createLotLoading: false, createLotError: payload };

    case UPDATE_LOT:
      return { ...state, updateLotLoading: true, updateLotError: null };
    case UPDATE_LOT_SUCCESS:
      return { ...state, updateLotLoading: false };
    case UPDATE_LOT_ERROR:
      return { ...state, updateLotLoading: false, updateLotError: payload };

    case DELETE_LOT:
      return { ...state, deleteLotLoading: true, deleteLotError: null };
    case DELETE_LOT_SUCCESS:
      return { ...state, deleteLotLoading: false, selectedAuction: removeLot(state, payload)};
    case DELETE_LOT_ERROR:
      return { ...state, deleteLotLoading: false, deleteLotError: payload };

    default:
      return state;
  }
}

function removeLot(state, payload) {
  let lotIndex = findIndex(state.selectedAuction.lots, ['_id', payload._id])
  return {
    ...state.selectedAuction,
    lots: [
      ...state.selectedAuction.lots.slice(0, lotIndex),
      ...state.selectedAuction.lots.slice(lotIndex + 1)
    ]
  }
}
