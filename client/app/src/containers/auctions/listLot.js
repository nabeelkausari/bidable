import { connect } from 'react-redux';

function mapStateToProps(state) {
  return { ...state.auctions }
}

export default connect(mapStateToProps)
