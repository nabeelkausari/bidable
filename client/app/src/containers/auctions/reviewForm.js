import { reduxForm } from 'redux-form';

const validate = formProps => {
  const errors = {};

  if (!formProps.auctionDate) errors.auctionDate = 'Auction Date required'
  if (!formProps.auctionStartsFrom) errors.auctionStartsFrom = 'Auction starts from required'
  return errors;
}

export default reduxForm({
  form: 'lotAuctionForm',
  validate
})
