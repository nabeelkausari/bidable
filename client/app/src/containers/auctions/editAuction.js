import { reduxForm, formValueSelector } from 'redux-form';
import { connect } from 'react-redux';

import { getAuctionToEdit, editAuction, deleteAuction, getMyAuctions } from './actions';

export const validate = formProps => {
  const errors = {};

  if (!formProps.company) errors.company = 'Please enter the company name';
  if (!formProps.inspectionDates || !formProps.inspectionDates[1]) errors.inspectionDates = 'Please enter the dates when the place is open for inspection';
  if (!formProps.inspectionTimeFrom) errors.inspectionTimeFrom = 'Please enter the time when the place is open for inspection';
  if (!formProps.inspectionTimeTo) errors.inspectionTimeTo = 'Please enter the time when the place is closed for inspection';
  return errors;
}

const selector = formValueSelector('editAuction')

function mapStateToProps(state) {
  const inspectionTimeFromValue = selector(state, 'inspectionTimeFrom')

  return {
    ...state.auctions,
    initialValues: state.auctions.auctionToEdit,
    inspectionTimeFromValue
  }
}

export default Comp => {
  const edit = reduxForm({
    form: 'editAuction',
    validate
  })(Comp);

  return connect(mapStateToProps, { getAuctionToEdit, editAuction, deleteAuction, getMyAuctions })(edit)
}

