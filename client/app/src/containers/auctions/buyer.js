import { connect } from 'react-redux';
import { getAuctions, getAuction } from './actions';
import { getLot } from "../lots/actions";

function mapStateToProps(state) {
  return {
    ...state.auctions,
    ...state.buyerAuth
  }
}

export default connect(mapStateToProps, { getAuctions, getAuction, getLot })
