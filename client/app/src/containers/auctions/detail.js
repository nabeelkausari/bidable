import { connect } from 'react-redux';

import { getSelectedAuction, submitAuction, selectAuctionToEdit, selectLotToEdit } from './actions';

function mapStateToProps(state) {
  return { ...state.auctions }
}

export default connect(mapStateToProps, {
  getSelectedAuction, submitAuction, selectAuctionToEdit, selectLotToEdit
})

