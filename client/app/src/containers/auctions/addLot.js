import { reduxForm, formValueSelector } from 'redux-form';
import { connect } from 'react-redux';

import { createLot } from './actions';

export const validate = formProps => {
  const errors = {};

  if (!formProps.productName) errors.productName = 'Please enter the product name';
  if (!formProps.productLocation) errors.productLocation = 'Please enter the product location';
  if (!formProps.productDescription) errors.productDescription = 'Please enter the product description';
  if (!formProps.startingPrice) errors.startingPrice = 'Please enter the starting price';
  if (!formProps.bidPer) errors.bidPer = 'Please select the bid per basis';
  if (!formProps.minimumIncrement) errors.minimumIncrement = 'Please enter the minimum increment value';
  if (!formProps.quantity) errors.quantity = 'Please enter the approx product quantity';
  if (!formProps.unit) errors.unit = 'Please select the unit of quantity';
  if (!formProps.gst) errors.gst = 'Please enter the applicable GST percent';

  return errors;
}

const selector = formValueSelector('createLot')

function mapStateToProps(state) {
  const startingPrice = selector(state, 'startingPrice');
  const bidPer = selector(state, 'bidPer');
  const serviceFee = selector(state, 'serviceFee');
  return {
    ...state.auctions,
    initialValues: state.auctions.lotToCreate,
    lotValues: { startingPrice, bidPer, serviceFee }
  }
}


export default Comp => {
  const createLotComp = reduxForm({
    form: 'createLot',
    validate
  })(Comp);

  return connect(mapStateToProps, { createLot })(createLotComp)
}

