export const GET_AUCTIONS = "GET_AUCTIONS";
export const GET_AUCTIONS_SUCCESS = "GET_AUCTIONS_SUCCESS";
export const GET_AUCTIONS_ERROR = "GET_AUCTIONS_ERROR";

export const GET_AUCTION = "GET_AUCTION";
export const GET_AUCTION_SUCCESS = "GET_AUCTION_SUCCESS";
export const GET_AUCTION_ERROR = "GET_AUCTION_ERROR";

export const REDRAFT_AUCTION = "REDRAFT_AUCTION";
export const REDRAFT_AUCTION_SUCCESS = "REDRAFT_AUCTION_SUCCESS";
export const REDRAFT_AUCTION_ERROR = "REDRAFT_AUCTION_ERROR";

export const APPROVE_AUCTION = "APPROVE_AUCTION";
export const APPROVE_AUCTION_SUCCESS = "APPROVE_AUCTION_SUCCESS";
export const APPROVE_AUCTION_ERROR = "APPROVE_AUCTION_ERROR";

export const CANCEL_AUCTION = "CANCEL_AUCTION";
export const CANCEL_AUCTION_SUCCESS = "CANCEL_AUCTION_SUCCESS";
export const CANCEL_AUCTION_ERROR = "CANCEL_AUCTION_ERROR";

export const CREATE_AUCTION = "CREATE_AUCTION";
export const CREATE_AUCTION_SUCCESS = "CREATE_AUCTION_SUCCESS";
export const CREATE_AUCTION_ERROR = "CREATE_AUCTION_ERROR";

export const GET_MY_AUCTIONS = "GET_MY_AUCTIONS";
export const GET_MY_AUCTIONS_SUCCESS = "GET_MY_AUCTIONS_SUCCESS";
export const GET_MY_AUCTIONS_ERROR = "GET_MY_AUCTIONS_ERROR";

export const SUBMIT_AUCTION = "SUBMIT_AUCTION";
export const SUBMIT_AUCTION_SUCCESS = "SUBMIT_AUCTION_SUCCESS";
export const SUBMIT_AUCTION_ERROR = "SUBMIT_AUCTION_ERROR";

export const DELETE_AUCTION = "DELETE_AUCTION";
export const DELETE_AUCTION_SUCCESS = "DELETE_AUCTION_SUCCESS";
export const DELETE_AUCTION_ERROR = "DELETE_AUCTION_ERROR";

export const SELECT_AUCTION = "SELECT_AUCTION";
export const SELECT_AUCTION_TO_EDIT = "SELECT_AUCTION_TO_EDIT";
export const SELECT_LOT_TO_EDIT = "SELECT_LOT_TO_EDIT";

export const GET_SELECTED_AUCTION = "GET_SELECTED_AUCTION";
export const GET_SELECTED_AUCTION_SUCCESS = "GET_SELECTED_AUCTION_SUCCESS";
export const GET_SELECTED_AUCTION_ERROR = "GET_SELECTED_AUCTION_ERROR";

export const GET_AUCTION_TO_EDIT = "GET_AUCTION_TO_EDIT";
export const GET_AUCTION_TO_EDIT_SUCCESS = "GET_AUCTION_TO_EDIT_SUCCESS";
export const GET_AUCTION_TO_EDIT_ERROR = "GET_AUCTION_TO_EDIT_ERROR";

export const UPDATE_AUCTION = "UPDATE_AUCTION";
export const UPDATE_AUCTION_SUCCESS = "UPDATE_AUCTION_SUCCESS";
export const UPDATE_AUCTION_ERROR = "UPDATE_AUCTION_ERROR";

export const CREATE_LOT = "CREATE_LOT";
export const CREATE_LOT_SUCCESS = "CREATE_LOT_SUCCESS";
export const CREATE_LOT_ERROR = "CREATE_LOT_ERROR";

export const UPDATE_LOT = "UPDATE_LOT";
export const UPDATE_LOT_SUCCESS = "UPDATE_LOT_SUCCESS";
export const UPDATE_LOT_ERROR = "UPDATE_LOT_ERROR";

export const DELETE_LOT = "DELETE_LOT";
export const DELETE_LOT_SUCCESS = "DELETE_LOT_SUCCESS";
export const DELETE_LOT_ERROR = "DELETE_LOT_ERROR";
