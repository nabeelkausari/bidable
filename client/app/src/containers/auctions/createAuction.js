import { reduxForm, formValueSelector } from 'redux-form';
import { connect } from 'react-redux';

import { createAuction } from './actions';

export const validate = formProps => {
  const errors = {};

  if (!formProps.inspectionDates || !formProps.inspectionDates[1]) errors.inspectionDates = 'Please enter the dates when the place is open for inspection';
  if (!formProps.inspectionTimeFrom) errors.inspectionTimeFrom = 'Please enter the time when the place is open for inspection';
  if (!formProps.inspectionTimeTo) errors.inspectionTimeTo = 'Please enter the time when the place is closed for inspection';
  return errors;
}

const selector = formValueSelector('createAuction')

function mapStateToProps(state) {
  const inspectionTimeFromValue = selector(state, 'inspectionTimeFrom')
  return {
    ...state.auctions,
    ...state.sellerAuth,
    inspectionTimeFromValue
  }
}

export default Comp => {
  const create = connect(mapStateToProps, { createAuction })(Comp)

  return reduxForm({
    form: 'createAuction',
    validate
  })(create);
}

