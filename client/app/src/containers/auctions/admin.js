import { connect } from 'react-redux';
import { getAuctionsToReview, getAuctionToReview, redraftAuction, raiseApproveError, approveAuction, cancelAuction } from './actions';
import { getLot } from "../lots/actions";

function mapStateToProps(state) {
  return {
    ...state.auctions,
    ...state.adminAuth
  }
}

export default connect(mapStateToProps, {
  getAuctionsToReview, getAuctionToReview, getLot, redraftAuction,
  raiseApproveError, approveAuction, cancelAuction
})
