import axios from 'axios';

import { HOST, getAuthHeaders } from "../../constants";
import {
  SUBMIT_AUCTION,
  SUBMIT_AUCTION_ERROR,
  SUBMIT_AUCTION_SUCCESS,
  DELETE_LOT,
  DELETE_LOT_ERROR,
  DELETE_LOT_SUCCESS,
  CREATE_AUCTION,
  CREATE_AUCTION_ERROR,
  CREATE_AUCTION_SUCCESS,
  CREATE_LOT,
  CREATE_LOT_ERROR,
  CREATE_LOT_SUCCESS,
  DELETE_AUCTION,
  DELETE_AUCTION_ERROR,
  DELETE_AUCTION_SUCCESS,
  GET_AUCTION_TO_EDIT,
  GET_AUCTION_TO_EDIT_ERROR,
  GET_AUCTION_TO_EDIT_SUCCESS,
  GET_MY_AUCTIONS,
  GET_MY_AUCTIONS_ERROR,
  GET_MY_AUCTIONS_SUCCESS,
  GET_SELECTED_AUCTION,
  GET_SELECTED_AUCTION_ERROR,
  GET_SELECTED_AUCTION_SUCCESS,
  SELECT_AUCTION_TO_EDIT,
  SELECT_LOT_TO_EDIT,
  UPDATE_AUCTION,
  UPDATE_AUCTION_ERROR,
  UPDATE_AUCTION_SUCCESS,
  UPDATE_LOT,
  UPDATE_LOT_ERROR,
  UPDATE_LOT_SUCCESS,
  GET_AUCTIONS,
  GET_AUCTIONS_SUCCESS,
  GET_AUCTIONS_ERROR,
  GET_AUCTION_SUCCESS,
  GET_AUCTION_ERROR,
  GET_AUCTION,
  REDRAFT_AUCTION,
  REDRAFT_AUCTION_ERROR,
  REDRAFT_AUCTION_SUCCESS,
  APPROVE_AUCTION,
  APPROVE_AUCTION_ERROR,
  APPROVE_AUCTION_SUCCESS,
  CANCEL_AUCTION,
  CANCEL_AUCTION_ERROR,
  CANCEL_AUCTION_SUCCESS,
} from "./types"


// ADMIN ACTIONS

export const getAuctionsToReview = (authHeaders) => dispatch => {
  dispatch({ type: GET_AUCTIONS })
  axios.get(`${HOST}/api/admin/auctions`, authHeaders)
    .then(res => dispatch({ type: GET_AUCTIONS_SUCCESS, payload: res.data }))
    .catch(err => dispatch({ type: GET_AUCTIONS_ERROR, payload: err.response && err.response.data }))
}

export const redraftAuction = (id, redirect, authHeaders) => dispatch => {
  dispatch({ type: REDRAFT_AUCTION })
  axios.put(`${HOST}/api/admin/auctions/${id}/redraft`, {}, authHeaders)
    .then(res => {
      dispatch({ type: REDRAFT_AUCTION_SUCCESS, payload: res.data });
      redirect()
    })
    .catch(err => dispatch({ type: REDRAFT_AUCTION_ERROR, payload: err.response && err.response.data }))
}

export const approveAuction = (auction, redirect, authHeaders) => dispatch => {
  dispatch({ type: APPROVE_AUCTION })
  axios.put(`${HOST}/api/admin/auctions/${auction._id}/approve`, auction, authHeaders)
    .then(res => {
      dispatch({ type: APPROVE_AUCTION_SUCCESS, payload: res.data });
      redirect()
    })
    .catch(err => dispatch({ type: APPROVE_AUCTION_ERROR, payload: err.response && err.response.data }))
}

export const cancelAuction = (id, redirect, authHeaders) => dispatch => {
  dispatch({ type: CANCEL_AUCTION })
  axios.put(`${HOST}/api/admin/auctions/${id}/cancel`, {}, authHeaders)
    .then(res => {
      dispatch({ type: CANCEL_AUCTION_SUCCESS, payload: res.data });
      redirect()
    })
    .catch(err => dispatch({ type: CANCEL_AUCTION_ERROR, payload: err.response && err.response.data }))
}

export const raiseApproveError = error => {
  return { type: APPROVE_AUCTION_ERROR, payload: error }
}

export const getAuctionToReview = (auctionId, authHeaders) => dispatch => {
  dispatch({ type: GET_AUCTION })
  axios.get(`${HOST}/api/admin/auctions/${auctionId}`, authHeaders)
    .then(res => dispatch({ type: GET_AUCTION_SUCCESS, payload: res.data }))
    .catch(err => dispatch({ type: GET_AUCTION_ERROR, payload: err.response && err.response.data }))
}


// BUYER ACTIONS

export const getAuctions = (authHeaders) => dispatch => {
  dispatch({ type: GET_AUCTIONS })
  axios.get(`${HOST}/api/auctions`, authHeaders)
    .then(res => dispatch({ type: GET_AUCTIONS_SUCCESS, payload: res.data }))
    .catch(err => dispatch({ type: GET_AUCTIONS_ERROR, payload: err.response && err.response.data }))
}

export const getAuction = (auctionId, authHeaders) => dispatch => {
  dispatch({ type: GET_AUCTION })
  axios.get(`${HOST}/api/auctions/${auctionId}`, authHeaders)
    .then(res => dispatch({ type: GET_AUCTION_SUCCESS, payload: res.data }))
    .catch(err => dispatch({ type: GET_AUCTION_ERROR, payload: err.response && err.response.data }))
}


// SELLER ACTIONS

export const createAuction = ({ inspectionDetails, preferredDate, redirect }) => dispatch => {
  dispatch({ type: CREATE_AUCTION })
  axios.post(`${HOST}/api/auctions`, { inspectionDetails, preferredDate }, getAuthHeaders())
    .then(res => {
      dispatch({ type: CREATE_AUCTION_SUCCESS })
      redirect(res.data);
    })
    .catch(err => dispatch({ type: CREATE_AUCTION_ERROR, payload: err.response && err.response.data }))
}

export const submitAuction = ({ auctionId, redirect }) => dispatch => {
  dispatch({ type: SUBMIT_AUCTION })
  axios.put(`${HOST}/api/auctions/${auctionId}/submit`, {}, getAuthHeaders())
    .then(res =>  {
      dispatch({ type: SUBMIT_AUCTION_SUCCESS, payload: res.data })
      redirect()
    })
    .catch(err => dispatch({ type: SUBMIT_AUCTION_ERROR, payload: err.response && err.response.data }))
}

export const deleteAuction = auctionId => dispatch => {
  dispatch({ type: DELETE_AUCTION })
  axios.delete(`${HOST}/api/auctions/${auctionId}`, getAuthHeaders())
    .then(res =>  dispatch({ type: DELETE_AUCTION_SUCCESS }))
    .catch(err => dispatch({ type: DELETE_AUCTION_ERROR, payload: err.response && err.response.data }))
}

export const getMyAuctions = () => dispatch => {
  dispatch({ type: GET_MY_AUCTIONS })
  axios.get(`${HOST}/api/auctions/mine`, getAuthHeaders())
    .then(res => dispatch({ type: GET_MY_AUCTIONS_SUCCESS, payload: res.data }))
    .catch(err => dispatch({ type: GET_MY_AUCTIONS_ERROR, payload: err.response && err.response.data }))
}

export const getSelectedAuction = auctionId => dispatch => {
  dispatch({ type: GET_SELECTED_AUCTION })
  axios.get(`${HOST}/api/auctions/mine/${auctionId}`, getAuthHeaders())
    .then(res =>  dispatch({ type: GET_SELECTED_AUCTION_SUCCESS, payload: res.data }))
    .catch(err => dispatch({ type: GET_SELECTED_AUCTION_ERROR, payload: err.response && err.response.data }))
}

export const getAuctionToEdit = auctionId => dispatch => {
  dispatch({ type: GET_AUCTION_TO_EDIT })
  axios.get(`${HOST}/api/auctions/mine/${auctionId}`, getAuthHeaders())
    .then(res =>  dispatch({ type: GET_AUCTION_TO_EDIT_SUCCESS, payload: extractSelectedAuction(res.data) }))
    .catch(err => dispatch({ type: GET_AUCTION_TO_EDIT_ERROR, payload: err.response && err.response.data }))
}

export const selectAuctionToEdit = auction => {
  return { type: SELECT_AUCTION_TO_EDIT, payload: extractSelectedAuction(auction) }
}

export const selectLotToEdit = lot => {
  return { type: SELECT_LOT_TO_EDIT, payload: extractLotToEdit(lot) }
}

const extractSelectedAuction = auction => {
  let { _id, auctionId, inspectionDetails: { from, to, timings: [time] }, preferredDate, status } = auction;
  return {
    _id,
    auctionId,
    inspectionDates: [from, to],
    inspectionTimeFrom: time.from,
    inspectionTimeTo: time.to,
    auctionDate: preferredDate,
    status
  }
}

const extractLotToEdit = (lot) => {
  let { product: { name, description, location }, ...restLot } = lot;
  return {
    ...restLot,
    productName: name,
    productDescription: description,
    productLocation: location,
  }
}

export const editAuction = ({ auctionId, inspectionDetails, preferredDate, redirect }) => dispatch => {
  dispatch({ type: UPDATE_AUCTION })
  axios.put(`${HOST}/api/auctions/${auctionId}`, { inspectionDetails, preferredDate }, getAuthHeaders())
    .then(res => {
      dispatch({ type: UPDATE_AUCTION_SUCCESS })
      redirect();
    })
    .catch(err => dispatch({ type: UPDATE_AUCTION_ERROR, payload: err.response && err.response.data }))
}

export const createLot = props => dispatch => {
  dispatch({ type: CREATE_LOT })
  axios.post(`${HOST}/api/lots/${props.auctionId}`, props, getAuthHeaders())
    .then(res => {
      dispatch({ type: CREATE_LOT_SUCCESS, payload: res.data });
      props.redirect();
    })
    .catch(err => dispatch({ type: CREATE_LOT_ERROR, payload: err.response && err.response.data }))
}

export const editLot = props => dispatch => {
  dispatch({ type: UPDATE_LOT })
  axios.put(`${HOST}/api/lots/${props.lotId}`, props, getAuthHeaders())
    .then(res => {
      dispatch({ type: UPDATE_LOT_SUCCESS })
      props.redirect();
    })
    .catch(err => dispatch({ type: UPDATE_LOT_ERROR, payload: err.response && err.response.data }))
}

export const deleteLot = lotId => dispatch => {
  dispatch({ type: DELETE_LOT })
  axios.delete(`${HOST}/api/lots/${lotId}`, getAuthHeaders())
    .then(res =>  dispatch({ type: DELETE_LOT_SUCCESS, payload: res.data }))
    .catch(err => dispatch({ type: DELETE_LOT_ERROR, payload: err.response && err.response.data }))
}
