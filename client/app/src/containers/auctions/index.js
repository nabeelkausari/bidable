import { connect } from 'react-redux';
import { getMyAuctions, getAuctions } from './actions';

function mapStateToProps(state) {
  return { ...state.auctions }
}

export default connect(mapStateToProps, { getMyAuctions, getAuctions })
