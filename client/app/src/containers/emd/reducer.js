import {
  SUBMIT_EMD_SUCCESS, SUBMIT_EMD, SUBMIT_EMD_ERROR, SELECT_EMD_LOT, CLEAR_EMD_LOT, CLEAR_EMD_ERRORS,
  GET_MY_EMDS, GET_MY_EMDS_SUCCESS, GET_MY_EMDS_ERROR, SUBMIT_BANK, SUBMIT_BANK_SUCCESS, SUBMIT_BANK_ERROR,
  GET_BANK_SUCCESS, GET_BANK_ERROR, GET_BANK, GET_EMDS, GET_EMDS_SUCCESS, GET_EMDS_ERROR, EMD_TO_REJECT,
  REJECT_EMD_SUCCESS, REJECT_EMD_ERROR, REJECT_EMD
} from "./types";

export default (state = {}, { type, payload }) => {
  switch (type) {
    case SUBMIT_EMD:
      return { ...state, submitEMDLoading: true, submitEMDError: null };
    case SUBMIT_EMD_SUCCESS:
      return { ...state, submitEMDLoading: false, submitEMDSuccess: true };
    case SUBMIT_EMD_ERROR:
      return { ...state, submitEMDLoading: false, submitEMDError: payload.error, submitEMDSuccess: false };

    case SELECT_EMD_LOT:
      return { ...state, emdLot: payload };
    case CLEAR_EMD_LOT:
      return { ...state, emdLot: null };
    case CLEAR_EMD_ERRORS:
      return { ...state, submitEMDError: null };

    case GET_MY_EMDS:
      return { ...state, myEMDsLoading: true, myEMDsError: null };
    case GET_MY_EMDS_SUCCESS:
      return { ...state, myEMDsLoading: false, myEMDs: payload };
    case GET_MY_EMDS_ERROR:
      return { ...state, myEMDsLoading: false, myEMDsError: payload.error };

    case GET_EMDS:
      return { ...state, EMDsLoading: true, EMDsError: null };
    case GET_EMDS_SUCCESS:
      return { ...state, EMDsLoading: false, EMDs: payload };
    case GET_EMDS_ERROR:
      return { ...state, EMDsLoading: false, EMDsError: payload.error };

    case EMD_TO_REJECT:
      return { ...state, emdToReject: payload };

    case REJECT_EMD:
      return { ...state, rejectEMDLoading: true, rejectEMDError: null };
    case REJECT_EMD_SUCCESS:
      return { ...state, rejectEMDLoading: false, rejectEMDSuccess: true };
    case REJECT_EMD_ERROR:
      return { ...state, rejectEMDLoading: false, rejectEMDError: payload.error, rejectEMDSuccess: false };

    case SUBMIT_BANK:
      return { ...state, submitBankLoading: true, submitBankError: null };
    case SUBMIT_BANK_SUCCESS:
      return { ...state, submitBankLoading: false, bank: payload };
    case SUBMIT_BANK_ERROR:
      return { ...state, submitBankLoading: false, submitBankError: payload.error };

    case GET_BANK:
      return { ...state, bankLoading: true, bankError: null };
    case GET_BANK_SUCCESS:
      return { ...state, bankLoading: false, bank: payload };
    case GET_BANK_ERROR:
      return { ...state, bankLoading: false, bankError: payload.error };

    default:
      return state;
  }
}
