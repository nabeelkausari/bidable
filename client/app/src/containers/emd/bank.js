import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { submitBankDetails, getBankDetails } from './actions';

const validate = formProps => {
  const errors = {};

  if (!formProps.name) errors.name = 'Please enter bank name'
  if (!formProps.account) errors.account = 'Please enter account number'
  if (!formProps.code) errors.code = 'Please enter IFSC code'
  if (!formProps.branch) errors.branch = 'Please enter branch'
  if (!formProps.address) errors.address = 'Please enter bank address'
  return errors;
}

function mapStateToProps(state) {
  return {
    ...state.emd,
    ...state.buyerAuth,
  }
}

export default Comp => {
  Comp = reduxForm({
    form: 'bankForm',
    validate
  })(Comp);

  return connect(mapStateToProps, { submitBankDetails, getBankDetails })(Comp)
}
