import axios from 'axios';

import { HOST } from "../../constants";
import {
  SUBMIT_EMD,
  SUBMIT_EMD_ERROR,
  SUBMIT_EMD_SUCCESS,
  SELECT_EMD_LOT,
  CLEAR_EMD_LOT,
  CLEAR_EMD_ERRORS,
  GET_MY_EMDS,
  GET_MY_EMDS_ERROR,
  GET_MY_EMDS_SUCCESS,
  SUBMIT_BANK,
  SUBMIT_BANK_ERROR,
  SUBMIT_BANK_SUCCESS,
  GET_BANK,
  GET_BANK_ERROR,
  GET_BANK_SUCCESS,
  GET_EMDS,
  GET_EMDS_ERROR,
  GET_EMDS_SUCCESS,
  REJECT_EMD,
  REJECT_EMD_ERROR,
  REJECT_EMD_SUCCESS,
  APPROVE_EMD,
  APPROVE_EMD_ERROR,
  APPROVE_EMD_SUCCESS,
  EMD_TO_REJECT,
  DELETE_EMD,
  DELETE_EMD_ERROR,
  DELETE_EMD_SUCCESS
} from "./types";

// ADMIN ACTIONS

export const getEMDs = (query, authHeaders) => dispatch => {
  dispatch({ type: GET_EMDS })
  axios.get(`${HOST}/api/admin/emds?filter=${query}`, authHeaders)
    .then(res => dispatch({ type: GET_EMDS_SUCCESS, payload: res.data }))
    .catch(err => dispatch({ type: GET_EMDS_ERROR, payload: err.response && err.response.data }))
}

export const selectEMDToReject = emdId => {
  return { type: EMD_TO_REJECT, payload: emdId }
}

export const rejectEMD = (emd, authHeaders) => dispatch => {
  dispatch({ type: REJECT_EMD })
  axios.put(`${HOST}/api/admin/emds/${emd._id}/reject`, emd, authHeaders)
    .then(res => {
      dispatch({ type: REJECT_EMD_SUCCESS });
      if (emd.redirect) emd.redirect()
    })
    .catch(err => dispatch({ type: REJECT_EMD_ERROR, payload: err.response && err.response.data }))
}

export const approveEMD = (id, redirect, authHeaders) => dispatch => {
  dispatch({ type: APPROVE_EMD })
  axios.put(`${HOST}/api/admin/emds/${id}/approve`, {}, authHeaders)
    .then(res => {
      dispatch({ type: APPROVE_EMD_SUCCESS })
      redirect()
    })
    .catch(err => dispatch({ type: APPROVE_EMD_ERROR, payload: err.response && err.response.data }))
}


// BUYER ACTIONS

export const selectEMDLot = lotId => {
  return { type: SELECT_EMD_LOT, payload: lotId }
}

export const clearEMDLot = () => {
  return { type: CLEAR_EMD_LOT }
}

export const clearEMDErrors = () => {
  return { type: CLEAR_EMD_ERRORS }
}

export const submitEMD = (emd, authHeaders) => dispatch => {
  dispatch({ type: SUBMIT_EMD })
  axios.post(`${HOST}/api/emd`, emd, authHeaders)
    .then(res => {
      dispatch({ type: SUBMIT_EMD_SUCCESS, payload: res.data})
      if (emd.redirect) emd.redirect();
    })
    .catch(err => dispatch({ type: SUBMIT_EMD_ERROR, payload: err.response && err.response.data}))
}

export const deleteEMD = (emd, authHeaders) => dispatch => {
  dispatch({ type: DELETE_EMD })
  axios.delete(`${HOST}/api/emd/${emd._id}`, authHeaders)
    .then(res => {
      dispatch({ type: DELETE_EMD_SUCCESS})
      if (emd.redirect) emd.redirect();
    })
    .catch(err => dispatch({ type: DELETE_EMD_ERROR, payload: err.response && err.response.data}))
}

export const getMyEMDs = (authHeaders) => dispatch => {
  dispatch({ type: GET_MY_EMDS })
  axios.get(`${HOST}/api/emd`, authHeaders)
    .then(res => dispatch({ type: GET_MY_EMDS_SUCCESS, payload: res.data }))
    .catch(err => dispatch({ type: GET_MY_EMDS_ERROR, payload: err.response && err.response.data }))
}

export const submitBankDetails = (bank, authHeaders) => dispatch => {
  dispatch({ type: SUBMIT_BANK })
  axios.post(`${HOST}/api/bank`, bank, authHeaders)
    .then(res => {
      dispatch({ type: SUBMIT_BANK_SUCCESS, payload: res.data})
      if (bank.redirect) bank.redirect();
    })
    .catch(err => dispatch({ type: SUBMIT_BANK_ERROR, payload: err.response && err.response.data}))
}

export const getBankDetails = (authHeaders) => dispatch => {
  dispatch({ type: GET_BANK })
  axios.get(`${HOST}/api/bank`, authHeaders)
    .then(res => dispatch({ type: GET_BANK_SUCCESS, payload: res.data }))
    .catch(err => dispatch({ type: GET_BANK_ERROR, payload: err.response && err.response.data }))
}
