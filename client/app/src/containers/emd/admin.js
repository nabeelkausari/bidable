import { connect } from 'react-redux';
import {
  getEMDs, approveEMD, rejectEMD, selectEMDToReject,
} from './actions';

function mapStateToProps(state) {
  return {
    ...state.emd,
  }
}

export default connect(mapStateToProps, {
  getEMDs, approveEMD, rejectEMD, selectEMDToReject,
})
