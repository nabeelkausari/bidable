import { connect } from 'react-redux';
import { submitEMD, selectEMDLot, clearEMDLot, clearEMDErrors, getMyEMDs, deleteEMD } from './actions';
import { getLotById } from "../lots/actions";
import { getAuctions } from "../auctions/actions"

function mapStateToProps(state) {
  return {
    ...state.emd,
    ...state.lots,
    ...state.auctions
  }
}

export default connect(mapStateToProps, {
  getAuctions, getLotById, submitEMD, selectEMDLot, clearEMDLot, clearEMDErrors,
  getMyEMDs, deleteEMD
})
