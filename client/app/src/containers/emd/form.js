import { reduxForm } from 'redux-form';
import { connect } from 'react-redux';

import { selectEMDLot } from './actions';

const validate = formProps => {
  const errors = {};

  if (!formProps.refNo) errors.refNo = 'Please enter transaction ref number'
  if (!formProps.amount) errors.amount = 'Please enter the deposited amount'
  return errors;
}

export default Comp => {
  Comp = reduxForm({
    form: 'emdForm',
    validate
  })(Comp);

  return connect(null, { selectEMDLot })(Comp)
}
