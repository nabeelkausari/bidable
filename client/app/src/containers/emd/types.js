export const SUBMIT_EMD = "SUBMIT_EMD";
export const SUBMIT_EMD_SUCCESS = "SUBMIT_EMD_SUCCESS";
export const SUBMIT_EMD_ERROR = "SUBMIT_EMD_ERROR";

export const DELETE_EMD = "DELETE_EMD";
export const DELETE_EMD_SUCCESS = "DELETE_EMD_SUCCESS";
export const DELETE_EMD_ERROR = "DELETE_EMD_ERROR";

export const SELECT_EMD_LOT = "SELECT_EMD_LOT";
export const CLEAR_EMD_LOT = "CLEAR_EMD_LOT";

export const CLEAR_EMD_ERRORS = "CLEAR_EMD_ERRORS";

export const GET_MY_EMDS = "GET_MY_EMDS";
export const GET_MY_EMDS_SUCCESS = "GET_MY_EMDS_SUCCESS";
export const GET_MY_EMDS_ERROR = "GET_MY_EMDS_ERROR";

export const SUBMIT_BANK = "SUBMIT_BANK";
export const SUBMIT_BANK_SUCCESS = "SUBMIT_BANK_SUCCESS";
export const SUBMIT_BANK_ERROR = "SUBMIT_BANK_ERROR";

export const GET_BANK = "GET_BANK";
export const GET_BANK_SUCCESS = "GET_BANK_SUCCESS";
export const GET_BANK_ERROR = "GET_BANK_ERROR";

export const GET_EMDS = "GET_EMDS";
export const GET_EMDS_SUCCESS = "GET_EMDS_SUCCESS";
export const GET_EMDS_ERROR = "GET_EMDS_ERROR";

export const REJECT_EMD = "REJECT_EMD";
export const REJECT_EMD_SUCCESS = "REJECT_EMD_SUCCESS";
export const REJECT_EMD_ERROR = "REJECT_EMD_ERROR";

export const APPROVE_EMD = "APPROVE_EMD";
export const APPROVE_EMD_SUCCESS = "APPROVE_EMD_SUCCESS";
export const APPROVE_EMD_ERROR = "APPROVE_EMD_ERROR";

export const EMD_TO_REJECT = "EMD_TO_REJECT";
