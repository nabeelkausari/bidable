import axios from 'axios';

import { HOST, getAuthHeaders } from "../../../constants";
import {
  AUTH_SELLER, AUTH_SELLER_ERROR, AUTH_SELLER_SUCCESS, LOGOUT_SELLER,
  REGISTER_SELLER, REGISTER_SELLER_ERROR, REGISTER_SELLER_SUCCESS,
  SUBMIT_SELLER_COMPANY, SUBMIT_SELLER_COMPANY_ERROR, SUBMIT_SELLER_COMPANY_SUCCESS
} from "./types"

export const loginSeller = ({ email, password, redirect }) => dispatch => {
  dispatch({ type: AUTH_SELLER })
  axios.post(`${HOST}/api/seller/login`, { email, password })
    .then(res => doLogin(dispatch, res, redirect))
    .catch(err => dispatch(authError(err.response && err.response.data)))
}

export const registerSeller = ({ email, password, username, redirect, mobile }) => dispatch => {
  dispatch({ type: REGISTER_SELLER })
  axios.post(`${HOST}/api/seller/register`, { email, password, username, mobile })
    .then(res => {
      dispatch({ type: REGISTER_SELLER_SUCCESS, payload: res.data.user })
      redirect();
    })
    .catch(err => {
      dispatch({
        type: REGISTER_SELLER_ERROR,
        payload: err.response && err.response.data && err.response.data.error
      })
    })
}

export const submitSellerCompany = ({ ...props, redirect }) => dispatch => {
  dispatch({ type: SUBMIT_SELLER_COMPANY })
  axios.post(`${HOST}/api/seller/submit-company`, props, getAuthHeaders())
    .then(res => {
      dispatch({ type: SUBMIT_SELLER_COMPANY_SUCCESS, payload: res.data })
      redirect();
    })
    .catch(err => {
      dispatch({
        type: SUBMIT_SELLER_COMPANY_ERROR,
        payload:err.response && err.response.data && err.response.data.error
      })
    })
}

const doLogin = (dispatch, res, redirect) => {
  dispatch({ type: AUTH_SELLER_SUCCESS, payload: res.data.user })
  localStorage.setItem('token', res.data.jwt)
  localStorage.setItem('type', 'seller')
  redirect(res.data.user);
}

export const logoutSeller = ({ redirect }) => {
  localStorage.removeItem('token');
  localStorage.removeItem('type');
  redirect()
  return { type: LOGOUT_SELLER }
}

export const isAuthenticated = ({ redirect }) => dispatch => {
  dispatch({ type: AUTH_SELLER })
  let jwt = localStorage.getItem('token');
  let type = localStorage.getItem('type');
  if (!jwt || !type) {
    dispatch(logoutSeller({ redirect }))
  }
  axios.post(`${HOST}/api/getMe`, { jwt })
    .then(res => {
      if (!res.data.user) {
        dispatch(logoutSeller({ redirect }))
      } else {
        dispatch({ type: AUTH_SELLER_SUCCESS, payload: res.data.user })
      }
    })
    .catch(() => dispatch(authError(null, redirect)))
}

const authError = (error, redirect) => {
  if (redirect) redirect()
  return { type: AUTH_SELLER_ERROR, payload: error };
}
