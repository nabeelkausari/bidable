import { connect } from 'react-redux';

import {logoutSeller} from './actions';

export default connect(null, { logoutSeller })
