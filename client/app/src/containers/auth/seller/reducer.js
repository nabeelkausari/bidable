import {
  AUTH_SELLER, AUTH_SELLER_ERROR, AUTH_SELLER_SUCCESS, LOGOUT_SELLER,
  REGISTER_SELLER, REGISTER_SELLER_SUCCESS, REGISTER_SELLER_ERROR,
  SUBMIT_SELLER_COMPANY, SUBMIT_SELLER_COMPANY_ERROR, SUBMIT_SELLER_COMPANY_SUCCESS
} from "./types"
import {CLEAR_AUTH_ERRORS} from "../common/types"

export default (state = {}, { type, payload }) => {
  switch (type) {
    case AUTH_SELLER:
      return { ...state, authSellerLoading: true, authSellerError: null };
    case AUTH_SELLER_SUCCESS:
      return { ...state, authSellerLoading: false, sellerAuthenticated: true, user: payload };
    case AUTH_SELLER_ERROR:
      return { ...state, authSellerLoading: false, sellerAuthenticated: false, authSellerError: payload };

    case REGISTER_SELLER:
      return { ...state, registerSellerLoading: true, registerSellerError: null };
    case REGISTER_SELLER_SUCCESS:
      return { ...state, registerSellerLoading: false, };
    case REGISTER_SELLER_ERROR:
      return { ...state, registerSellerLoading: false, registerSellerError: payload };

    case SUBMIT_SELLER_COMPANY:
      return { ...state, submitSellerCompanyLoading: true, submitSellerCompanyError: null };
    case SUBMIT_SELLER_COMPANY_SUCCESS:
      return { ...state, submitSellerCompanyLoading: false, user: { ...state.user, ...payload }};
    case SUBMIT_SELLER_COMPANY_ERROR:
      return { ...state, submitSellerCompanyLoading: false, submitSellerCompanyError: payload };

    case LOGOUT_SELLER:
      return { ...state, sellerAuthenticated: false };
    case CLEAR_AUTH_ERRORS:
      return { ...state, registerSellerError: null, authSellerError: null, submitSellerCompanyError: null }
    default:
      return state;
  }
}
