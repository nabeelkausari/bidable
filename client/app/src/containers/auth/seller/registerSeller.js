import { connect } from 'react-redux';

import {registerSeller} from './actions';
import { clearAuthErrors } from "../common/actions"

function mapStateToProps(state) {
  return { ...state.sellerAuth }
}

export default connect(mapStateToProps, { registerSeller, clearAuthErrors })
