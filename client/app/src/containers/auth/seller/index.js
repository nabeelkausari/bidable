import { connect } from 'react-redux';
import { logoutSeller, loginSeller, isAuthenticated } from './actions';

function mapStateToProps(state) {
  return { ...state.sellerAuth }
}

export default connect(mapStateToProps, { logoutSeller, loginSeller, isAuthenticated })
