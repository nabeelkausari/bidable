import { reduxForm } from 'redux-form';
import { connect } from 'react-redux';

import {submitSellerCompany} from './actions';

function mapStateToProps(state) {
  return { ...state.sellerAuth }
}

export const validate = formProps => {
  const errors = {};

  if (!formProps.companyName) errors.companyName = 'Please enter the company name';
  if (!formProps.pan) errors.pan = 'Please enter the PAN number';
  if (!formProps.gstin) errors.gstin = 'Please enter the GSTIN number';
  return errors;
}

export default Comp => {
  const register = connect(mapStateToProps, { submitSellerCompany })(Comp)

  return reduxForm({
    form: 'registerSellerCompany',
    validate,
  })(register);
}
