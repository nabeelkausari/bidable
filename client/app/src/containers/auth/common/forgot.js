import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';

import { requestResetPassword } from './actions';
import { clearAuthErrors } from "./actions"

const validate = formProps => {
  const errors = {};
  if (!formProps.email) errors.email = 'Please enter an email'
  return errors;
}

const mapStateToProps = state => {
  return {
    requestResetError: state.auth.requestResetErr,
    requestResetLoading: state.auth.requestResetLoading,
    requestResetSuccess: state.auth.requestResetSuccess
  }
}

export default Comp => {
  const forgot = reduxForm({
    form: 'forgot',
    validate
  })(Comp);

  return connect(mapStateToProps, { requestResetPassword, clearAuthErrors })(forgot);
}
