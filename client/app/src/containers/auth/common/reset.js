import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';

import { resetPassword } from './actions';
import { clearAuthErrors } from "./actions"

const validate = formProps => {
  const errors = {};
  if (!formProps.password) errors.password = 'Please enter a password'
  if (formProps.password && formProps.password.length < 6) errors.password = 'Password should be at least 6 digit'
  if (!formProps.confirmPassword) errors.confirmPassword = 'Please enter a password confirmation'

  if (formProps.password !== formProps.confirmPassword) {
    errors.password = 'Passwords must match'
  }
  return errors;
}

const mapStateToProps = state => {
  return {
    resetError: state.auth.resetErr,
    resetLoading: state.auth.resetLoading,
    resetSuccess: state.auth.resetSuccess
  }
}

export default Comp => {
  const reset = reduxForm({
    form: 'reset',
    validate
  })(Comp);

  return connect(mapStateToProps, { resetPassword, clearAuthErrors })(reset);
}
