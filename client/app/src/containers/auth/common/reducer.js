import {
  REQUEST_RESET_PASSWORD, REQUEST_RESET_PASSWORD_ERROR, REQUEST_RESET_PASSWORD_SUCCESS,
  RESET_PASSWORD, RESET_PASSWORD_ERROR, RESET_PASSWORD_SUCCESS, FORGOT_PASSWORD_OTP, FORGOT_PASSWORD_OTP_ERROR,
  FORGOT_PASSWORD_OTP_SUCCESS, RESET_PASSWORD_OTP, RESET_PASSWORD_OTP_ERROR, RESET_PASSWORD_OTP_SUCCESS
} from "./types"

export default (state = {}, { type, payload }) => {
  switch (type) {
    case REQUEST_RESET_PASSWORD:
      return { ...state, requestResetLoading: true, requestResetErr: null };
    case REQUEST_RESET_PASSWORD_SUCCESS:
      return { ...state, requestResetLoading: false, requestResetSuccess: true };
    case REQUEST_RESET_PASSWORD_ERROR:
      return { ...state, requestResetLoading: false, requestResetSuccess: false, requestResetErr: payload };

    case RESET_PASSWORD:
      return { ...state, resetLoading: true, resetErr: null };
    case RESET_PASSWORD_SUCCESS:
      return { ...state, resetLoading: false, resetSuccess: true };
    case RESET_PASSWORD_ERROR:
      return { ...state, resetLoading: false, resetSuccess: false, resetErr: payload };

    case FORGOT_PASSWORD_OTP:
      return { ...state, forgotPassOTPLoading: true, forgotPassOTPErr: null };
    case FORGOT_PASSWORD_OTP_SUCCESS:
      return { ...state, forgotPassOTPLoading: false, forgotPassOTPSuccess: true };
    case FORGOT_PASSWORD_OTP_ERROR:
      return { ...state, forgotPassOTPLoading: false, forgotPassOTPSuccess: false, forgotPassOTPErr: payload };

    case RESET_PASSWORD_OTP:
      return { ...state, resetPassOTPLoading: true, resetPassOTPErr: null };
    case RESET_PASSWORD_OTP_SUCCESS:
      return { ...state, resetPassOTPLoading: false, resetPassOTPSuccess: true };
    case RESET_PASSWORD_OTP_ERROR:
      return { ...state, resetPassOTPLoading: false, resetPassOTPSuccess: false, resetPassOTPErr: payload };

    default:
      return state;
  }
}
