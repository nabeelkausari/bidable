import axios from 'axios';

import { HOST } from "../../../constants";
import {
  REQUEST_RESET_PASSWORD,
  RESET_PASSWORD_SUCCESS,
  RESET_PASSWORD_ERROR,
  RESET_PASSWORD,
  REQUEST_RESET_PASSWORD_SUCCESS,
  REQUEST_RESET_PASSWORD_ERROR,
  FORGOT_PASSWORD_OTP,
  FORGOT_PASSWORD_OTP_ERROR,
  FORGOT_PASSWORD_OTP_SUCCESS,
  RESET_PASSWORD_OTP,
  RESET_PASSWORD_OTP_ERROR,
  RESET_PASSWORD_OTP_SUCCESS,
  CLEAR_AUTH_ERRORS
} from "./types"

export const requestResetPassword = ({ email, redirect }) => dispatch => {
  dispatch({ type: REQUEST_RESET_PASSWORD })
  axios.post(`${HOST}/api/requestResetPassword`, { email })
    .then(res => {
      dispatch({ type: REQUEST_RESET_PASSWORD_SUCCESS })
      redirect();
    })
    .catch(err => {
      dispatch({
        type: REQUEST_RESET_PASSWORD_ERROR,
        payload: err.response && err.response.data && err.response.data.error
      })
    })
}

export const resetPassword = ({ password, token, redirect }) => dispatch => {
  dispatch({ type: RESET_PASSWORD })
  axios.post(`${HOST}/api/resetPassword`, { password, token })
    .then(res => {
      if (res.data.error) {
        dispatch({
          type: RESET_PASSWORD_ERROR,
          payload: res.data.error
        })
      } else {
        dispatch({ type: RESET_PASSWORD_SUCCESS })
        redirect();
      }
    })
    .catch(err => {
      dispatch({
        type: RESET_PASSWORD_ERROR,
        payload: err.response && err.response.data && err.response.data.error
      })
    })
}


export const forgotPasswordOTP = ({ email, redirect }) => dispatch => {
  dispatch({ type: FORGOT_PASSWORD_OTP })
  axios.post(`${HOST}/api/forgotPasswordOTP`, { email })
    .then(res => {
      dispatch({ type: FORGOT_PASSWORD_OTP_SUCCESS })
      redirect();
    })
    .catch(err => {
      dispatch({
        type: FORGOT_PASSWORD_OTP_ERROR,
        payload: err.response && err.response.data && err.response.data.error
      })
    })
}

export const resetPasswordOTP = ({ password, otp, redirect }) => dispatch => {
  dispatch({ type: RESET_PASSWORD_OTP })
  axios.post(`${HOST}/api/resetPasswordOTP`, { password, otp })
    .then(res => {
      if (res.data.error) {
        dispatch({
          type: RESET_PASSWORD_OTP_ERROR,
          payload: res.data.error
        })
      } else {
        dispatch({ type: RESET_PASSWORD_OTP_SUCCESS })
        redirect();
      }
    })
    .catch(err => {
      dispatch({
        type: RESET_PASSWORD_ERROR,
        payload: err.response && err.response.data && err.response.data.error
      })
    })
}

export const clearAuthErrors = () => ({ type: CLEAR_AUTH_ERRORS });
