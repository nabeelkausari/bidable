import { connect } from 'react-redux';
import { logoutAdmin, loginAdmin, isAuthenticated } from './actions';

function mapStateToProps(state) {
  return {
    ...state.adminAuth
  }
}

export default connect(mapStateToProps, { logoutAdmin, loginAdmin, isAuthenticated })
