import {
  AUTH_ADMIN, AUTH_ADMIN_ERROR, AUTH_ADMIN_SUCCESS, LOGOUT_ADMIN,
} from "./types"
import {CLEAR_AUTH_ERRORS} from "../common/types"

export default (state = {}, { type, payload }) => {
  switch (type) {
    case AUTH_ADMIN:
      return { ...state, authAdminLoading: true, authAdminError: null };
    case AUTH_ADMIN_SUCCESS:
      return { ...state, authAdminLoading: false, adminAuthenticated: true, user: payload };
    case AUTH_ADMIN_ERROR:
      return { ...state, authAdminLoading: false, adminAuthenticated: false, authAdminError: payload };

    case LOGOUT_ADMIN:
      return { ...state, adminAuthenticated: false };

    case CLEAR_AUTH_ERRORS:
      return { ...state, authAdminError: null };
    default:
      return state;
  }
}
