import { connect } from 'react-redux';

import {registerBuyer} from './actions';
import { clearAuthErrors } from "../common/actions";

function mapStateToProps(state) {
  return { ...state.buyerAuth }
}

export default connect(mapStateToProps, { registerBuyer, clearAuthErrors })
