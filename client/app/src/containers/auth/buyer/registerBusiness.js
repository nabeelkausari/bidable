import { reduxForm } from 'redux-form';
import { connect } from 'react-redux';

import {submitBuyerBusiness} from './actions';
import { getCategories } from "../../dashboard/actions"

function mapStateToProps(state) {
  return {
    ...state.sellerAuth,
    ...state.dashboard
  }
}

export const validate = formProps => {
  const errors = {};

  if (!formProps.companyName) errors.companyName = 'Please enter the company name';
  if (!formProps.categories || formProps.categories.length === 0) errors.categories = 'Please select at least one category';
  return errors;
}

export default Comp => {
  const register = connect(mapStateToProps, { submitBuyerBusiness, getCategories })(Comp)

  return reduxForm({
    form: 'registerBuyerBusiness',
    validate,
  })(register);
}
