import { connect } from 'react-redux';

import {logoutBuyer} from './actions';

function mapStateToProps(state) {
  return {
    ...state.buyerAuth
  }
}

export default connect(mapStateToProps, { logoutBuyer })
