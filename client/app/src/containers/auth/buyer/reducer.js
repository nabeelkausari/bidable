import {
  AUTH_BUYER, AUTH_BUYER_ERROR, AUTH_BUYER_SUCCESS, LOGOUT_BUYER,
  REGISTER_BUYER, REGISTER_BUYER_SUCCESS, REGISTER_BUYER_ERROR,
  SUBMIT_BUYER_BUSINESS, SUBMIT_BUYER_BUSINESS_SUCCESS, SUBMIT_BUYER_BUSINESS_ERROR
} from "./types"
import {CLEAR_AUTH_ERRORS} from "../common/types"

export default (state = {}, { type, payload }) => {
  switch (type) {
    case AUTH_BUYER:
      return { ...state, authBuyerLoading: true, authBuyerError: null };
    case AUTH_BUYER_SUCCESS:
      return { ...state, authBuyerLoading: false, buyerAuthenticated: true, user: payload };
    case AUTH_BUYER_ERROR:
      return { ...state, authBuyerLoading: false, buyerAuthenticated: false, authBuyerError: payload };

    case REGISTER_BUYER:
      return { ...state, registerSellerLoading: true, registerBuyerError: null };
    case REGISTER_BUYER_SUCCESS:
      return { ...state, registerSellerLoading: false, };
    case REGISTER_BUYER_ERROR:
      return { ...state, registerSellerLoading: false, registerBuyerError: payload };

    case LOGOUT_BUYER:
      return { ...state, authBuyerLoading: false, buyerAuthenticated: false };

    case SUBMIT_BUYER_BUSINESS:
      return { ...state, submitBuyerBusinessLoading: true, submitBuyerBusinessError: null };
    case SUBMIT_BUYER_BUSINESS_SUCCESS:
      return { ...state, submitBuyerBusinessLoading: false, user: { ...state.user, ...payload }};
    case SUBMIT_BUYER_BUSINESS_ERROR:
      return { ...state, submitBuyerBusinessLoading: false, submitBuyerBusinessError: payload };

    case CLEAR_AUTH_ERRORS:
      return { ...state, authBuyerError: null, registerBuyerError: null };
    default:
      return state;
  }
}
