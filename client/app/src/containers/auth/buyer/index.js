import { connect } from 'react-redux';
import { logoutBuyer, loginBuyer, isAuthenticated } from './actions';

function mapStateToProps(state) {
  return { ...state.buyerAuth }
}

export default connect(mapStateToProps, { logoutBuyer, loginBuyer, isAuthenticated })
