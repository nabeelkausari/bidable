import { reduxForm } from 'redux-form';
import { connect } from 'react-redux';

import {loginBuyer} from './actions';
import { clearAuthErrors } from "../common/actions";

const validate = formProps => {
  const errors = {};

  if (!formProps.email) errors.email = 'Please enter an email'
  if (!formProps.password) errors.password = 'Please enter a password'
  return errors;
}

function mapStateToProps(state) {
  return { ...state.buyerAuth }
}

export default Comp => {
  const login = connect(mapStateToProps, { loginBuyer, clearAuthErrors })(Comp)

  return reduxForm({
    form: 'loginBuyer',
    validate
  })(login);
}
