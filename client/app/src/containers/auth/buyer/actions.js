import axios from 'axios';

import {getAuthHeaders, HOST} from "../../../constants";
import {
  AUTH_BUYER, AUTH_BUYER_ERROR, AUTH_BUYER_SUCCESS, LOGOUT_BUYER,
  REGISTER_BUYER, REGISTER_BUYER_SUCCESS, REGISTER_BUYER_ERROR,
  SUBMIT_BUYER_BUSINESS, SUBMIT_BUYER_BUSINESS_ERROR, SUBMIT_BUYER_BUSINESS_SUCCESS
} from "./types"

export const registerBuyer = ({ email, password, mobile, redirect }) => dispatch => {
  dispatch({ type: REGISTER_BUYER })
  axios.post(`${HOST}/api/buyer/register`, { email, password, mobile })
    .then(res => {
      dispatch({ type: REGISTER_BUYER_SUCCESS, payload: res.data.user })
      redirect();
    })
    .catch(err => {
      dispatch({
        type: REGISTER_BUYER_ERROR,
        payload: err.response && err.response.data && err.response.data.error
      })
    })
}

export const loginBuyer = ({ email, password, redirect }) => dispatch => {
  dispatch({ type: AUTH_BUYER })
  axios.post(`${HOST}/api/buyer/login`, { email, password })
    .then(res => doLogin(dispatch, res, redirect))
    .catch(err => dispatch(authError(err.response && err.response.data)))
}

const doLogin = (dispatch, res, redirect) => {
  dispatch({ type: AUTH_BUYER_SUCCESS, payload: res.data.user })
  localStorage.setItem('token', res.data.jwt)
  localStorage.setItem('type', 'buyer')
  redirect(res.data.user);
}

export const logoutBuyer = ({ redirect }) => {
  localStorage.removeItem('token');
  localStorage.removeItem('type');
  redirect()
  return { type: LOGOUT_BUYER }
}

export const isAuthenticated = ({ redirect }) => dispatch => {
  dispatch({ type: AUTH_BUYER })
  let jwt = localStorage.getItem('token');
  let type = localStorage.getItem('type');
  if (!jwt || !type) {
    dispatch(logoutBuyer({ redirect }))
  }
  axios.post(`${HOST}/api/getMe`, { jwt })
    .then(res => {
      if (!res.data.user) {
        dispatch(logoutBuyer({ redirect }))
      } else {
        dispatch({ type: AUTH_BUYER_SUCCESS, payload: res.data.user })
      }
    })
    .catch(() => dispatch(authError(null, redirect)))
}

const authError = (err, redirect) => {
  if (redirect) redirect()
  return { type: AUTH_BUYER_ERROR, payload: err };
}

export const submitBuyerBusiness = ({ ...props, redirect }) => dispatch => {
  dispatch({ type: SUBMIT_BUYER_BUSINESS })
  axios.post(`${HOST}/api/buyer/submit-business`, props, getAuthHeaders())
    .then(res => {
      dispatch({ type: SUBMIT_BUYER_BUSINESS_SUCCESS, payload: res.data })
      redirect();
    })
    .catch(err => {
      dispatch({
        type: SUBMIT_BUYER_BUSINESS_ERROR,
        payload:err.response && err.response.data && err.response.data.error
      })
    })
}
