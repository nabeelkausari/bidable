import axios from 'axios';
import { isEmpty } from 'lodash';

import {HOST, emailRegEx} from "../../constants"

export const asyncValidate = async (values, dispatch, props, field)  => {
  let errors = { ...props.asyncErrors }
  if (field === "mobile") {

    let { data } = await axios.get(`${HOST}/api/checkMobile?mobile=${values.mobile}`)
    if (data.taken) errors.mobile = 'This mobile number is already registered';

  } else if (field === "email") {

    let { data } = await axios.get(`${HOST}/api/checkEmail?email=${values.email}`);
    if (data.taken) errors.email = 'This email is already registered';

  }

  return new Promise((resolve, reject) => {
    if (!isEmpty(errors)) {
      reject(errors);
    }

    resolve();
  });

}

export const validate = formProps => {
  const errors = {};

  if (!formProps.email) errors.email = 'Please enter an email'
  if (!formProps.password) errors.password = 'Please enter a password'
  if (!formProps.confirmPassword) errors.confirmPassword = 'Please enter a password confirmation'
  if (!formProps.mobile) errors.mobile = 'Please enter your mobile number'

  if (formProps.email && !emailRegEx.test(formProps.email)) errors.email = 'Please enter a valid email'
  if (formProps.password && formProps.password.length < 6) errors.password = 'Password should be at least 6 digit'
  if (formProps.mobile && formProps.mobile.length !== 10) errors.mobile = 'Please enter a valid mobile number (only 10 digits)'

  if (formProps.password !== formProps.confirmPassword) {
    errors.password = 'Passwords must match'
  }

  return errors;
}
