import axios from 'axios';

import { HOST } from "../../constants";
import { GET_SELLER_QUOTATIONS, GET_SELLER_QUOTATIONS_ERROR, GET_SELLER_QUOTATIONS_SUCCESS } from "./types";

export const getSellerQuotations = (authHeaders) => dispatch => {
  dispatch({ type: GET_SELLER_QUOTATIONS })
  axios.get(`${HOST}/api/seller/quotations`, authHeaders)
    .then(res => dispatch({ type: GET_SELLER_QUOTATIONS_SUCCESS, payload: res.data}))
    .catch(err => dispatch({ type: GET_SELLER_QUOTATIONS_ERROR, payload: err.response && err.response.data}))
}
