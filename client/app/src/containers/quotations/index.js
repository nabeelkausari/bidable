import { connect } from 'react-redux';
import { getSellerQuotations } from './actions';

function mapStateToProps(state) {
  return {
    ...state.quotations
  }
}

export default connect(mapStateToProps, { getSellerQuotations })
