import { GET_SELLER_QUOTATIONS, GET_SELLER_QUOTATIONS_ERROR, GET_SELLER_QUOTATIONS_SUCCESS } from "./types";

export default (state = {}, { type, payload }) => {
  switch (type) {

    case GET_SELLER_QUOTATIONS:
      return { ...state, quotationsLoading: true, quotationsError: null };
    case GET_SELLER_QUOTATIONS_SUCCESS:
      return { ...state, quotationsLoading: false, quotations: payload };
    case GET_SELLER_QUOTATIONS_ERROR:
      return { ...state, quotationsLoading: false, quotationsError: payload && payload.error };

    default:
      return state;
  }
}
