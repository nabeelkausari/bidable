import { connect } from 'react-redux';
import { getClaims, confirmBonus } from './actions';

function mapStateToProps(state) {
  return { ...state.users }
}

export default connect(mapStateToProps, { getClaims, confirmBonus })
