import {
  GET_USERS, GET_USERS_ERROR, GET_USERS_SUCCESS,
  GET_CLAIMS, GET_CLAIMS_ERROR, GET_CLAIMS_SUCCESS
} from "./types";

export default (state = {}, { type, payload }) => {
  switch (type) {
    case GET_USERS:
      return { ...state, getUserLoading: true, error: null };
    case GET_USERS_SUCCESS:
      return { ...state, getUserLoading: false, users: payload };
    case GET_USERS_ERROR:
      return { ...state, getUserLoading: false, error: payload.data };

    case GET_CLAIMS:
      return { ...state, getClaimsLoading: true, getClaimsError: null };
    case GET_CLAIMS_SUCCESS:
      return { ...state, getClaimsLoading: false, claims: payload }
    case GET_CLAIMS_ERROR:
      return { ...state, getClaimsLoading: false, getClaimsError: payload.data }

    default:
      return state;
  }
}
