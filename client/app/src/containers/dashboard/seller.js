import { connect } from 'react-redux';
import { getSellerStats, getCategories } from './actions';
import { logoutSeller } from "../auth/seller/actions"

function mapStateToProps(state) {
  return {
    ...state.dashboard,
    ...state.sellerAuth
  }
}

export default connect(mapStateToProps, { getSellerStats, getCategories, logoutSeller })
