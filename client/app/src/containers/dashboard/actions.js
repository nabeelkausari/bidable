import axios from 'axios';

import {getAuthHeaders, HOST} from "../../constants"
import {
  GET_SELLER_STATS, GET_SELLER_STATS_ERROR, GET_SELLER_STATS_SUCCESS,
  GET_CATEGORIES, GET_CATEGORIES_ERROR, GET_CATEGORIES_SUCCESS, GET_BUYER_STATS,
  GET_BUYER_STATS_ERROR, GET_BUYER_STATS_SUCCESS
} from "./types"

export const getSellerStats = () => dispatch => {
  dispatch({ type: GET_SELLER_STATS })
  axios.get(`${HOST}/api/seller/stats`, getAuthHeaders())
    .then(res =>  dispatch({ type: GET_SELLER_STATS_SUCCESS, payload: res.data }))
    .catch(err => dispatch({ type: GET_SELLER_STATS_ERROR, payload: err.response && err.response.data }))
}

export const getBuyerStats = authHeaders => dispatch => {
  dispatch({ type: GET_BUYER_STATS })
  axios.get(`${HOST}/api/buyer/stats`, authHeaders)
    .then(res =>  dispatch({ type: GET_BUYER_STATS_SUCCESS, payload: res.data }))
    .catch(err => dispatch({ type: GET_BUYER_STATS_ERROR, payload: err.response && err.response.data }))
}

export const getCategories = () => dispatch => {
  dispatch({ type: GET_CATEGORIES })
  axios.get(`${HOST}/api/categories`)
    .then(res =>  dispatch({ type: GET_CATEGORIES_SUCCESS, payload: res.data }))
    .catch(err => dispatch({ type: GET_CATEGORIES_ERROR, payload: err.response && err.response.data }))
}
