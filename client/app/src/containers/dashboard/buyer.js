import { connect } from 'react-redux';
import { getCategories, getBuyerStats } from "./actions";
import { logoutBuyer } from "../auth/buyer/actions";

function mapStateToProps(state) {
  return {
    ...state.dashboard,
    ...state.buyerAuth
  }
}

export default connect(mapStateToProps, { getCategories, getBuyerStats, logoutBuyer } )
