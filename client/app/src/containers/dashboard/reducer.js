import {
  GET_SELLER_STATS, GET_SELLER_STATS_ERROR, GET_SELLER_STATS_SUCCESS,
  GET_CATEGORIES, GET_CATEGORIES_SUCCESS, GET_CATEGORIES_ERROR,
  GET_BUYER_STATS_SUCCESS, GET_BUYER_STATS_ERROR, GET_BUYER_STATS
} from "./types"

export default (state = {}, { type, payload }) => {
  switch (type) {
    case GET_SELLER_STATS:
      return { ...state, sellerStatsLoading: true, sellerStatsError: null };
    case GET_SELLER_STATS_SUCCESS:
      return { ...state, sellerStatsLoading: false, sellerStats: payload };
    case GET_SELLER_STATS_ERROR:
      return { ...state, sellerStatsLoading: false, sellerStatsError: payload };

    case GET_BUYER_STATS:
      return { ...state, buyerStatsLoading: true, buyerStatsError: null };
    case GET_BUYER_STATS_SUCCESS:
      return { ...state, buyerStatsLoading: false, buyerStats: payload };
    case GET_BUYER_STATS_ERROR:
      return { ...state, buyerStatsLoading: false, buyerStatsError: payload };

    case GET_CATEGORIES:
      return { ...state, categoriesLoading: true, categoriesError: null };
    case GET_CATEGORIES_SUCCESS:
      return { ...state, categoriesLoading: false, categories: payload };
    case GET_CATEGORIES_ERROR:
      return { ...state, categoriesLoading: false, categoriesError: payload };

    default:
      return state;
  }
}
