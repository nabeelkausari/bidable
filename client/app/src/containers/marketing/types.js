export const SEND_MAIL = "SEND_MAIL";
export const SEND_MAIL_SUCCESS = "SEND_MAIL_SUCCESS";
export const SEND_MAIL_ERROR = "SEND_MAIL_ERROR";

export const GET_MAILS = "GET_MAILS";
export const GET_MAILS_SUCCESS = "GET_MAILS_SUCCESS";
export const GET_MAILS_ERROR = "GET_MAILS_ERROR";

export const CLEAR_MAIL_ERRORS = "CLEAR_MAIL_ERRORS";
