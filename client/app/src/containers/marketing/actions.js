import axios from 'axios';

import { HOST } from "../../constants";
import {
  SEND_MAIL, CLEAR_MAIL_ERRORS, SEND_MAIL_ERROR, SEND_MAIL_SUCCESS,
  GET_MAILS, GET_MAILS_ERROR, GET_MAILS_SUCCESS
} from "./types";

export const clearSendMailErrors = () => {
  return { type: CLEAR_MAIL_ERRORS }
}

export const sendMail = (mail, authHeaders) => dispatch => {
  dispatch({ type: SEND_MAIL })
  axios.post(`${HOST}/api/admin/mails`, mail, authHeaders)
    .then(res => {
      dispatch({ type: SEND_MAIL_SUCCESS, payload: res.data})
      if (mail.redirect) mail.redirect();
    })
    .catch(err => dispatch({ type: SEND_MAIL_ERROR, payload: err.response && err.response.data}))
}

export const getMails = (authHeaders) => dispatch => {
  dispatch({ type: GET_MAILS })
  axios.get(`${HOST}/api/admin/mails`, authHeaders)
    .then(res => dispatch({ type: GET_MAILS_SUCCESS, payload: res.data }))
    .catch(err => dispatch({ type: GET_MAILS_ERROR, payload: err.response && err.response.data }))
}
