import {
  SEND_MAIL, CLEAR_MAIL_ERRORS, SEND_MAIL_ERROR, SEND_MAIL_SUCCESS,
  GET_MAILS, GET_MAILS_ERROR, GET_MAILS_SUCCESS
} from "./types";

export default (state = {}, { type, payload }) => {
  switch (type) {
    case SEND_MAIL:
      return { ...state, sendMailLoading: true, sendMailError: null };
    case SEND_MAIL_SUCCESS:
      return { ...state, sendMailLoading: false, sendMailSuccess: true };
    case SEND_MAIL_ERROR:
      return { ...state, sendMailLoading: false, sendMailError: payload.error, sendMailSuccess: false };

    case CLEAR_MAIL_ERRORS:
      return { ...state, sendMailError: null, mailsError: null };

    case GET_MAILS:
      return { ...state, mailsLoading: true, mailsError: null };
    case GET_MAILS_SUCCESS:
      return { ...state, mailsLoading: false, mails: payload };
    case GET_MAILS_ERROR:
      return { ...state, mailsLoading: false, mailsError: payload.error };

    default:
      return state;
  }
}
