import { reduxForm } from 'redux-form';

const validate = formProps => {
  const errors = {};

  if (!formProps.recipient) errors.recipient = 'Please enter the recipient email address'
  if (!formProps.subject) errors.subject = 'Please enter the mail subject'

  if (!formProps.body || !formProps.body.length || !formProps.body[0] ) {
    errors.body = 'At least one paragraph is required'
  }

  return errors;
}

export default reduxForm({
  form: 'mailForm',
  validate
})
