import { connect } from 'react-redux';
import { clearSendMailErrors, sendMail, getMails } from './actions';

function mapStateToProps(state) {
  return {
    ...state.marketing,
    ...state.adminAuth
  }
}

export default connect(mapStateToProps, {
  clearSendMailErrors, sendMail, getMails
})
