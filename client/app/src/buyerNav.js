export default {
  items: [
    {
      name: 'Dashboard',
      url: '/buyer/dashboard',
      icon: 'icon-speedometer'
    },
    {
      title: true,
      name: 'Auctions',
      wrapper: {            // optional wrapper object
        element: '',        // required valid HTML5 element tag
        attributes: {}        // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
      },
      class: ''             // optional class names space delimited list for title item ex: "text-center"
    },
    {
      name: 'Active Auctions',
      url: '/buyer/auctions',
      // icon: 'icon-drop',
    },
    {
      name: 'Past Auctions',
      url: '/buyer/past-auctions',
      // icon: 'icon-drop',
    },
    {
      title: true,
      name: 'Reports',
      wrapper: {            // optional wrapper object
        element: '',        // required valid HTML5 element tag
        attributes: {}        // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
      },
      class: ''             // optional class names space delimited list for title item ex: "text-center"
    },
    {
      name: 'My Bids',
      url: '/buyer/my-bids',
      // icon: 'icon-drop',
    },
    {
      name: 'My Quotations',
      url: '/buyer/my-quotations',
      // icon: 'icon-drop',
    },
    {
      name: 'My Sales',
      url: '/buyer/sales',
      // icon: 'icon-pencil',
    },
    {
      title: true,
      name: 'Online EMD',
      wrapper: {
        element: '',
        attributes: {},
      },
    },
    {
      name: 'Bank A/c Registration',
      url: '/buyer/bank-details',
      // icon: 'icon-drop',
    },
    {
      name: 'Pay EMD',
      url: '/buyer/submit-emd',
      // icon: 'icon-pencil',
    },
    {
      name: 'My EMD List',
      url: '/buyer/my-emd-list',
      // icon: 'icon-pencil',
    },
  //   {
  //     title: true,
  //     name: 'My Account',
  //     wrapper: {
  //       element: '',
  //       attributes: {},
  //     },
  //   },
  //   {
  //     name: 'Personal Details',
  //     icon: 'icon-user',
  //     children: [
  //       {
  //         name: 'Identity',
  //         url: '/buyer/identity',
  //         // icon: 'icon-puzzle',
  //       },
  //       {
  //         name: 'Renewal',
  //         url: '/buyer/renewal',
  //         // icon: 'icon-puzzle',
  //       },
  //       {
  //         name: 'Certificate Enroll',
  //         url: '/buyer/certificate-enroll',
  //         // icon: 'icon-puzzle',
  //       },
  //       {
  //         name: 'GST Registration',
  //         url: '/buyer/gst-registration',
  //         // icon: 'icon-puzzle',
  //       },
  //       {
  //         name: 'Change Password',
  //         url: '/buyer/change-password',
  //         // icon: 'icon-puzzle',
  //       },
  //       {
  //         name: 'Update Profile',
  //         url: '/buyer/updated-profile',
  //         // icon: 'icon-puzzle',
  //       }
  //     ],
  //   }
  ],
};
