export const gwei = 1000000000;
export const wei = 1000000000000000000;

export const ethToGwei = etherValue => etherValue * gwei;
export const gweiToEth = gweiValue => gweiValue / gwei;
export const ethToWei = etherValue => etherValue * wei;
export const weiToEth = weiValue => weiValue / wei;

