export default {
  items: [
    {
      name: 'Dashboard',
      url: '/seller/dashboard',
      icon: 'icon-speedometer'
    },
    {
      title: true,
      name: 'Auctions',
      wrapper: {            // optional wrapper object
        element: '',        // required valid HTML5 element tag
        attributes: {}        // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
      },
      class: ''             // optional class names space delimited list for title item ex: "text-center"
    },
    {
      name: 'Create New Auction',
      url: '/seller/create-auction',
      icon: 'icon-plus',
    },
    {
      name: 'My Auctions',
      url: '/seller/auctions',
      icon: 'icon-list',
    },
    {
      title: true,
      name: 'Reports',
      wrapper: {
        element: '',
        attributes: {},
      },
    },
    {
      name: 'Sales',
      url: '/seller/sales',
      icon: 'icon-basket-loaded',
    },
    {
      name: 'Quotations',
      url: '/seller/quotations',
      icon: 'icon-note',
    },
    // {
    //   title: true,
    //   name: 'My Account',
    //   wrapper: {
    //     element: '',
    //     attributes: {},
    //   },
    // },
    // {
    //   name: 'Company Details',
    //   icon: 'icon-user',
    //   children: [
    //     {
    //       name: 'Identity',
    //       url: '/seller/identity',
    //       // icon: 'icon-puzzle',
    //     },
    //     {
    //       name: 'Certificate Enroll',
    //       url: '/seller/certificate-enroll',
    //       // icon: 'icon-puzzle',
    //     },
    //     {
    //       name: 'GST Registration',
    //       url: '/seller/gst-registration',
    //       // icon: 'icon-puzzle',
    //     }
    //   ],
    // }
  ],
};
