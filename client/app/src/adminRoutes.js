import {
  AdminDashboard,
} from './components';

import SendMail from './components/Admin/Marketing/SendMail';
import MailList from './components/Admin/Marketing/Mails';
import ApprovedList from './components/Admin/Auctions/ApprovedList';
import ReviewList from './components/Admin/Auctions/ReviewList';
import AuctionDetail from './components/Admin/Auctions/View';
import AuctionReviewDetail from './components/Admin/Auctions/ReviewDetails';
import ReviewEMDList from './components/Admin/EMD/ReviewEMDList';
import RejectedEMDList from './components/Admin/EMD/RejectedEMDList';
import ApprovedEMDList from './components/Admin/EMD/ApprovedEMDList';
import ClearedEMDList from './components/Admin/EMD/ClearedEMDList';

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/admin/dashboard', name: 'Admin Dashboard', component: AdminDashboard },
  { path: '/admin/mails', name: 'Mail List', component: MailList },
  { path: '/admin/send-mail', name: 'Send Mail', component: SendMail },
  { exact: true, path: '/admin/auctions', name: 'Auctions To Review', component: ReviewList },
  { exact: true, path: '/admin/auctions/:auctionId', name: 'Auction Review Detail', component: AuctionReviewDetail },
  { exact: true, path: '/admin/auctions-approved', name: 'Approved Auctions', component: ApprovedList },
  { exact: true, path: '/admin/auctions-approved/:auctionId', name: 'Approved Auction Detail', component: AuctionDetail },
  { exact: true, path: '/admin/emds', name: 'Review EMD List', component: ReviewEMDList },
  { exact: true, path: '/admin/emds-rejected', name: 'Rejected EMD List', component: RejectedEMDList },
  { exact: true, path: '/admin/emds-approved', name: 'Approved EMD List', component: ApprovedEMDList },
  { exact: true, path: '/admin/emds-cleared', name: 'Cleared EMD List', component: ClearedEMDList },
];

export default routes;
