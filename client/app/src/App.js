import React, { Component } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './store';
import requireAdmin from './hocs/requireAdmin';
import requireBuyer from './hocs/requireBuyer';
import requireSeller from './hocs/requireSeller';
import requireUnAuth from './hocs/requireUnauth';
import './App.css';
// Styles
import 'flag-icon-css/css/flag-icon.min.css';
import 'font-awesome/css/font-awesome.min.css';
import 'simple-line-icons/css/simple-line-icons.css';
import 'flatpickr/dist/themes/airbnb.css'
import 'react-notifications/lib/notifications.css';
import 'animate.css/animate.min.css';
import 'react-select/dist/react-select.css';
import './scss/style.css'
// import '../node_modules/@coreui/styles/scss/_dropdown-menu-right.scss';

// Containers
import AdminLayout from './components/Admin/Layout'
import BuyerLayout from './components/Buyer/Layout'
import SellerLayout from './components/Seller/Layout'

// Pages
import Page404 from './components/Pages/Page404';
import Page500 from './components/Pages/Page500';
import {
  BuyerLogin, BuyerRegister, SellerRegister, SellerLogin,
  BuyerEmailConfirmed, SellerEmailConfirmed
} from './components/Pages';
import ForgotPassword from "./components/Common/ForgotPassword"
import ResetPassword from "./components/Common/ResetPassword"
import ResetError from "./components/Common/ResetPasswordError"
import LandingPage from "./components/LandingPage";
import AdminLogin from "./components/Admin/Login";

// import { renderRoutes } from 'react-router-config';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <BrowserRouter basename="/app">
          <Switch>
            <Route exact path="/404" name="Page 404" component={Page404} />
            <Route exact path="/500" name="Page 500" component={Page500} />
            <Route exact path="/" name="Landing Page" component={requireUnAuth(LandingPage)} />

            <Route path="/seller/forgot-password" component={requireUnAuth(ForgotPassword)} />
            <Route path="/seller/reset-password/:token" component={requireUnAuth(ResetPassword)} />
            <Route path="/seller/reset-password-error" component={requireUnAuth(ResetError)} />

            <Route path="/buyer/forgot-password" component={requireUnAuth(ForgotPassword)} />
            <Route path="/buyer/reset-password/:token" component={requireUnAuth(ResetPassword)} />
            <Route path="/buyer/reset-password-error" component={requireUnAuth(ResetError)} />

            <Route path="/seller/register" name="Seller Register Page" component={requireUnAuth(SellerRegister)} />
            <Route path="/seller/login" name="Seller Login Page" component={requireUnAuth(SellerLogin)} />
            <Route path="/seller/email-confirmed" name="Seller Email Confirmed" component={SellerEmailConfirmed} />

            <Route path="/buyer/register" name="Buyer Register Page" component={requireUnAuth(BuyerRegister)} />
            <Route path="/buyer/login" name="Buyer Login Page" component={requireUnAuth(BuyerLogin)} />
            <Route path="/buyer/email-confirmed" name="Buyer Email Confirmed" component={BuyerEmailConfirmed} />

            <Route path="/admin/login" name="Admin Login Page" component={requireUnAuth(AdminLogin)} />
            <Route path="/admin/forgot-password" component={requireUnAuth(ForgotPassword)} />
            <Route path="/admin/reset-password/:token" component={requireUnAuth(ResetPassword)} />
            <Route path="/admin/reset-password-error" component={requireUnAuth(ResetError)} />

            <Route path="/seller" name="Seller Home" component={requireSeller(SellerLayout)} />
            <Route path="/buyer" name="Buyer Home" component={requireBuyer(BuyerLayout)} />
            <Route path="/admin" name="Admin Home" component={requireAdmin(AdminLayout)} />
            {/*<Redirect from="/" to="/" />*/}
            <Redirect from="/" to="/404" />
          </Switch>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
