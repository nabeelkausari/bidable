import React, { Component } from 'react';
import { AppNavbarBrand } from '@coreui/react';
import { Nav, Container } from 'reactstrap';
import {NotificationContainer} from 'react-notifications';

import logo from '../assets/img/brand/logo.png'
import AuthContainer from '../containers/auth/seller/index'

class AuthLayout extends Component {
  render() {
    return (
      <div className="app-auth">
        <Nav className="auth-header" navbar>
          <AppNavbarBrand
            href="/app"
            full={{ src: logo, height: 42, alt: 'Bidable' }}
          />
        </Nav>
        <NotificationContainer/>
        <div className="app-body">
          <Container>
            <div className="app mx-auto">
              {this.props.children}
            </div>
          </Container>
        </div>
      </div>
    );
  }
}

export default AuthContainer(AuthLayout);
