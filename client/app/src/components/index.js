import AdminDashboard from './Admin/Dashboard';
import Users from './Users';

import SellerDashboard from './Seller/Dashboard'
import SellerMyAuctions from './Seller/Auction/MyAuctions'
import SellerCreateAuction from './Seller/Auction/CreateAuction'
import SellerEditAuction from './Seller/Auction/EditAuction'
import SellerAuctionDetails from './Seller/Auction/AuctionDetail'

import BuyerDashboard from './Buyer/Dashboard'
import UpcomingAuctions from './Buyer/Auction/UpcomingAuctions'
import PastAuctions from './Buyer/Auction/PastAuctions'
import AllAuctions from './Buyer/Auction/AllAuctions'
import AuctionDetail from './Buyer/Auction/AuctionDetail'
import LotDetail from './Buyer/Lot/LotDetail'
import BuyerMyBids from './Buyer/MyBids'
import BuyerMyQuotations from './Buyer/MyQuotations'
import BuyerSales from './Buyer/Sales'
import SubmitEMD from './Buyer/EMD/SubmitEMD'
import SelectLotEMD from './Buyer/EMD/SelectLotEMD'
import MyEMDs from './Buyer/EMD/MyEMDs'
import BankDetails from './Buyer/Bank/Details'

export {
  AdminDashboard,
  Users,
  SellerDashboard,
  SellerMyAuctions,
  SellerCreateAuction,
  SellerEditAuction,
  SellerAuctionDetails,

  BuyerDashboard,
  UpcomingAuctions,
  AllAuctions,
  PastAuctions,
  AuctionDetail,
  LotDetail,
  SubmitEMD,
  SelectLotEMD,
  MyEMDs,
  BankDetails,
  BuyerMyQuotations,

  BuyerMyBids,
  BuyerSales
};

