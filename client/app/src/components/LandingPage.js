import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Card, CardBody, CardGroup, Col, Container, Row } from 'reactstrap';

import AuthLayout from "./AuthLayout"
import {buyerExplainer, sellerExplainer} from "../constants"

class LandingPage extends Component {
  render() {
    return (
      <AuthLayout>
        <Container>
          <Row className="justify-content-center">
            <Col md="5">
              <CardGroup>
                <Card className="p-4">
                  <CardBody className="text-center">
                    <h2 className="text-center">Seller</h2>
                    <p className="lead text-center">{sellerExplainer}</p>
                    <Link className="btn btn-yellow btn-lg" tag="button" to="/seller/login">Seller Dashboard</Link>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
            <Col md="5">
              <CardGroup>
                <Card className="p-4">
                  <CardBody className="text-center">
                    <h2 className="text-center">Buyer</h2>
                    <p className="lead text-center">{buyerExplainer}</p>
                    <Link className="btn btn-blue btn-lg" tag="button" to="/buyer/login">Buyer Dashboard</Link>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </AuthLayout>
    );
  }
}

export default LandingPage;
