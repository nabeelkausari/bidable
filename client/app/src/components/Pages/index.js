import SellerLogin from '../Seller/Auth/Login';
import SellerRegister from '../Seller/Auth/Register';
import SellerEmailConfirmed from '../Seller/Auth/Confirmed';
import BuyerLogin from '../Buyer/Auth/Login';
import BuyerRegister from '../Buyer/Auth/Register';
import BuyerEmailConfirmed from '../Buyer/Auth/Confirmed';

export {
  SellerLogin, SellerRegister, SellerEmailConfirmed,
  BuyerLogin, BuyerRegister, BuyerEmailConfirmed
};
