import React, { Component } from 'react';
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  Table,
} from 'reactstrap';

import SalesContainer from '../../containers/sales';
import {getAuthHeaders, readableDate, regularTime} from "../../constants"
import NotifyModal from '../Common/NotifyModal';

class Sales extends Component {
  state = {
    modal: false,
    bids: []
  };

  toggleModal = () => this.setState({ modal: !this.state.modal })

  componentDidMount() {
    this.props.getBuyerSales(getAuthHeaders())
  }

  visitLot = (auctionId, lotNo) => {
    this.props.history.push(`/buyer/past-auctions/${auctionId}/${lotNo}`)
  }

  render() {
    const { buyerSales } = this.props;
    if (!buyerSales) return <div/>
    return (
      <Row>
        <Col>
          <Card>
            <CardHeader>
              My Sales Report
            </CardHeader>
            <CardBody>
              <Table responsive>
                <thead>
                <tr>
                  <th>Auction ID</th>
                  <th>Lot No</th>
                  <th>Company</th>
                  <th>Product</th>
                  <th>Auction Date</th>
                  <th>Balance Amount</th>
                  <th>Balance Paid</th>
                  <th>Product Picked Up</th>
                  <th>Lot Page</th>
                </tr>
                </thead>
                <tbody>
                {!buyerSales.length && <tr>
                  <td className="team-result__date">You have no sales yet</td>
                  <td/>
                  <td/>
                  <td/>
                  <td/>
                  <td/>
                  <td/>
                  <td/>
                  <td/>
                </tr>}
                {buyerSales.length > 0 && buyerSales.map(sale => (
                  <tr key={"row-data-" + sale._id}>
                    <td>{sale.auction.auctionId}</td>
                    <td>{sale.lot.lotNo}</td>
                    <td>{sale.lot.sellerName}</td>
                    <td>{sale.lot.product.name}</td>
                    <td>{readableDate(sale.lot.auctionDate)}</td>
                    <td>Not Updated</td>
                    <td>{sale.balancePaid ? "Paid" : "Not Paid"}</td>
                    <td>{sale.pickup.done ? "Yes" : "No"}</td>
                    <td>
                      <button
                        onClick={() => this.visitLot(sale.auction._id, sale.lot.lotNo)}
                        className="btn btn-link"
                      >Visit lot</button>
                    </td>
                  </tr>
                ))}
                </tbody>
              </Table>
            </CardBody>
          </Card>
          <NotifyModal
            onClose={() => this.setState({ modal: false, bids: [] })}
            modal={this.state.modal}
            title="All your bids"
            body={<ShowBids bids={this.state.bids}/>}
            showConfirmBtn
            toggle={this.toggleModal}
          />
        </Col>
      </Row>
    );
  }
}

const ShowBids = ({ bids }) => (
  <Table responsive>
    <thead>
    <tr>
      <th>Bid Price</th>
      <th>Bid Time</th>
    </tr>
    </thead>
    <tbody>
    {bids.map(bid => (
      <tr key={bid._id}>
        <td>{bid.price}</td>
        <td>{regularTime(bid.createdAt)}</td>
      </tr>
    ))}
    </tbody>
  </Table>
)

export default SalesContainer(Sales);
