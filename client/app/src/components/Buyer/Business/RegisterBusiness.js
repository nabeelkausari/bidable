import React, { Component } from 'react';
import {NotificationManager} from 'react-notifications';

import BuyerBusinessRegisterContainer from '../../../containers/auth/buyer/registerBusiness'
import BusinessForm from './BusinessForm';

class BuyerBusinessRegister extends Component {

  handleFormSubmit = props => {
    const { fullName, companyName, categories, ...restProps } = props;
    this.props.submitBuyerBusiness({
      ...restProps,
      representativeName: fullName,
      name: companyName,
      categories: categories.map(cat => cat.value),
      redirect: this.handleRedirect.bind(this)
    });
  }

  handleRedirect = () => {
    NotificationManager.success("Verification process will be completed within 2 working days", "Business Detail Received", 5000)
    this.props.history.push('/buyer/dashboard');
  }

  componentDidMount() {
    this.props.getCategories()
  }

  renderAlert = () => {
    if (this.props.submitBuyerBusinessError) {
      return (
        <div className="alert alert-danger">
          <strong>Oops!</strong> {this.props.submitBuyerBusinessError}
        </div>
      )
    }
  }

  render() {
    if (!this.props.categories) return <div/>
    const categories = this.props.categories.map(cat => ({label: cat.name, value: cat.categoryId }))
    return (
      <BusinessForm
        {...this.props}
        options={categories}
        handleFormSubmit={this.handleFormSubmit}
        renderAlert={this.renderAlert}
      />
    );
  }
}

export default BuyerBusinessRegisterContainer(BuyerBusinessRegister);

