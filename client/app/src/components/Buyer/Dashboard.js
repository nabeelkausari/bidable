import React, { Component } from 'react';
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
} from 'reactstrap';

import Widget from '../Common/Widget'
import RegisterBusiness from './Business/RegisterBusiness';
import BuyerDashboardContainer from '../../containers/dashboard/buyer';

import {getAuthHeaders} from "../../constants"

class Dashboard extends Component {
  handleLogout = () => {
    this.props.logoutBuyer({ redirect: () => this.props.history.push('/buyer/login')})
  }
  componentDidMount() {
    this.props.getBuyerStats(getAuthHeaders());
  }
  render() {
    if (!this.props.user || !this.props.buyerStats) return <div/>;
    const { businessSubmitted, email, userId } = this.props.user
    const { bidCount, saleCount, emdCount } = this.props.buyerStats
    return (
      <div className="animated fadeIn">
        {
          !businessSubmitted
            ? <Row>
              <Col md={6}>
                <Card>
                  <CardHeader>
                    Submit Business Details
                  </CardHeader>
                  <CardBody>
                    <RegisterBusiness history={this.props.history} />
                  </CardBody>
                </Card>
              </Col>
            </Row>
            : <Row>
              <Col xs="12" sm="6" lg="3">
                <Widget header={userId} mainText={email} icon="fa fa-user" color="primary" footer
                        onFooterClick={this.handleLogout.bind(this)}
                        footerText="Logout"
                />
              </Col>
              <Col xs="12" sm="6" lg="3">
                <Widget header={bidCount.toString()} mainText="No of Bids Placed" icon="fa fa-laptop" color="secondary" footer link="/app/buyer/my-bids" />
              </Col>
              <Col xs="12" sm="6" lg="3">
                <Widget header={saleCount.toString()} mainText="No of Sale Intimations" icon="fa fa-laptop" color="warning" footer link="/app/buyer/sale-intimations" />
              </Col>
              <Col xs="12" sm="6" lg="3">
                <Widget header={emdCount.toString()} mainText="No of EMDs Paid" icon="fa fa-laptop" color="success" footer link="/app/buyer/my-emd-list" />
              </Col>
            </Row>
        }
      </div>
    );
  }
}

export default BuyerDashboardContainer(Dashboard);
