import React from 'react';
import { reduxForm, Field } from 'redux-form';
import {regularField} from "../../Common"
import {price} from "../../../constants"

const validate = formProps => {
  const errors = {};
  if (!formProps.quotePrice || formProps.quotePrice <= 0) errors.quotePrice = 'Please enter the quotation price'
  return errors;
}

const QuotationForm = props => {
  return (
    <form onSubmit={props.handleSubmit(props.handleFormSubmit)}>
      <div className="row mt-3">
        <div className="col-xl-10 offset-xl-1 col-md-12 offset-md-0 col-sm-8 offset-sm-2 text-center">
          {
            props.quotation && <fieldset className="form-group">
              <h4>My Quotation: {price(props.quotation.price)}</h4>
            </fieldset>
          }
          <h5>You can quote a price below the starting price, if any bids are placed, quotations are not valid.</h5>
          <fieldset className="form-group">
            <Field hideLabel name="quotePrice" component={regularField} type="number" placeholder="Enter your quotation..." />
            <button type="submit" className="btn btn-secondary"><strong>Place Quotation</strong></button>
          </fieldset>
        </div>
      </div>
      {props.renderAlert()}
    </form>
  )
}

export default reduxForm({
  form: 'quotationForm',
  validate
})(QuotationForm)
