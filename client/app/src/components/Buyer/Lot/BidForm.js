import React, { Component } from 'react';
import { connect } from 'react-redux';
import { InputGroup, InputGroupAddon, Label, Input } from 'reactstrap';
import { reduxForm, Field } from 'redux-form';
import { setMyBid, clearMyBid } from "../../../containers/lots/actions"
import {price} from "../../../constants"

const validate = formProps => {
  const errors = {};
  if (!formProps.myBid) errors.myBid = 'Please enter the bid price'
  return errors;
}

class BidForm extends Component {
  state = {
    nextPrice: 0
  }

  setNextPrice = () => {
    const {highestBid, minimumIncrement} = this.props.lot;
    let nextPrice = Number(highestBid.price) + minimumIncrement;
    this.setState({ nextPrice })
    this.props.setMyBid(nextPrice)
  }

  setStartingPrice = () => {
    const {startingPrice} = this.props.lot;
    let nextPrice = Number(startingPrice);
    this.setState({ nextPrice })
    this.props.setMyBid(nextPrice)
  }

  clearNextPrice = () => {
    this.setState({ nextPrice: 0 })
    this.props.clearMyBid()
    this.props.reset()
  }

  componentWillUnmount() {
    this.props.clearMyBid()
  }

  doSubmit = props => {
    this.props.handleFormSubmit(props)
    this.clearNextPrice()
  }

  render() {
    const { props } = this;
    const {minimumIncrement, startingPrice} = props.lot;
    const { nextPrice } = this.state;
    return (
      <form onSubmit={props.handleSubmit(this.doSubmit)}>
        <div className="row mt-3">
          <div className="col-xl-10 offset-xl-1 col-md-12 offset-md-0 col-sm-8 offset-sm-2 text-center">
            <Label><strong>Your Bid Price</strong></Label>
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                {
                  props.lot.highestBid  && props.lot.highestBid.price
                    ? <button
                      type="button"
                      className="btn btn-primary"
                      onClick={this.setNextPrice}
                    >
                      <strong>+ {price(minimumIncrement)}</strong>
                    </button>
                    : <button
                      type="button"
                      className="btn btn-primary"
                      onClick={this.setStartingPrice}>
                      <strong>{price(startingPrice)}</strong>
                    </button>
                }
              </InputGroupAddon>
              <input readOnly className="bid-readonly form-control" value={nextPrice && price(nextPrice)}/>
              <Field label="Your Bid" name="myBid" component={renderField} type="number" placeholder="Enter your bid price..." />
              <InputGroupAddon addonType="append">
                <button type="button" className="btn btn-warning" onClick={this.clearNextPrice}><strong>Clear</strong></button>
              </InputGroupAddon>
            </InputGroup>
          </div>
          <div className="col-md-12 text-center mt-3">
            <button type="submit" className="btn btn-lg btn-primary"><strong>Place Your Bid</strong></button>
          </div>
        </div>
        {props.renderAlert()}
      </form>
    )
  }
}

const renderField = ({input, label, disabled, type, options, id}) => (
  <Input {...input}  readOnly hidden type={type} id={id} />
)


const reduxBidForm = reduxForm({
  form: 'bidForm', validate,
  enableReinitialize: true
})(BidForm)

export default connect(state => ({
  initialValues: state.lots.bidForm
}), { setMyBid, clearMyBid })(reduxBidForm)
