import React, { Component } from 'react';
import {Button, Card, CardBody, CardHeader, Col, Row, Table, Badge } from 'reactstrap';
import {NotificationManager} from 'react-notifications';

import NotifyModal from '../../Common/NotifyModal';
import { BuyerStatus } from '../../Common/Status';
import LotDetailContainer from '../../../containers/lots/index';
import LotBidView from './LotBidView'
import {readableDate, readableTime, getAuthHeaders, price} from "../../../constants"

class LotDetail extends Component {
  state = {
    modal: false,
    lotLoading: true
  }

  componentDidMount() {
    this.loadLot()
  }

  loadLot = () => {
    this.props.getLot(this.props.match.params, getAuthHeaders());
  }

  onCompleteTimer = () => {
    this.loadLot()
  }

  componentDidUpdate(prevProps) {
    const { lotLoading, selectedLot, getLotBids, getMyQuotation } = this.props;
    let lotTriggered = prevProps.lotLoading !== lotLoading;
    let lotLoaded = lotLoading === false;
    if (lotTriggered && lotLoaded) {
      getLotBids(selectedLot._id, getAuthHeaders())
      getMyQuotation(selectedLot._id, getAuthHeaders())
      this.setState({ lotLoading: false })
    }
  }

  render() {
    const { lotLoading } = this.state;
    if (!this.props.selectedLot) return (
      <div className="animated fadeIn">
        <Row>
          <div className="col-md-6">
            <h2>{lotLoading ? "Loading..." : "Lot Not Found"}</h2>
          </div>
        </Row>
      </div>
    )
    const {
      auction: { auctionId }, auctionDate, auctionTime, liveStatus, lotNo, sellerName,
      product: { location, name, description }, startingPrice, minimumIncrement, emdAmount,
      quantity, bidPer, unit, pcbRequired, gst
    } = this.props.selectedLot;

    return (
      <div className="animated fadeIn">
        <Row>
          <Col md={6}>
            <LotBidView
              {...this.props}
              onCompleteTimer={this.onCompleteTimer}
            />
          </Col>
          <Col md={6}>
            <Card>
              <CardBody>
                <Table responsive className="headless">
                  <tbody>
                  <tr>
                    <th>Auction ID</th>
                    <td>{auctionId} <span className="ml-2">{BuyerStatus({ liveStatus })}</span></td>
                  </tr>
                  <tr>
                    <th>Lot No</th>
                    <td>{lotNo}</td>
                  </tr>
                  <tr>
                    <th>Starting Price</th>
                    <td>{price(startingPrice)} (Per {bidPer})</td>
                  </tr>
                  <tr>
                    <th>Increment Value</th>
                    <td>{price(minimumIncrement)}</td>
                  </tr>
                  <tr>
                    <th>Seller Name</th>
                    <td>{sellerName}</td>
                  </tr>
                  <tr>
                    <th>Product Name</th>
                    <td>{name}</td>
                  </tr>
                  <tr>
                    <th>Product Location</th>
                    <td>{location}</td>
                  </tr>
                  <tr>
                    <th>Product Description</th>
                    <td>{description}</td>
                  </tr>
                  <tr>
                    <th>Quantity (Approx)</th>
                    <td>{quantity} {unit}</td>
                  </tr>
                  <tr>
                    <th>EMD Amount</th>
                    <td>{emdAmount} INR</td>
                  </tr>
                  <tr>
                    <th>PCB Certificate</th>
                    <td>{pcbRequired ? "Required" : "Not Required"}</td>
                  </tr>
                  <tr>
                    <th>Additional Taxes</th>
                    <td>{gst}% GST + 1% TCS</td>
                  </tr>
                  <tr>
                    <th>Auction Date</th>
                    <td>{readableDate(auctionDate)}</td>
                  </tr>
                  <tr>
                    <th>Auction Timings</th>
                    <td>{`${readableTime(auctionTime.start)} - ${readableTime(auctionTime.end)}`}</td>
                  </tr>
                  {auctionTime.extendedFrom && <tr>
                    <th>Grace Time Started From</th>
                    <td>{readableTime(auctionTime.extendedFrom)}</td>
                  </tr>}
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>

          <NotifyModal
            onClose={this.processAuctionSubmit}
            modal={this.state.modal}
            title="Submit Auction"
            body="Your auction is being submitted to our verification department to review. Once it is verified and approved, your auction will be listed on the platform and it will go live"
            showConfirmBtn
            showCancelBtn
            onCancel={() => this.setState({modal: false, submitInProcess: false})}
            confirmBtnText="Submit"
            confirmBtnClass="primary"
          />
        </Row>
      </div>
    );
  }
}


export default LotDetailContainer(LotDetail);
