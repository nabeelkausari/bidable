import React, { Component } from 'react';
import moment from 'moment';
import { filter } from 'lodash';
import {Card, CardBody, CardHeader, Button, Badge } from 'reactstrap';
import Timer from "react-time-counter"
import io from 'socket.io-client';
import {NotificationManager} from 'react-notifications';

import {HOST, getAuthHeaders, getDateTime, hasPaidEMD, price} from "../../../constants"
import BidForm from './BidForm';
import QuotationForm from './QuotationForm';
import { ShowBids } from "../MyBids";

class LotBidView extends Component {

  constructor(props) {
    super(props);
    let connectionOptions =  {
      // "force new connection" : true,
      // "reconnectionAttempts": "Infinity", //avoid having user reconnect manually in order to prevent dead clients after a server restart
      // "timeout" : 10000,                  //before connect_error and connect_timeout are emitted.
      "transports" : ["websocket"]
    };
    this.state = { showTimer: true }
    this.socket = io(HOST, connectionOptions);
    this.socket.on('bidCreated', data => {
      if (this.props.selectedLot && data.lotId === this.props.selectedLot._id) {
        this.notify(data)
        this.props.updateSelectedLot(data)
        this.refreshTimer();
      }
    });
  }

  timeLeft = ({ auctionDate, auctionTime }) => {
    let end = getDateTime(auctionDate, auctionTime.end);
    let secondsDiff = end.diff(moment(), 'seconds');
    return {
      seconds: (secondsDiff % 60),
      minutes: (Math.floor(secondsDiff / 60))
    }
  }

  refreshTimer = () => {
    this.setState({ showTimer: false })
    this.setState({ showTimer: true })
  }

  renderAlert = () => {
    if (this.props.createBidError) {
      return (
        <div className="alert alert-danger mt-2">
          {this.props.createBidError}
        </div>
      )
    }
  }

  notify = (data) => {
    if (data.bidUpdates.bidder === this.props.user._id) {
      NotificationManager.success(``, `Your bid placed for ${price(data.bidUpdates.price)}`, 5000)
    } else {
      NotificationManager.warning(``, `Bid placed for ${price(data.bidUpdates.price)}`, 5000)
    }
  }

  handleSubmitBid = ({ myBid }) => {
    this.props.createBid({
      myBid, lotId: this.props.selectedLot._id
    }, getAuthHeaders())
  }

  handleQuoteSubmit = ({ quotePrice }) => {
    this.props.createQuotation({
      quotePrice,
      lotId: this.props.selectedLot._id,
      redirect: () => NotificationManager.success("Your quotation is placed")
    }, getAuthHeaders())
  }

  renderQuoteAlert = () => {
    if (this.props.createQuotationError) {
      return (
        <div className="alert alert-danger mt-2">
          {this.props.createQuotationError}
        </div>
      )
    }
  }


  goToEMDPage = () => {
    this.props.history.push(`/buyer/submit-emd/${this.props.selectedLot._id}`)
  }

  componentDidUpdate(prevProps) {
    const { selectedLot } = this.props;
    if (
      selectedLot && prevProps.selectedLot &&
      selectedLot._id !== prevProps.selectedLot._id
    ) {
      this.refreshTimer()
    }
  }

  componentWillUnmount() {
    this.socket.close()
    this.props.removeSelectedLot()
  }

  userWinner = () => {
    const { lotBids, user } = this.props;
    return (lotBids && lotBids.length && lotBids[0].bidder === user._id)
  }

  showBidForm = () => {
    const { selectedLot : {liveStatus, emdList}, user } = this.props;
    if (!user) return;
    const isLive = liveStatus === "live";
    const [emd] = emdList.filter(e => e.user === user._id);
    return isLive && emd && emd.status === "paid";
  }

  showQuotationForm = () => {
    let emdPaid = this.showBidForm();
    const { highestBid } = this.props.selectedLot;
    return emdPaid && (!highestBid || !highestBid.price)
  }

  render() {
    const { selectedLot, lotBids, user, myQuotation } = this.props
    if (!selectedLot) return <div/>
    const { auction: { auctionId }, liveStatus, lotNo, highestBid, emdAmount } = selectedLot;
    const isLive = liveStatus === "live";
    const isUpcoming = liveStatus === "upcoming";
    const { showTimer } = this.state;
    const {seconds, minutes} = this.timeLeft(selectedLot);
    const [emd] = selectedLot.emdList.filter(e => e.user === user._id);
    return (
      <div>
        <Card>
          <CardHeader>
            Auction ID: <strong>{auctionId}</strong>
            <span className="float-right">
            Lot No: <strong>{lotNo}</strong>
          </span>
          </CardHeader>
          <CardBody>
            {isLive && showTimer && (
              <div className="text-center mb-3">
              <span className={'timer m-auto ' + (Number(minutes) < 3 ? 'last-minute' : '')}>
              <Timer
                backward
                minutes={minutes}
                seconds={seconds}
                callback={this.props.onCompleteTimer}
              />
            </span>
              </div>
            )}
            {
              isUpcoming ? (
                <div className={"highest-bid text-center " + (this.userWinner()? "winner" : "") }>
                  <h4>EMD Amount: {price(emdAmount)}</h4>
                  {!hasPaidEMD(selectedLot, user) ? (
                    <Button
                      size="lg"
                      className="mt-2 mb-2"
                      color="success"
                      onClick={this.goToEMDPage}
                    >Pay EMD</Button>
                  ) : (
                    emd.approval === "success"
                      ? <h6>EMD amount <Badge color="success">{emd.status}</Badge></h6>
                      : <h6>We have received your EMD form and your request is <Badge color="warning">{emd.approval}</Badge></h6>
                  )}
                </div>
              ) : (
                <div className={"highest-bid text-center " + (this.userWinner()? "winner" : "") }>
                  <h4>Highest Bid</h4>
                  <h2>{highestBid && highestBid.price
                    ?<span style={{ fontWeight: 'bold' }}>{highestBid.price.toLocaleString('en-IN')} INR</span>
                    :<span style={{ color: "#4dbd73" }}>No Bids Yet</span>}</h2>
                </div>
              )
            }
            {this.showBidForm() && <BidForm
              lot={selectedLot}
              handleFormSubmit={this.handleSubmitBid}
              renderAlert={this.renderAlert.bind(this)}
            />}
            {!isLive && this.userWinner() ? <div className="mt-3 text-center">
              <h3>Congratulations! You are the winner.</h3>
            </div>
            : <div/>}
          </CardBody>
        </Card>
        {
          this.showQuotationForm() && <Card>
            <CardHeader>
              <strong>Quotation</strong>
            </CardHeader>
            <CardBody>
              <QuotationForm
                quotation={myQuotation}
                handleFormSubmit={this.handleQuoteSubmit}
                renderAlert={this.renderQuoteAlert.bind(this)}
              />
            </CardBody>
          </Card>
        }
        {
          lotBids && lotBids.length && user ? (
            <Card>
              <CardBody>
                <ShowBids bids={lotBids} user={user}/>
              </CardBody>
            </Card>
          ) : <div/>
        }
      </div>
    )
  }
}

export default LotBidView
