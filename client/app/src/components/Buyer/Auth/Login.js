import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, CardHeader, CardBody, CardGroup, CardFooter, Col, Container, Row, ButtonGroup } from 'reactstrap';
import { Field } from 'redux-form';

import LoginContainer from '../../../containers/auth/buyer/loginBuyer'
import { renderField } from "../../Common/index";
import AuthLayout from "../../AuthLayout"

class Login extends Component {

  handleFormSubmit = ({ email, password }) => {
    this.props.loginBuyer({
      email,
      password,
      redirect: () => this.props.history.push('/buyer/dashboard')
    });
  }

  renderAlert = () => {
    if (this.props.authBuyerError) {
      return (
        <div className="alert alert-danger">
          <strong>Oops!</strong> {this.props.authBuyerError}
        </div>
      )
    }
  }

  componentDidMount() {
    this.props.clearAuthErrors()
  }

  render() {
    return (
      <AuthLayout>
        <Container>
          <Row className="justify-content-center">
            <Col md="5">
              <CardGroup>
                <Card>
                  <CardBody className="p-4">
                    <h1 className="text-center">Login as Buyer</h1>
                    <p className="text-muted text-center">Login to your buyer account</p>
                    <form className={'modal-form ' + this.props.className } onSubmit={this.props.handleSubmit(this.handleFormSubmit)}>
                      <fieldset className="form-group">
                        <Field icon="icon-envelope" label="Email" name="email" component={renderField} type="email" placeholder="Enter your email address..." />
                      </fieldset>
                      <fieldset className="form-group">
                        <Field icon="icon-lock" label="Password" name="password" component={renderField} type="password" placeholder="Enter your password..." />
                      </fieldset>
                      {this.renderAlert()}
                      <Row>
                        <Col className="text-center">
                          <Button action="submit" className="px-4 btn-blue btn-lg">Login</Button>
                        </Col>
                      </Row>
                    </form>
                  </CardBody>
                  <CardFooter className="p-4">
                    <Row>
                      <Col xs="6" className="text-left">
                        <Link className="btn btn-outline-primary" tag="button" to="/buyer/register">Register</Link>
                      </Col>
                      <Col xs="6" className="text-right">
                        <Link className="btn btn-link px-0" tag="button" to="/buyer/forgot-password">Forgot password?</Link>
                      </Col>
                    </Row>
                  </CardFooter>
                </Card>
              </CardGroup>
              <div className="text-center">
                <Link className="btn btn-link m-auto" tag="button" to="/">Back to Home</Link>
              </div>
            </Col>
          </Row>
        </Container>
      </AuthLayout>
    );
  }
}


export default LoginContainer(Login);
