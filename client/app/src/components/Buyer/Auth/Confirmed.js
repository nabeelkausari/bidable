import React, { Component } from 'react';
import { Card, CardBody, Col, Container, Row } from 'reactstrap';
import { Link } from 'react-router-dom';
import AuthLayout from "../../AuthLayout"

class BuyerConfirmed extends Component {

  render() {
    return (
      <AuthLayout>
        <Container>
          <Row className="justify-content-center">
            <Col md="12">
              <Card className="p-4">
                <CardBody>
                  <h1 className="text-center" style={{ textTransform: 'capitalize'}}>Hurray!</h1>
                  <p className="lead text-center">Your email address has been successfully verified</p>
                  <div className="text-center">
                    <Link to="/buyer/login" className="btn btn-primary m-auto">Login</Link>
                  </div>
                </CardBody>
              </Card>

            </Col>
          </Row>
        </Container>
      </AuthLayout>
    );
  }
}

export default BuyerConfirmed;
