import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Card, CardBody, CardFooter, Col, Container, Row } from 'reactstrap';

import RegisterContainer from '../../../containers/auth/buyer/registerBuyer'
import NotifyModal from '../../Common/NotifyModal';
import AuthLayout from "../../AuthLayout"
import RegisterForm from "../../../components/Common/RegisterForm";
import {buyerExplainer} from "../../../constants"


class Register extends Component {
  state = {modal: false}

  handleFormSubmit = ({ mobile, email, password }) => {
    this.props.registerBuyer({
      mobile,
      email,
      password,
      redirect: this.confirmRedirection.bind(this)
    });
  }

  confirmRedirection = () => {
    this.setState({ modal: true })
  }

  renderAlert = () => {
    if (this.props.registerBuyerError) {
      return (
        <div className="alert alert-danger">
          <strong>Oops!</strong> {this.props.registerBuyerError}
        </div>
      )
    }
  }

  componentDidMount() {
    this.props.clearAuthErrors()
  }

  render() {
    const { modal } = this.state;
    return (
      <AuthLayout>
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <Card>
                <CardBody className="p-4 text-center">
                  <h1>Register as Buyer</h1>
                  <p className="lead text-center mb-4">{buyerExplainer}  Or else register as <Link tag="button" to="/seller/register">Seller</Link></p>
                  <RegisterForm
                    handleFormSubmit={this.handleFormSubmit.bind(this)}
                    renderAlert={this.renderAlert.bind(this)}
                    btnClassName="btn-blue"
                  />
                </CardBody>
                <CardFooter className="p-4">
                  <Row>
                    <Col xs="6" className="text-left">
                      <Link className="btn btn-outline-primary" tag="button" to="/buyer/login">Login</Link>
                    </Col>
                    <Col xs="6" className="text-right">
                      <Link className="btn btn-link px-0" tag="button" to="/buyer/forgot-password">Forgot password?</Link>
                    </Col>
                  </Row>
                </CardFooter>
              </Card>
              <div className="text-center">
                <Link className="btn btn-link m-auto" tag="button" to="/">Back to Home</Link>
              </div>
            </Col>
          </Row>
          <NotifyModal
            onClose={() => {
              this.props.history.push('/buyer/login')
            }}
            modal={modal}
            title="Confirm Email"
            body="You’ll receive a confirmation email in your inbox with a link to activate your account"
            showConfirmBtn
          />
        </Container>
      </AuthLayout>
    );
  }
}

export default RegisterContainer(Register);
