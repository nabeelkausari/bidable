import React, { Component } from 'react';
import {NotificationManager} from 'react-notifications';

import BankForm from './BankForm';
import {getAuthHeaders} from "../../../constants"

class BankRegister extends Component {

  handleFormSubmit = props => {
    this.props.submitBankDetails({
      ...props,
      redirect: this.handleRedirect.bind(this)
    }, getAuthHeaders());
  }

  handleRedirect = () => {
    NotificationManager.success("", "Bank Details Received Successfully", 5000)
  }

  renderAlert = () => {
    if (this.props.submitBankError) {
      return (
        <div className="alert alert-danger">
          <strong>Oops!</strong> {this.props.submitBankError}
        </div>
      )
    }
  }

  render() {
    return (
      <BankForm
        {...this.props}
        handleFormSubmit={this.handleFormSubmit}
        renderAlert={this.renderAlert}
      />
    );
  }
}

export default BankRegister;

