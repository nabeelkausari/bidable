import React, { Component } from 'react';
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  Table,
} from 'reactstrap';

import BankContainer from '../../../containers/emd/bank'
import RegisterBank from './Registration';
import {getAuthHeaders} from "../../../constants"

class BankDetails extends Component {

  componentDidMount() {
    this.props.getBankDetails(getAuthHeaders())
  }

  render() {
    const { bank } = this.props
    return (
      <div className="animated fadeIn">
        <Row>
          <Col md={6}>
            {
              bank ?
                (
                  <Card>
                    <CardHeader>
                      Your Bank Details
                    </CardHeader>
                    <CardBody>
                      <Table responsive className="headless">
                        <tbody>
                        <tr>
                          <th>Bank Name</th>
                          <td>{bank.name}</td>
                        </tr>
                        <tr>
                          <th>Account No</th>
                          <td>{bank.account}</td>
                        </tr>
                        <tr>
                          <th>IFSC Code</th>
                          <td>{bank.code}</td>
                        </tr>
                        <tr>
                          <th>Branch</th>
                          <td>{bank.branch}</td>
                        </tr>
                        <tr>
                          <th>Address</th>
                          <td>{bank.address}</td>
                        </tr>
                        </tbody>
                      </Table>
                    </CardBody>
                  </Card>
                ) : (
                  <Card>
                    <CardHeader>
                      Submit Bank Details
                    </CardHeader>
                    <CardBody>
                      <RegisterBank {...this.props} />
                    </CardBody>
                  </Card>
                )
            }
          </Col>
        </Row>
      </div>
    );
  }
}

export default BankContainer(BankDetails);
