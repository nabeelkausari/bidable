import React from 'react';
import { Field } from 'redux-form';
import {regularField} from "../../Common/index.js"

export default props => (
  <form onSubmit={props.handleSubmit(props.handleFormSubmit)}>
    <fieldset className="form-group">
      <Field label="Bank Name" name="name" component={regularField} type="text" placeholder="Enter your bank's name..." />
    </fieldset>
    <fieldset className="form-group">
      <Field label="Account No" name="account" component={regularField} type="text" placeholder="Enter your bank's account no..." />
    </fieldset>
    <fieldset className="form-group">
      <div className="row">
        <div className="col-md-6">
          <Field label="IFSC Code" name="code" component={regularField} type="text" placeholder="Enter your bank's IFSC code..." />
        </div>
        <div className="col-md-6">
          <Field label="Account Branch" name="branch" component={regularField} type="text" placeholder="Enter your bank's branch..." />
        </div>
      </div>
    </fieldset>
    <fieldset className="form-group">
      <Field label="Bank Address" name="address" component={regularField} type="text" placeholder="Enter your bank's address..." />
    </fieldset>
    {props.renderAlert()}
    <fieldset>
      <button type="submit" className="btn btn-primary">Submit</button>
    </fieldset>
  </form>
)
