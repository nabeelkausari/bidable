import React, { Component } from 'react';
import { sortBy } from 'lodash'

import AuctionsContainer from '../../../containers/auctions/buyer';
import AuctionList from './AuctionList';
import {anyLotLive, getAuctionFilter, getAuthHeaders, getDateTime} from "../../../constants"
import moment from "moment/moment"

class ActiveAuctions extends Component {
  state = {
    auctionsLoading: true
  }

  componentDidMount() {
    this.props.getAuctions(getAuthHeaders());
  }

  componentDidUpdate(prevProps) {
    let auctionsTriggered = prevProps.auctionsLoading !== this.props.auctionsLoading;
    let auctionsLoaded = this.props.auctionsLoading === false;
    if (auctionsTriggered && auctionsLoaded) {
      this.setState({ auctionsLoading: false })
    }
  }

  doFilter = ({ auctionDate, auctionTime, lots }) => {
    let filter = getAuctionFilter()
    switch (filter) {
      case "upcoming":
        return moment().isBefore(
          getDateTime(auctionDate, auctionTime.start)
        )
      default:
        return anyLotLive(lots)
    }
  }

  filterAuctions = () => {
    const { auctions } = this.props
    if (auctions) {
      let filtered = auctions.filter(auction => this.doFilter(auction))
      return sortBy(filtered, 'auctionDate')
    }
  }

  render() {
    const { auctionsLoading } = this.state;
    return (
      <AuctionList
        {...this.props}
        auctions={this.filterAuctions()}
        auctionsLoading={auctionsLoading}
        detailPath="/buyer/auctions"
        title="Active Auctions"
      />
    );
  }
}

export default AuctionsContainer(ActiveAuctions);
