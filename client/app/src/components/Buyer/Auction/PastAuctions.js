import React, { Component } from 'react';
import { sortBy } from 'lodash'

import AuctionsContainer from '../../../containers/auctions/buyer';
import AuctionList from './AuctionList';
import {anyLotLive, getAuthHeaders} from "../../../constants"

class PastAuctions extends Component {
  state = {
    auctionsLoading: true
  }

  componentDidMount() {
    this.props.getAuctions(getAuthHeaders());
  }

  componentDidUpdate(prevProps) {
    let auctionsTriggered = prevProps.auctionsLoading !== this.props.auctionsLoading;
    let auctionsLoaded = this.props.auctionsLoading === false;
    if (auctionsTriggered && auctionsLoaded) {
      this.setState({ auctionsLoading: false })
    }
  }

  isPastAuction = ({ lots }) => {
    return !anyLotLive(lots)
  }

  filterPastAuctions = () => {
    const { auctions } = this.props
    if (auctions) {
      let filtered = auctions.filter(auction => this.isPastAuction(auction))
      return sortBy(filtered, 'auctionDate')
    }
  }

  render() {
    const { auctionsLoading } = this.state;
    return (
      <AuctionList
        {...this.props}
        auctions={this.filterPastAuctions()}
        auctionsLoading={auctionsLoading}
        detailPath="/buyer/past-auctions"
        title="Past Auctions"
      />
    );
  }
}

export default AuctionsContainer(PastAuctions);
