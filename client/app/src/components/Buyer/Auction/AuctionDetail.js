import React, { Component } from 'react';
import {Button, Card, CardBody, CardHeader, Col, Row, Table, Badge } from 'reactstrap';
import {NotificationManager} from 'react-notifications';

import NotifyModal from '../../Common/NotifyModal';
import { BuyerStatus } from '../../Common/Status';
import AuctionDetailContainer from '../../../containers/auctions/buyer';
import LotTable from './LotTable';
import { readableDate, readableTime, getAuthHeaders } from "../../../constants"

class AuctionDetail extends Component {
  state = {
    submitInProcess: false,
    createLotVisible: false,
    editLotVisible: false,
    modal: false,
    auctionLoading: true
  }

  componentDidMount() {
    this.loadAuction()
  }

  loadAuction = () => {
    const { getAuction, match } = this.props;
    getAuction(match.params.auctionId, getAuthHeaders())
  }

  componentDidUpdate(prevProps) {
    let auctionTriggered = prevProps.auctionLoading !== this.props.auctionLoading;
    let auctionLoaded = this.props.auctionLoading === false;
    if (auctionTriggered && auctionLoaded) {
      this.setState({ auctionLoading: false })
    }
  }

  render() {
    const { auctionLoading } = this.state;
    if (!this.props.auction || !this.props.user) return (
      <div className="animated fadeIn">
        <Row>
          <div className="col-md-6">
            <h2>{auctionLoading ? "Loading..." : "Auction Not Found"}</h2>
          </div>
        </Row>
      </div>
    )
    const {
      auctionId, company, inspectionDetails: { from, to, timings: [time]}, lots,
      auctionDate, auctionTime, liveStatus
    } = this.props.auction;
    return (
      <div className="animated fadeIn">
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                Auction ID : <strong>{auctionId}</strong>
                <span className="float-right mb-0">{BuyerStatus({ liveStatus })}</span>
              </CardHeader>
              <CardBody>
                <Table responsive>
                  <thead>
                    <tr>
                      <th>Company Name</th>
                      <th>Auction Date</th>
                      <th>Auction Timings</th>
                      {liveStatus === "upcoming" && <th>Inspection Dates</th>}
                      {liveStatus === "upcoming" && <th>Inspection Timings</th>}
                    </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td>{company}</td>
                    <td>{readableDate(auctionDate)}</td>
                    <td>{`${readableTime(auctionTime.start)} - ${readableTime(auctionTime.end)}`}</td>
                    {liveStatus === "upcoming" && <td>{readableDate(from)} - {readableDate(to)}</td>}
                    {liveStatus === "upcoming" && <td>{readableTime(time.from)} - {readableTime(time.to)}</td>}
                  </tr>
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>

          <NotifyModal
            onClose={this.processAuctionSubmit}
            modal={this.state.modal}
            title="Submit Auction"
            body="Your auction is being submitted to our verification department to review. Once it is verified and approved, your auction will be listed on the platform and it will go live"
            showConfirmBtn
            showCancelBtn
            onCancel={() => this.setState({modal: false, submitInProcess: false})}
            confirmBtnText="Submit"
            confirmBtnClass="primary"
          />
        </Row>
        <LotTable
          {...this.props}
          lots={lots}
        />
      </div>
    );
  }
}


export default AuctionDetailContainer(AuctionDetail);
