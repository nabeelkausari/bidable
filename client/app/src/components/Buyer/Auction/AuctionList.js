import React, { Component } from 'react';
import { join, truncate } from 'lodash';
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  Table,
  Button
} from 'reactstrap';

import {getAuctionLiveStatus, readableDate, readableTime} from "../../../constants"
import auctionImg from '../../../assets/img/auction.png';
import {BuyerStatus} from "../../Common/Status"

const AuctionList = props => {
  const { auctions, auctionsLoading, title } = props
  if (!auctions || !auctions.length) return (
    <div className="animated fadeIn">
      <Row>
        <div className="col-md-6 offset-md-3">
          {
            auctionsLoading
              ? <h2>Loading....</h2>
              : <div className="dashboard-coming-soon">
                <div>
                  <img alt="Bidable Auctions" src={auctionImg}/>
                </div>
                <h3>No {title} Found</h3>
              </div>
          }
        </div>
      </Row>
    </div>
  )
  return (
    <div className="animated fadeIn">
      <Row>
        <Col>
          <Card>
            <CardHeader>
              {title}
            </CardHeader>
            <CardBody>
              <Table hover responsive>
                <thead>
                <tr>
                  <th>Auction ID</th>
                  <th>Company Name</th>
                  <th>Inspection Dates</th>
                  <th>Inspection Timings</th>
                  <th>Auction Date</th>
                  <th>Auction Timings</th>
                  <th>Lots</th>
                  <th>Status</th>
                </tr>
                </thead>
                <tbody>
                {auctions.map((auction, i) => (
                  <AuctionRow key={i} {...props} auction={auction}/>
                ))}
                </tbody>
              </Table>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  );
}

class AuctionRow extends Component {

  viewDetails = auctionId => {
    this.props.history.push(`${this.props.detailPath}/${auctionId}`)
  }

  render() {
    const { _id, auctionId, company, inspectionDetails, lots, auctionDate, auctionTime } = this.props.auction
    const [time] = inspectionDetails.timings;
    return (
      <tr style={{ cursor: 'pointer' }} onClick={() => this.viewDetails(_id)}>
        <td>{auctionId}</td>
        <td>{company}</td>
        <td>{readableDate(inspectionDetails.from)} - {readableDate(inspectionDetails.to)}</td>
        <td>{readableTime(time.from)} - {readableTime(time.to)}</td>
        <td>{auctionDate ? readableDate(auctionDate) : "Not Assigned"}</td>
        <td>{(auctionTime && `${readableTime(auctionTime.start)} - ${readableTime(auctionTime.end)}`) || "Not Assigned"}</td>
        <td>{truncate(join(lots.map(lot => lot.product.name), ", "))}</td>
        <td>{BuyerStatus({ liveStatus: getAuctionLiveStatus(this.props.auction) })}</td>
      </tr>
    )
  }
}

export default AuctionList
