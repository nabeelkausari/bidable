import React, { Component } from 'react';
import {Button, Card, CardBody, CardHeader, Col, Table } from 'reactstrap';

import {BuyerStatus} from "../../Common/Status"
import {price, readableTime, shortDate, hasPaidEMD, isBeforeStart} from "../../../constants"

class LotTable extends Component {

  handleSelectLot = lotNo => {
    const { auction, location, history } = this.props
    const path = location.pathname.split("/")[2]
    history.push(`/buyer/${path}/${auction._id}/${lotNo}`)
  }

  goToEMDPage = lotId => {
    this.props.history.push(`/buyer/submit-emd/${lotId}`)
  }

  showEMDButton = (lot, user) => {
    if (!user) return false;
    return !hasPaidEMD(lot, user) && isBeforeStart(lot)
  }

  render() {
    const { lots, user } = this.props;
    if (!lots || lots.length === 0) return <div/>
    return (
      <div className="auction-lots row">
        {
          lots.map(lot => (
            <Col key={lot._id} md={4}>
              <Card className="card-accent-primary">
                <CardHeader>
                  <strong>Lot #{lot.lotNo}</strong>
                  <span className="float-right mb-0">
                  {BuyerStatus({ liveStatus: lot.liveStatus })}
                </span>
                </CardHeader>
                <CardBody>
                  <Table responsive className="headless">
                    <tbody>
                    <tr>
                      <th>Product Name</th>
                      <td>{lot.product.name}</td>
                    </tr>
                    <tr>
                      <th>Starting Price</th>
                      <td>{price(lot.startingPrice)} (Per {lot.bidPer})</td>
                    </tr>
                    <tr>
                      <th>Qty (Approx)</th>
                      <td>{lot.quantity} {lot.unit}</td>
                    </tr>
                    <tr>
                      <th>EMD Amount</th>
                      <td>{lot.emdAmount ? price(lot.emdAmount) : "Not Assigned"}</td>
                    </tr>
                    <tr>
                      <th>Auction Starts</th>
                      <td>{lot.auctionDate ? `${shortDate(lot.auctionDate)} (${readableTime(lot.auctionTime.start)})` : 'Not Assigned'}</td>
                    </tr>
                    </tbody>
                  </Table>
                  <div className="text-center">
                    <Button
                      color="primary"
                      onClick={() => this.handleSelectLot(lot.lotNo)}
                    >Details</Button>
                    {this.showEMDButton(lot, user) && <Button
                      className="ml-3"
                      color="success"
                      onClick={() => this.goToEMDPage(lot._id)}
                    >Pay EMD</Button>}
                  </div>
                </CardBody>
              </Card>
            </Col>
          ))
        }
      </div>
    )
  }
}

export default LotTable
