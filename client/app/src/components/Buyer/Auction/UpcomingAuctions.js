import React, { Component } from 'react';
import moment from 'moment';
import { sortBy } from 'lodash'

import AuctionsContainer from '../../../containers/auctions/buyer';
import AuctionList from './AuctionList';
import {getAuthHeaders, getDateTime} from "../../../constants"

class UpcomingAuctions extends Component {
  state = {
    auctionsLoading: true
  }

  componentDidMount() {
    this.props.getAuctions(getAuthHeaders());
  }

  componentDidUpdate(prevProps) {
    let auctionsTriggered = prevProps.auctionsLoading !== this.props.auctionsLoading;
    let auctionsLoaded = this.props.auctionsLoading === false;
    if (auctionsTriggered && auctionsLoaded) {
      this.setState({ auctionsLoading: false })
    }
  }

  isUpcomingAuction = ({ auctionDate, auctionTime }) => {
    return moment()
      .isBefore(getDateTime(auctionDate, auctionTime.start))
  }

  filterUpcomingAuctions = () => {
    const { auctions } = this.props
    if (auctions) {
      let filtered = auctions.filter(auction => this.isUpcomingAuction(auction));
      return sortBy(filtered, 'auctionDate')
    }
  }

  render() {
    const { auctionsLoading } = this.state;
    return (
      <AuctionList
        {...this.props}
        auctions={this.filterUpcomingAuctions()}
        auctionsLoading={auctionsLoading}
        title="Upcoming Auctions"
      />
    );
  }
}

export default AuctionsContainer(UpcomingAuctions);
