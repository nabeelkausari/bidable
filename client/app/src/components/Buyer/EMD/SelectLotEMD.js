import React, { Component } from 'react';
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
} from 'reactstrap';
import {NotificationManager} from 'react-notifications';

import SubmitEMDContainer from '../../../containers/emd'
import SelectAuctionForm from './SelectAuctionForm';
import {getAuthHeaders} from "../../../constants"

class SubmitEMD extends Component {

  handleFormSubmit = ({ emdLot: { value }}) => {
    this.props.history.push(`/buyer/submit-emd/${value}`)
  }

  handleRedirect = () => {
    NotificationManager.success("Your EMD will be processed within 1 working day", "EMD Details Received", 5000)
    this.props.history.push('/buyer/dashboard');
  }

  componentDidMount() {
    this.props.getAuctions(getAuthHeaders())
    this.props.clearEMDErrors()
  }

  renderAlert = () => {
    if (this.props.submitEMDError) {
      return (
        <div className="alert alert-danger">
          <strong>Oops!</strong> {this.props.submitEMDError}
        </div>
      )
    }
  }

  render() {
    const { auctions } = this.props;
    if (!auctions) return <div/>
    return (
      <div className="animated fadeIn">
        <Row>
          <Col md={6}>
            <Card>
              <CardHeader>
                Select Auction and Lot
              </CardHeader>
              <CardBody>
                <SelectAuctionForm
                  handleFormSubmit={this.handleFormSubmit}
                  renderAlert={this.renderAlert}
                />
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default SubmitEMDContainer(SubmitEMD);

