import React, { Component } from 'react';
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
} from 'reactstrap';
import {NotificationManager} from 'react-notifications';

import SubmitEMDContainer from '../../../containers/emd'
import EMDForm from './EMDForm';
import {getAuthHeaders} from "../../../constants"

class SubmitEMD extends Component {

  handleFormSubmit = props => {
    const { refNo, amount, submitNote } = props;
    this.props.submitEMD({
      refNo, amount, submitNote,
      lotId: this.props.selectedLot._id,
      redirect: this.handleRedirect.bind(this)
    }, getAuthHeaders());
  }

  handleRedirect = () => {
    NotificationManager.success("Your EMD will be processed within 1 working day", "EMD Details Received", 5000)
    this.props.history.push('/buyer/dashboard');
  }

  componentDidMount() {
    this.props.getLotById(this.props.match.params.lotId, getAuthHeaders())
    this.props.clearEMDErrors()
  }

  renderAlert = () => {
    if (this.props.submitEMDError) {
      return (
        <div className="alert alert-danger">
          <strong>Oops!</strong> {this.props.submitEMDError}
        </div>
      )
    }
  }

  render() {
    const { selectedLot} = this.props;
    if (!selectedLot) return <div/>
    return (
      <div className="animated fadeIn">
        <Row>
          <Col md={6}>
            <Card>
              <CardHeader>
                Submit EMD Details
              </CardHeader>
              <CardBody>
                <h3>Auction ID: {selectedLot.auction.auctionId}</h3>
                <h3>Lot No: #{selectedLot.lotNo}</h3>
                <EMDForm
                  {...this.props}
                  handleFormSubmit={this.handleFormSubmit}
                  renderAlert={this.renderAlert}
                />
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default SubmitEMDContainer(SubmitEMD);

