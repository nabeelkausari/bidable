import React from 'react';
import { Field } from 'redux-form';
import {regularField} from "../../Common/index.js"
import EMDFormContainer from '../../../containers/emd/form';

const EMDForm = props => (
  <form onSubmit={props.handleSubmit(props.handleFormSubmit)}>
    <fieldset className="form-group">
      <Field label="UTR No (Transaction Ref No)" name="refNo" component={regularField} type="text" placeholder="Enter the transaction ref no..." />
    </fieldset>
    <fieldset className="form-group">
      <Field label="Amount Paid" name="amount" component={regularField} type="number" placeholder="Enter the deposited amount..." />
    </fieldset>
    <fieldset className="form-group">
      <Field label="Notes (Optional)" name="submitNote" component={regularField} type="textarea" placeholder="Enter notes if any..." />
    </fieldset>
    {props.renderAlert()}
    <fieldset>
      <button type="submit" className="btn btn-primary">Submit</button>
      <button type="button" onClick={() => props.history.push('/buyer/submit-emd')} className="btn btn-warning float-right">Back</button>
    </fieldset>
  </form>
)

export default EMDFormContainer(EMDForm)
