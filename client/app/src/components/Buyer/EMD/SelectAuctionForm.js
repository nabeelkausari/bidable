import React from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import {select} from "../../Common/index.js"
import {getDateTime} from "../../../constants"
// import { selectEMDLot } from './actions';

const validate = formProps => {
  const errors = {};
  if (!formProps.emdAuction) errors.emdAuction = 'Please select the auction'
  if (!formProps.emdLot) errors.emdLot = 'Please select the lot'
  return errors;
}

const AuctionForm = props => {
  if (!props.auctions) return <div/>

  const upcomingAuctions = props.auctions.filter(({ auctionDate, auctionTime }) => (
    getDateTime(auctionDate, auctionTime.start).isAfter()
  ))

  const auctionList = upcomingAuctions.map(a => ({label: a.auctionId, value: a._id }))

  const getLotList = () => {
    let lotList = []
    if (props.emdAuctionId) {
      lotList = props.auctions.filter(d => d._id === props.emdAuctionId.value)[0].lots
      lotList = lotList.map(l => ({label: `${l.lotNo} - ${l.product.name}`, value: l._id }))
    }
    return lotList
  }

  return (
    <form onSubmit={props.handleSubmit(props.handleFormSubmit)}>
      <fieldset className="form-group">
        <div className="row">
          <div className="col-md-6">
            <Field label="Select Auction" name="emdAuction" component={select} options={auctionList} />
          </div>
          <div className="col-md-6">
            <Field label="Select Lot" name="emdLot" component={select} options={getLotList()} />
          </div>
        </div>
      </fieldset>
      {props.renderAlert()}
      <fieldset>
        <button type="submit" className="btn btn-primary">Go</button>
      </fieldset>
    </form>
  )
}

const selector = formValueSelector('auctionForm')

let AuctionFormBound = reduxForm({
  form: 'auctionForm',
  validate
})(AuctionForm);

function mapStateToProps(state) {
  const emdAuctionId = selector(state, 'emdAuction')
  return {
    ...state.auctions,
    emdAuctionId,
    auctionList: state.auctions.auctions,
  }
}

export default connect(mapStateToProps)(AuctionFormBound)
