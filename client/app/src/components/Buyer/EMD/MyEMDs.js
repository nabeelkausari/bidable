import React, { Component } from 'react';
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  Table,
  Badge
} from 'reactstrap';

import {NotificationManager} from 'react-notifications';

import NotifyModal from '../../Common/NotifyModal';
import EMDContainer from '../../../containers/emd';
import {getAuthHeaders, readableDate, price} from "../../../constants"

class MyEMDs extends Component {
  state = {modal: false}
  hideModal = () => this.setState({ modal: false })

  showRejectionReason = (emd) => {
    this.setState({
      modal: true,
      modalTitle: 'Rejection Reason',
      modalBody: emd.rejectNote
    })
  }

  handleDelete = (emd) => {
    this.props.deleteEMD({
      _id: emd._id,
      redirect: () => {
        this.props.history.push(`/buyer/submit-emd/${emd.lot._id}`)
        setTimeout(NotificationManager.success("Kindly resubmit the emd form with required changes", "Deleted Successfully", 10000))
      }
    }, getAuthHeaders())
  }

  visitLot = (auctionId, lotNo) => {
    this.props.history.push(`/buyer/past-auctions/${auctionId}/${lotNo}`)
  }

  componentDidMount() {
    this.props.getMyEMDs(getAuthHeaders())
  }
  render() {
    const { myEMDs } = this.props;
    const { modal, modalTitle, modalBody } = this.state;
    if (!myEMDs) return <div/>
    return (
      <Row>
        <Col>
          <Card>
            <CardHeader>
              My EMD List
            </CardHeader>
            <CardBody>
              <Table responsive>
                <thead>
                <tr>
                  <th>Auction ID</th>
                  <th>Lot No</th>
                  <th>Seller Name</th>
                  <th>Product</th>
                  <th>Auction Date</th>
                  <th>Ref No</th>
                  <th>EMD Amount</th>
                  <th>Notes</th>
                  <th>Approval Status</th>
                  <th>EMD Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                {!myEMDs.length && <tr>
                  <td className="team-result__date">You have not submitted any EMD yet</td>
                  <td/>
                  <td/>
                  <td/>
                  <td/>
                  <td/>
                  <td/>
                  <td/>
                </tr>}
                {myEMDs.length > 0 && myEMDs.map(emd => (
                  <tr key={"row-data-" + emd._id}>
                    <td><button
                      onClick={() => this.visitLot(emd.lot.auction._id, emd.lot.lotNo)}
                      className="btn btn-link"
                    >{emd.lot.auction.auctionId}</button></td>
                    <td><button
                      onClick={() => this.visitLot(emd.lot.auction._id, emd.lot.lotNo)}
                      className="btn btn-link"
                    >#{emd.lot.lotNo}</button></td>
                    <td>{emd.lot.sellerName}</td>
                    <td>{emd.lot.product.name}</td>
                    <td>{readableDate(emd.lot.auctionDate)}</td>
                    <td>{emd.refNo}</td>
                    <td>{price(emd.amount)}</td>
                    <td>{emd.submitNote || "-"}</td>
                    <td>{emd.approval === "rejected" ? (
                      <button
                        onClick={() => this.showRejectionReason(emd)}
                        className="btn btn-link"
                      >{emd.approval}</button>
                    ) : emd.approval}</td>
                    <td>{emd.status ? <Badge color="success">{emd.status}</Badge> : "-"}</td>
                    <td>
                      {emd.approval === "rejected" ? (
                        <button
                          onClick={() => this.handleDelete(emd)}
                          className="btn btn-danger"
                        >Delete & Resubmit</button>
                      ) : "-"}
                    </td>
                  </tr>
                ))}
                </tbody>
              </Table>
            </CardBody>
          </Card>
          <NotifyModal
            onClose={this.hideModal}
            modal={modal}
            title={modalTitle}
            body={modalBody}
            showConfirmBtn
          />
        </Col>
      </Row>
    );
  }
}

export default EMDContainer(MyEMDs);
