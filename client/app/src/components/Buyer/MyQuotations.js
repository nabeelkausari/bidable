import React, { Component } from 'react';
import {
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Row,
  Table,
  Badge
} from 'reactstrap';

import QuotationsContainer from '../../containers/lots';
import {getAuthHeaders, readableDate, price} from "../../constants"

class MyQuotations extends Component {

  visitLot = (auctionId, lotNo) => {
    this.props.history.push(`/buyer/past-auctions/${auctionId}/${lotNo}`)
  }

  componentDidMount() {
    this.props.getMyQuotations(getAuthHeaders())
  }
  render() {
    const { myQuotations } = this.props;
    if (!myQuotations) return <div/>
    return (
      <Row>
        <Col>
          <Card>
            <CardHeader>
              My Quotations
            </CardHeader>
            <CardBody>
              <Table responsive>
                <thead>
                <tr>
                  <th>Auction ID</th>
                  <th>Lot No</th>
                  <th>Seller Name</th>
                  <th>Product</th>
                  <th>Auction Date</th>
                  <th>Status</th>
                  <th>Highest Bid</th>
                  <th>Your Quotation</th>
                  <th>Approved</th>
                </tr>
                </thead>
                <tbody>
                {!myQuotations.length && <tr>
                  <td className="team-result__date">You have not placed any quotations yet</td>
                  <td/>
                  <td/>
                  <td/>
                  <td/>
                  <td/>
                  <td/>
                </tr>}
                {myQuotations.length > 0 && myQuotations.map(q => (
                  <tr key={"row-data-" + q._id}>
                    <td><button
                      onClick={() => this.visitLot(q.auction._id, q.lot.lotNo)}
                      className="btn btn-link"
                    >{q.auction.auctionId}</button></td>
                    <td><button
                      onClick={() => this.visitLot(q.auction._id, q.lot.lotNo)}
                      className="btn btn-link"
                    >{q.lot.lotNo}</button></td>
                    <td>{q.lot.sellerName}</td>
                    <td>{q.lot.product.name}</td>
                    <td>{readableDate(q.lot.auctionDate)}</td>
                    <td>{q.lot.highestBid && q.lot.highestBid.price ? <Badge color="danger">Invalid</Badge> : <Badge>Valid</Badge>}</td>
                    <td>{q.lot.highestBid && q.lot.highestBid.price ? price(q.lot.highestBid.price): "-"}</td>
                    <td>{price(q.price)}</td>
                    <td>{getApprovalStatus(q)}</td>
                  </tr>
                ))}
                </tbody>
              </Table>
            </CardBody>
          </Card>
        </Col>
      </Row>
    );
  }
}

const getApprovalStatus = q => {
  if (q.approved === false) {
    return "No"
  } else if (q.approved === true) {
    return "Yes"
  } else {
    return "-"
  }
}

export default QuotationsContainer(MyQuotations);
