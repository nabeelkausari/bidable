import React from 'react';
import Layout from '../Common/Layout';

import BuyerLayoutContainer from '../../containers/auth/buyer'
import sideBarNav from '../../buyerNav';
import routes from '../../buyerRoutes';

const BuyerLayout = props => (
  <Layout
    name="Bidable Buyer"
    navConfig={sideBarNav}
    routes={routes}
    type="buyer"
    handleLogout={() => {props.logoutBuyer({ redirect: () => props.history.push('/buyer/login')})}}
    {...props}
  />
)

export default BuyerLayoutContainer(BuyerLayout)
