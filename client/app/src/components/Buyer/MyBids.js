import React, { Component } from 'react';
import {
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Row,
  Table,
} from 'reactstrap';

import BidsContainer from '../../containers/lots';
import {getAuthHeaders, readableDate, readableTime, regularTime, getDateTime, price} from "../../constants"
import NotifyModal from '../Common/NotifyModal';
import moment from "moment/moment"

class MyBids extends Component {

  state = {
    modal: false,
    bids: []
  };

  handleRowClick = bids => this.setState({ modal: true, bids })
  toggleModal = () => this.setState({ modal: !this.state.modal })

  visitLot = (auctionId, lotNo) => {
    this.props.history.push(`/buyer/past-auctions/${auctionId}/${lotNo}`)
  }

  componentDidMount() {
    this.props.getMyBids(getAuthHeaders())
  }
  render() {
    const { myBids } = this.props;
    if (!myBids) return <div/>
    return (
      <Row>
        <Col>
          <Card>
            <CardHeader>
              My Bids
            </CardHeader>
            <CardBody>
              <Table responsive>
                <thead>
                <tr>
                  <th>Auction ID</th>
                  <th>Lot No</th>
                  <th>Seller Name</th>
                  <th>Product</th>
                  <th>Auction Date</th>
                  <th>Highest Bid</th>
                  <th>Your Final Bid</th>
                  <th>Status</th>
                  <th>View All Bids</th>
                  <th>Lot Page</th>
                </tr>
                </thead>
                <tbody>
                {!myBids.length && <tr>
                  <td className="team-result__date">You have not placed any bids yet</td>
                  <td/>
                  <td/>
                  <td/>
                  <td/>
                  <td/>
                  <td/>
                </tr>}
                {myBids.length > 0 && myBids.map(lot => (
                  <tr key={"row-data-" + lot._id}>
                    <td>{lot.auction.auctionId}</td>
                    <td>{lot.lotNo}</td>
                    <td>{lot.sellerName}</td>
                    <td>{lot.product.name}</td>
                    <td>{readableDate(lot.auctionDate)}</td>
                    <td>{price(lot.highestBid.price)}</td>
                    <td>{price(lot.bids[0].price)}</td>
                    <td>{getWinningStatus(lot)}</td>
                    <td><button onClick={() => this.handleRowClick(lot.bids)} className="btn btn-link">All Bids</button></td>
                    <td>
                      <button
                        onClick={() => this.visitLot(lot.auction._id, lot.lotNo)}
                        className="btn btn-link"
                      >Visit lot</button>
                    </td>
                  </tr>
                ))}
                </tbody>
              </Table>
            </CardBody>
          </Card>
          <NotifyModal
            onClose={() => this.setState({ modal: false, bids: [] })}
            modal={this.state.modal}
            title="All your bids"
            body={<ShowBids user={this.props.user} bids={this.state.bids}/>}
            showConfirmBtn
            toggle={this.toggleModal}
          />
        </Col>
      </Row>
    );
  }
}

const getWinningStatus = lot => {
  let highestBidder = lot.bids[0]._id === lot.highestBid.id
  if (lot.liveStatus !== "past") {
    return "-"
  } else {
    return highestBidder ? "Won" : "Lost"
  }
}

export const ShowBids = ({ bids, user }) => {
  return (
    <Table responsive className="headless">
      <thead>
      <tr>
        <th>Bid Price</th>
        <th>Bidding Time</th>
        <th>Bidder</th>
      </tr>
      </thead>
      <tbody>
      {bids.map(bid => (
        <tr key={bid._id}>
          <td><strong>{price(bid.price)}</strong></td>
          <td>{regularTime(bid.createdAt)}</td>
          <td>{bid.bidder === user._id ? <span className="your-bid">You</span> : "Others"}</td>
        </tr>
      ))}
      </tbody>
    </Table>
  )
}

export default BidsContainer(MyBids);
