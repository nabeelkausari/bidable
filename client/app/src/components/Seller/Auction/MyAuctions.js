import React, { Component } from 'react';
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  Table,
  Badge,
  Button
} from 'reactstrap';

import Status from '../../Common/Status';
import AuctionsContainer from '../../../containers/auctions/index';
import {readableDate, readableTime, regularDateTime} from "../../../constants"

class MyAuctions extends Component {

  componentWillMount() {
    this.props.getMyAuctions();
  }

  render() {
    const { myAuctions } = this.props
    if (!myAuctions) return <div/>
    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            <Card>
              <CardHeader>
                My Auctions
              </CardHeader>
              <CardBody>
                <Table hover responsive>
                  <thead>
                  <tr>
                    <th>Creation Date</th>
                    <th>Auction ID</th>
                    <th>Company Name</th>
                    <th>Inspection Dates</th>
                    <th>Inspection Timings</th>
                    <th>Auction Status</th>
                    <th>No of Lots</th>
                    <th>Auction Date</th>
                  </tr>
                  </thead>
                  <tbody>
                  {!myAuctions.length && <tr>
                    <td className="team-result__date">You have not created any auction yet</td>
                    <td className="team-result__date">
                      <Button
                        className={'mx-auto'}
                        color={'primary'}
                        size='sm'
                        onClick={() => this.props.history.push(`/seller/create-auction`)}
                      >Create Auction</Button>
                    </td>
                    <td/>
                    <td/>
                    <td/>
                    <td/>
                    <td/>
                  </tr>}
                  {myAuctions.length > 0 && myAuctions.map((auction, i) => (
                    <AuctionRow key={i} {...this.props} auction={auction}/>
                  ))}
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

class AuctionRow extends Component {


  viewDetails = auctionId => {
    this.props.history.push(`/seller/auctions/${auctionId}`)
  }

  render() {
    const { auction } = this.props
    const [time] = auction.inspectionDetails.timings;
    return (
      <tr style={{ cursor: 'pointer' }} onClick={() => this.viewDetails(auction._id)}>
        <td>{regularDateTime(auction.createdAt)}</td>
        <td>{auction.auctionId}</td>
        <td>{auction.company}</td>
        <td>{readableDate(auction.inspectionDetails.from)} - {readableDate(auction.inspectionDetails.to)}</td>
        <td>{readableTime(time.from)} - {readableTime(time.to)}</td>
        <td><Status status={auction.status}/></td>
        <td>{auction.lots.length}</td>
        <td>{auction.auctionDate ? readableDate(auction.auctionDate) : "Not Assigned"}</td>
      </tr>
    )
  }
}

export default AuctionsContainer(MyAuctions);
