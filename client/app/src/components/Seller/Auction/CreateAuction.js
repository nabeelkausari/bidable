import React, { Component } from 'react';
import {Card, CardBody, CardHeader, Col, Row } from 'reactstrap';
import {NotificationManager} from 'react-notifications';

import CreateAuctionContainer from '../../../containers/auctions/createAuction';
import AuctionForm from './AuctionForm';
import RegisterCompany from '../Company/RegisterCompany';

class CreateAuction extends Component {

  handleFormSubmit = props => {
    const { inspectionDates: [ from, to ], inspectionTimeFrom,
      inspectionTimeTo, auctionDate } = props
    this.props.createAuction({
      inspectionDetails: {
        from,
        to,
        timings: [{
          from: inspectionTimeFrom,
          to: inspectionTimeTo
        }]
      },
      preferredDate: auctionDate,
      redirect: this.handleRedirection.bind(this)
    });
  }

  handleRedirection = (auction) => {
    NotificationManager.success("Your auction is created successfully, add a 'LOT' to your auction", "Auction Created", 5000)
    this.props.history.push(`/seller/auctions/${auction._id}`)
  }

  renderAlert = () => {
    if (this.props.createAuctionError) {
      return (
        <div className="alert alert-danger">
          <strong>Oops!</strong> {this.props.createAuctionError}
        </div>
      )
    }
  }

  render() {
    if (!this.props.user) return <div/>;
    const { businessVerified, businessSubmitted } = this.props.user
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" sm="6">
            {
              businessVerified
                ? <Card>
                  <CardHeader>
                    <strong>Create New Auction</strong>
                  </CardHeader>
                  <CardBody>
                    <AuctionForm
                      {...this.props}
                      handleFormSubmit={this.handleFormSubmit}
                      renderAlert={this.renderAlert}
                      actionText="Create Auction"
                      cancelAction={() => this.props.history.push('/seller/auctions')}
                    />
                  </CardBody>
                </Card>
                : <Card>
                  <CardHeader>
                    Business Details
                  </CardHeader>
                  {
                    businessSubmitted
                      ? <CardBody>
                          <h3>Verification pending</h3>
                          <p className="lead mb-5">We have received your business details. Our team is verifying the details. This process will be completed within 2 working days.</p>
                        </CardBody>
                      : <CardBody>
                        <div className="text-center mb-4">
                          <h3>Register your company</h3>
                          <p className="lead">In order to create an auction, you must submit your business details</p>
                        </div>
                        <RegisterCompany  history={this.props.history}/>
                      </CardBody>
                  }
                </Card>
            }
          </Col>
        </Row>
      </div>
    );
  }
}

export default CreateAuctionContainer(CreateAuction);
