import React, { Component } from 'react';
import {Card, CardBody, CardHeader, Col, Row, Button } from 'reactstrap';
import {NotificationManager} from 'react-notifications';

import NotifyModal from '../../Common/NotifyModal';
import EditAuctionContainer from '../../../containers/auctions/editAuction';
import AuctionForm from './AuctionForm';

class EditAuction extends Component {
  state = {modal: false}
  handleFormSubmit = ({ auctionDate, inspectionDates: [ from, to ], inspectionTimeFrom, inspectionTimeTo }) => {
    this.props.editAuction({
      inspectionDetails: {
        from,
        to,
        timings: [{
          from: inspectionTimeFrom,
          to: inspectionTimeTo
        }]
      },
      preferredDate: auctionDate,
      redirect: () => {
        this.backToDetails();
        setTimeout(() => NotificationManager.success("", "Auction updated successfully", 5000), 1000)
      },
      auctionId: this.props.auctionToEdit._id
    });
  }


  handleDelete = () => {
    this.props.deleteAuction(this.props.auctionToEdit._id);
    this.props.getMyAuctions();
    this.props.history.push('/seller/auctions')
    setTimeout(() => NotificationManager.success("", "Auction deleted successfully", 5000), 1000)
  }

  componentDidMount() {
    if (!this.props.auctionToEdit) {
      this.props.getAuctionToEdit(this.props.match.params.auctionId)
    }
  }

  renderAlert = () => {
    if (this.props.updateAuctionError) {
      return (
        <div className="alert alert-danger">
          <strong>Oops!</strong> {this.props.updateAuctionError}
        </div>
      )
    }
  }

  backToDetails = () => this.props.history.push(`/seller/auctions/${this.props.auctionToEdit._id}`)

  render() {
    if (!this.props.auctionToEdit) return <div/>;
    const { status, auctionId } = this.props.auctionToEdit;
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" sm="6">
            {status === "draft" && <Card>
              <CardHeader>
                <strong>Edit Auction Id: {auctionId}</strong>
                <Button
                  className={'float-right mb-0'}
                  color={'danger'}
                  size='sm'
                  onClick={() => this.setState({ modal: true })}
                >Delete Auction</Button>
              </CardHeader>
              <CardBody>
                <AuctionForm
                  {...this.props}
                  handleFormSubmit={this.handleFormSubmit}
                  renderAlert={this.renderAlert}
                  actionText="Update Auction"
                  cancelAction={this.backToDetails}
                />
              </CardBody>
            </Card>}
            <NotifyModal
              modal={this.state.modal}
              title="Delete Auction"
              body="Are you sure you want to delete this auction?"
              showConfirmBtn
              showCancelBtn
              onCancel={() => this.setState({modal: false})}
              onClose={this.handleDelete}
              confirmBtnText="Delete"
              confirmBtnClass="danger"
            />
          </Col>
        </Row>
      </div>
    );
  }
}

export default EditAuctionContainer(EditAuction);
