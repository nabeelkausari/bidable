import React, { Component } from 'react';
import { Button, Card, CardBody, CardHeader, Col, Row, Table } from 'reactstrap';
import {NotificationManager} from 'react-notifications';

import NotifyModal from '../../Common/NotifyModal';
import Status from '../../Common/Status';
import AuctionDetailContainer from '../../../containers/auctions/detail';
import AddLot from '../Lot/AddLot';
import EditLot from '../Lot/EditLot';
import LotList from "../Lot/LotList"
import { readableDate, readableTime } from "../../../constants"

class AuctionDetail extends Component {
  state = {
    submitInProcess: false,
    createLotVisible: false,
    editLotVisible: false,
    modal: false,
    auctionLoading: true,
  }

  handleFormSubmit = ({ company, inspectionDates: [ from, to ], inspectionTimeFrom, inspectionTimeTo }) => {
    this.props.createAuction({
      company,
      inspectionDetails: {
        from,
        to,
        timings: [{
          from: inspectionTimeFrom,
          to: inspectionTimeTo
        }]
      },
      redirect: () => this.props.history.push('/seller/auctions')
    });
  }

  renderAlert = () => {
    if (this.props.createAuctionError) {
      return (
        <div className="alert alert-danger">
          <strong>Oops!</strong> {this.props.createAuctionError}
        </div>
      )
    }
  }

  handleAuctionSubmit = auction => {
    this.setState({ submitInProcess: true });
    if (auction.lots.length === 0) {
      NotificationManager.error("An auction should have at least one lot before submitting", "Not Eligible", 5000)
      return this.setState({ submitInProcess: false });
    }
    if (auction.status === "draft") {
      this.setState({ modal: true })
    }
  }

  processAuctionSubmit = () => {
    this.props.submitAuction({
      auctionId: this.props.selectedAuction._id,
      redirect: () => {
        this.props.history.push('/seller/auctions')
        NotificationManager.success("Your auction has been submitted for review", "Success", 5000)
      }
    });
  }

  startEdit = auction => {
    this.props.selectAuctionToEdit(auction);
    this.props.history.push(`/seller/edit-auction/${auction._id}`)
  }

  startLotEdit = (lot, lotNo) => {
    this.props.selectLotToEdit({...lot, lotNo});
    this.setState({ editLotVisible: true, createLotVisible: false })
  }
  doneLotEdit = () => this.setState({ editLotVisible: false })

  startLotCreate = () => this.setState({ createLotVisible: true, editLotVisible: false })
  doneLotCreate = () => this.setState({ createLotVisible: false })

  componentDidMount() {
    this.props.getSelectedAuction(this.props.match.params.auctionId)
  }

  componentDidUpdate(prevProps) {
    let getAuctionTriggered = prevProps.getAuctionLoading !== this.props.getAuctionLoading;
    let getAuctionLoaded = this.props.getAuctionLoading === false;
    if (getAuctionTriggered && getAuctionLoaded) {
      this.setState({ auctionLoading: false })
    }
  }

  render() {
    const { createLotVisible, editLotVisible, auctionLoading } = this.state;
    if (!this.props.selectedAuction) return (
      <div className="animated fadeIn">
        <Row>
          <div className="col-md-6">
            <h2>{auctionLoading ? "Loading..." : "Auction Not Found"}</h2>
          </div>
        </Row>
      </div>
    )
    const {
      auctionId, company, inspectionDetails: { from, to, timings: [time]}, status, lots,
      auctionDate, auctionTime
    } = this.props.selectedAuction;
    return (
      <div className="animated fadeIn">
        <Row>
          <Col md="6">
            <Card>
              <CardHeader>
                Auction Details
                {status === "draft" && <Button
                  className={'float-right mb-0'}
                  color={'warning'}
                  size='sm'
                  onClick={() => this.startEdit(this.props.selectedAuction)}
                >Edit Auction</Button>}
                {status === "draft" && <Button
                  className={'float-right mb-0 mr-3'}
                  color={'success'}
                  size='sm'
                  disabled={this.state.submitInProcess}
                  onClick={() => this.handleAuctionSubmit(this.props.selectedAuction)}
                >Submit Auction</Button>}
              </CardHeader>
              <CardBody>
                <Table responsive className="headless">
                  <tbody>
                  <tr>
                    <th>Auction ID</th>
                    <td>{auctionId}</td>
                  </tr>
                  <tr>
                    <th>Company Name</th>
                    <td>{company}</td>
                  </tr>
                  <tr>
                    <th>Inspection Dates</th>
                    <td>{readableDate(from)} - {readableDate(to)}</td>
                  </tr>
                  <tr>
                    <th>Inspection Timings</th>
                    <td>{readableTime(time.from)} - {readableTime(time.to)}</td>
                  </tr>
                  <tr>
                    <th>Auction Status</th>
                    <td><Status status={status}/></td>
                  </tr>
                  <tr>
                    <th>Auction Date</th>
                    <td>{auctionDate && readableDate(auctionDate) || "Not Assigned"}</td>
                  </tr>
                  <tr>
                    <th>Auction Timings</th>
                    <td>{(auctionTime && `${readableTime(auctionTime.start)} - ${readableTime(auctionTime.end)}`) || "Not Assigned"}</td>
                  </tr>
                  </tbody>
                </Table>
              </CardBody>
            </Card>
            {createLotVisible && <Row className="d-md-none">
              <Col md="6" className="animated bounceInRight">
                <AddLot {...this.props} hideLotWidget={this.doneLotCreate} />
              </Col>
            </Row>}
            { !editLotVisible && <Card className="d-md-none">
              <CardHeader>
                Auction Lots
                {status === "draft" && <Button
                  className={'float-right mb-0'}
                  color={'primary'}
                  onClick={this.startLotCreate}
                >Create Lot</Button>}
              </CardHeader>
              <CardBody>
                <LotList auction={this.props.selectedAuction} startLotEdit={this.startLotEdit} lots={lots}/>
              </CardBody>
            </Card>}
            <Card className="d-none d-md-block">
              <CardHeader className="pb-4">
                Auction Lots
                {status === "draft" && <Button
                  className={'float-right mb-0'}
                  color={'primary'}
                  onClick={this.startLotCreate}
                >Create Lot</Button>}
              </CardHeader>
              <CardBody>
                <LotList auction={this.props.selectedAuction} startLotEdit={this.startLotEdit} lots={lots}/>
              </CardBody>
            </Card>
          </Col>
          {createLotVisible && <Col md="6" className="animated bounceInRight d-none d-md-block">
            <AddLot {...this.props} hideLotWidget={this.doneLotCreate} />
          </Col>}
          {editLotVisible && <Col md="6" className="animated bounceInRight">
            <EditLot {...this.props} doneLotEdit={this.doneLotEdit} />
          </Col>}
          <NotifyModal
            onClose={this.processAuctionSubmit}
            modal={this.state.modal}
            title="Submit Auction"
            body="Your auction is being submitted to our verification department to review. Once it is verified and approved, your auction will be listed on the platform and it will go live"
            showConfirmBtn
            showCancelBtn
            onCancel={() => this.setState({modal: false, submitInProcess: false})}
            confirmBtnText="Submit"
            confirmBtnClass="primary"
          />
        </Row>
      </div>
    );
  }
}


export default AuctionDetailContainer(AuctionDetail);
