import React from 'react';
import Layout from '../Common/Layout';

import SellerLayoutContainer from '../../containers/auth/seller'
import sideBarNav from '../../sellerNav';
import routes from '../../sellerRoutes';

const SellerLayout = props => (
  <Layout
    name="Bidable Seller"
    navConfig={sideBarNav}
    routes={routes}
    type="seller"
    handleLogout={() => {props.logoutSeller({ redirect: () => props.history.push('/seller/login')})}}
    {...props}
  />
)

export default SellerLayoutContainer(SellerLayout)
