import React, { Component } from 'react';
import {Card, CardBody, CardHeader } from 'reactstrap';
import {NotificationManager} from 'react-notifications';

import AddLotContainer from '../../../containers/auctions/addLot';
import LotForm from './LotForm';

class AddLot extends Component {
  handleFormSubmit = props => {
    const {
      productName, productLocation, productDescription, ...restProps} = props;
    this.props.createLot({
      ...restProps,
      auctionId: this.props.selectedAuction._id,
      product: {
        name: productName,
        location: productLocation,
        description: productDescription
      },
      redirect: this.handleRedirect
    });
  }

  handleRedirect = () => {
    this.props.hideLotWidget()
    this.props.getSelectedAuction(this.props.selectedAuction._id)
    NotificationManager.success("", "Lot created successfully", 5000)
  }

  renderAlert = () => {
    if (this.props.createLotError) {
      return (
        <div className="alert alert-danger">
          <strong>Oops!</strong> {this.props.createLotError}
        </div>
      )
    }
  }

  render() {
    return (
      <div className="animated fadeIn">
        <Card className="border-primary">
          <CardHeader className="bg-primary">
            Create New Lot
            <div className="card-header-actions">
              <a className="card-header-action btn btn-close" onClick={this.props.hideLotWidget}><i className="icon-close"/></a>
            </div>
          </CardHeader>
          <CardBody>
            <LotForm
              {...this.props}
              handleFormSubmit={this.handleFormSubmit}
              renderAlert={this.renderAlert}
              actionText={`Create Lot`}
            />
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default AddLotContainer(AddLot);
