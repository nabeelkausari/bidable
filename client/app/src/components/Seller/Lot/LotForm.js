import React from 'react';
import { Field } from 'redux-form';
import {Button, Col, Row } from 'reactstrap';
import {Decimal} from 'decimal.js';

import {regularField} from "../../Common/index"
import {price} from "../../../constants"

export default props => {
  return (
    <form onSubmit={props.handleSubmit(props.handleFormSubmit)}>
      <fieldset className="form-group">
        <div className="row">
          <div className="col-md-6">
            <Field label="Product Name" id="productName" name="productName" component={regularField} type="text" placeholder="Enter product name..." />
          </div>
          <div className="col-md-6">
            <Field label="Product Location" id="productLocation" name="productLocation" component={regularField} type="text" placeholder="Enter product location..." />
          </div>
        </div>
      </fieldset>

      <fieldset className="form-group">
        <Field label="Product Description" id="productDescription" name="productDescription" component={regularField} type="textarea" placeholder="Enter product description..." />
      </fieldset>

      <fieldset className="form-group">
        <div className="row">
          <div className="col-md-6">
            <Field label="Starting Price (INR)" id="startingPrice" name="startingPrice" component={regularField} type="number" placeholder="Enter price..." />
          </div>
          <div className="col-md-6">
            <Field
              label="Per"
              id="bidPer"
              name="bidPer"
              component={regularField}
              type="select"
              placeholder="Select Unit"
              options={["Metric Tonne", "Kg", "Pc", "Lot"]}
            />
          </div>
        </div>
      </fieldset>

      <fieldset className="form-group">
        <div className="row">
          <div className="col-md-4">
            <div className="form-group">
              <label>Service Fee (2%)</label>
              <label className="form-control">
                <Field id="serviceFee" name="serviceFee" component="input" type="checkbox"/>
                {props.lotValues.serviceFee ? " Inclusive" : " Exclusive"}
              </label>
            </div>
          </div>
          <div className="col-md-8 price-box">
            <div className="seller">
              <span>You receive</span>
              <h5>{priceView(props).seller}</h5>
              <span>{unitView(props)}</span>
            </div>
            <div className="buyer">
              <span>User pays</span>
              <h5><strong>{priceView(props).buyer}</strong></h5>
              <span>{unitView(props)}</span>
            </div>
          </div>
        </div>
      </fieldset>

      <fieldset className="form-group">
        <div className="row">
          <div className="col-md-6">
            <Field label="Qty (Approx)" id="quantity" name="quantity" component={regularField} type="number" placeholder="Approx Qty" />
          </div>
          <div className="col-md-6">
            <Field
              label="Unit"
              id="unit"
              name="unit"
              component={regularField}
              type="select"
              placeholder="Select Unit"
              options={["Metric Tonne", "Kg", "Pc", "Lot"]}
            />
          </div>
        </div>
      </fieldset>

      <fieldset className="form-group">
        <div className="row">
          <div className="col-md-6">
            <Field label="Increment Value (INR)" id="minimumIncrement" name="minimumIncrement" component={regularField} type="number" placeholder="Enter increment value..." />
          </div>
          <div className="col-md-6">
            <Field label="Applicable GST (%)" id="gst" name="gst" component={regularField} type="number" placeholder="Enter the applicable GST percent" />
          </div>
        </div>
      </fieldset>

      <fieldset className="form-group">
        <div className="row">
          <div className="col-md-12">
            <div className="form-group">
              <label>Is PCB Required?</label>
              <label className="form-control">
                <Field id="pcbRequired" name="pcbRequired" component="input" type="checkbox"/>{" "}
                Yes
              </label>
            </div>
          </div>
        </div>
      </fieldset>

      {props.renderAlert()}
      <Row>
        <Col className="text-center">
          <Button action="submit" color="primary" className="px-4">{props.actionText}</Button>
        </Col>
      </Row>
    </form>
  )
}

const priceView = (props) => {
  const { startingPrice, serviceFee } = props.lotValues;
  const sp = new Decimal(startingPrice || 0);
  const seller = price(serviceFee ? sp.sub(sp.mul(2).div(100)) : sp)
  const buyer = price(!serviceFee ? sp.add(sp.mul(2).div(100)) : sp)
  return sp ? {seller, buyer} : {seller: "", buyer: ""}
}

const unitView = (props) => {
  const { startingPrice, bidPer } = props.lotValues;
  return startingPrice && bidPer ? "(per " + bidPer + ")" : "";
}
