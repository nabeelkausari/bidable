import React, { Component } from 'react';
import {Card, CardBody, CardHeader, Button } from 'reactstrap';
import {NotificationManager} from 'react-notifications';

import NotifyModal from '../../Common/NotifyModal';
import EditLotContainer from '../../../containers/auctions/editLot';
import LotForm from './LotForm';

class EditLot extends Component {
  state = {modal: false}
  handleFormSubmit = props => {
    const {
      productName, productLocation, productDescription, ...restProps } = props;
    this.props.editLot({
      ...restProps,
      lotId: this.props.lotToEdit._id,
      product: {
        name: productName,
        location: productLocation,
        description: productDescription
      },
      redirect: this.handleRedirect
    });
  }

  handleRedirect = () => {
    this.props.doneLotEdit()
    this.props.getSelectedAuction(this.props.selectedAuction._id)
    NotificationManager.success("", "Lot updated successfully", 5000)
  }

  handleDelete = () => {
    this.props.doneLotEdit()
    this.props.deleteLot(this.props.lotToEdit._id);
    NotificationManager.success("", "Lot deleted successfully", 5000)
  }

  renderAlert = () => {
    if (this.props.updateLotError) {
      return (
        <div className="alert alert-danger">
          <strong>Oops!</strong> {this.props.updateLotError}
        </div>
      )
    }
  }

  render() {
    return (
      <div className="animated fadeIn">
        <Card className="border-warning">
          <CardHeader className="text-dark bg-warning">
            Edit Lot #{this.props.initialValues.lotNo}
            <Button
              className={'float-right mb-0'}
              color={'secondary'}
              size='sm'
              onClick={this.props.doneLotEdit}
            >Close</Button>
            <Button
              className={'float-right mb-0 mr-2'}
              color={'danger'}
              size='sm'
              onClick={() => this.setState({ modal: true })}
            >Delete</Button>
          </CardHeader>
          <CardBody>
            <LotForm
              {...this.props}
              handleFormSubmit={this.handleFormSubmit}
              renderAlert={this.renderAlert}
              actionText={`Update Lot #${this.props.initialValues.lotNo}`}
            />
          </CardBody>
          <NotifyModal
            modal={this.state.modal}
            title="Delete Lot"
            body="Are you sure you want to delete this lot?"
            showConfirmBtn
            showCancelBtn
            onCancel={() => this.setState({modal: false})}
            onClose={this.handleDelete}
            confirmBtnText="Delete"
            confirmBtnClass="danger"
          />
        </Card>
      </div>
    );
  }
}

export default EditLotContainer(EditLot);
