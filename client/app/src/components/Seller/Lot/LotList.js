import React, { Component } from 'react';
import { Button, Card, CardBody, CardHeader, Collapse, Table } from 'reactstrap';

import LotContainer from '../../../containers/auctions/listLot';
import {price} from "../../../constants"

class LotList extends Component {
  state = {
    accordion: null,
  };

  toggleAccordion = tab => {
    const prevState = this.state.accordion;
    const state = prevState.map((x, index) => tab === index ? !x : false);

    this.setState({
      accordion: state
    });
  }

  componentWillMount() {
    if (this.props.lots) {
      let accordion = [];
      this.props.lots.forEach(() => accordion.push(false))
      accordion[accordion.length - 1] = true;
      this.setState({ accordion })
    }
  }

  render() {
    const { lots, auction } = this.props;
    if (!lots) return <div/>
    return (
      <div className="auction-lots">
        {lots.length === 0 && <div>
          <p>No lots created</p>
        </div>}
        {lots.length > 0 && <div id="accordion">
          {lots.map((lot, i) => (
            <Card key={i}>
              <CardHeader id="headingOne">
                <Button block color="link" className="text-left m-0 p-0" onClick={() => this.toggleAccordion(i)} aria-expanded={this.state.accordion[i]}>
                  <h5 className="m-0 p-0">Lot no #{i+1}</h5>
                </Button>
                {auction.status === "draft" && <Button
                  className={'lot-edit-btn'}
                  color={'warning'}
                  size='sm'
                  onClick={() => this.props.startLotEdit(lot, i+1)}
                >Edit Lot</Button>}
              </CardHeader>
              <Collapse isOpen={this.state.accordion[i]} data-parent="#accordion" aria-labelledby="headingOne">
                <CardBody>
                  <Table responsive className="headless">
                    <tbody>
                    <tr>
                      <th>Seller Name</th>
                      <td>{lot.sellerName}</td>
                    </tr>
                    <tr>
                      <th>Product Name</th>
                      <td>{lot.product.name}</td>
                    </tr>
                    <tr>
                      <th>Product Location</th>
                      <td>{lot.product.location}</td>
                    </tr>
                    <tr>
                      <th>Product Description</th>
                      <td>{lot.product.description}</td>
                    </tr>
                    <tr>
                      <th>Starting Price</th>
                      <td>{price(lot.startingPrice)} (Per {lot.bidPer})</td>
                    </tr>
                    <tr>
                      <th>Service Charges</th>
                      <td>{lot.serviceFee ? "Free" : "2%"}</td>
                    </tr>
                    <tr>
                      <th>Increment Value</th>
                      <td>{price(lot.minimumIncrement)}</td>
                    </tr>
                    <tr>
                      <th>Product Quantity</th>
                      <td>{lot.quantity} {lot.unit} (Approx)</td>
                    </tr>
                    <tr>
                      <th>GST</th>
                      <td>{lot.gst}%</td>
                    </tr>
                    <tr>
                      <th>PCB Certification</th>
                      <td>{lot.pcbRequired ? "Required" : "Not Required"}</td>
                    </tr>
                    </tbody>
                  </Table>
                </CardBody>
              </Collapse>
            </Card>
          ))}
        </div>}
      </div>
    )
  }
}

export default LotContainer(LotList)
