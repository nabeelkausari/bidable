import React, { Component } from 'react';
import {Col, Row} from 'reactstrap';

import { SellerDashboard } from '../../containers/dashboard'

import Widget from '../Common/Widget'

class Dashboard extends Component {
  componentDidMount() {
    this.props.getSellerStats();
  }
  handleLogout = () => {
    this.props.logoutBuyer({ redirect: () => this.props.history.push('/seller/login')})
  }
  render() {
    if (!this.props.sellerStats || !this.props.user) return <div/>
    const { auctionDraft, auctionReview, auctionLive } = this.props.sellerStats
    const { userId, email } = this.props.user;
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" sm="6" lg="3">
            <Widget header={userId} mainText={email} icon="fa fa-user" color="primary" footer
                    onFooterClick={this.handleLogout.bind(this)}
                    footerText="Logout"
            />
          </Col>
          <Col xs="12" sm="6" lg="3">
            <Widget header={auctionDraft.toString()} mainText="No of Auctions (Draft)" icon="fa fa-laptop" color="secondary" footer link="/app/seller/auctions" />
          </Col>
          <Col xs="12" sm="6" lg="3">
            <Widget header={auctionReview.toString()} mainText="No of Auctions (In Review)" icon="fa fa-laptop" color="warning" footer link="/app/seller/auctions" />
          </Col>
          <Col xs="12" sm="6" lg="3">
            <Widget header={auctionLive.toString()} mainText="No of Auctions (Live)" icon="fa fa-laptop" color="success" footer link="/app/seller/auctions" />
          </Col>
        </Row>
      </div>
    );
  }
}

export default SellerDashboard(Dashboard);
