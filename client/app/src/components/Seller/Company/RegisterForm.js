import React from 'react';
import { Field } from 'redux-form';
import {regularField} from "../../Common/index"

export default props => (
  <form onSubmit={props.handleSubmit(props.handleFormSubmit)}>
    <fieldset className="form-group">
      <Field icon="icon-user" label="Company Representative Full Name" name="fullName" component={regularField} type="text" placeholder="Enter your full name..." />
    </fieldset>
    <fieldset className="form-group">
      <Field icon="icon-factory" label="Company Name" name="companyName" component={regularField} type="text" placeholder="Enter your company name..." />
    </fieldset>
    <fieldset className="form-group">
      <Field icon="icon-factory" label="Company Address" name="address" component={regularField} type="text" placeholder="Enter address..." />
    </fieldset>
    <fieldset className="form-group">
      <div className="row">
        <div className="col-md-6">
          <Field icon="icon-factory" label="City" name="city" component={regularField} type="text" placeholder="Enter city..." />
        </div>
        <div className="col-md-6">
          <Field icon="icon-factory" label="State" name="state" component={regularField} type="text" placeholder="Enter state..." />
        </div>
      </div>
    </fieldset>
    <fieldset className="form-group">
      <div className="row">
        <div className="col-md-6">
          <Field icon="icon-factory" label="Pin Code" name="pinCode" component={regularField} type="text" placeholder="Enter pincode..." />
        </div>
        <div className="col-md-6">
          <Field icon="icon-factory" label="Phone No" name="phone" component={regularField} type="text" placeholder="Enter phone no..." />
        </div>
      </div>
    </fieldset>
    <fieldset className="form-group">
      <div className="row">
        <div className="col-md-6">
          <Field icon="icon-factory" label="PAN" name="pan" component={regularField} type="text" placeholder="Enter PAN number..." />
        </div>
        <div className="col-md-6">
          <Field icon="icon-factory" label="GSTIN" name="gstin" component={regularField} type="text" placeholder="Enter GST number..." />
        </div>
      </div>
    </fieldset>
    {props.renderAlert()}
    <fieldset>
      <button action="submit" className="btn btn-primary">Submit</button>
    </fieldset>
  </form>
)
