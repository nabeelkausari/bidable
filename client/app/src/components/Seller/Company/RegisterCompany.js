import React, { Component } from 'react';
import {NotificationManager} from 'react-notifications';

import SellerCompanyRegisterContainer from '../../../containers/auth/seller/registerCompany'
import RegisterForm from './RegisterForm';

class SellerCompanyRegister extends Component {

  handleFormSubmit = props => {
    const { fullName, companyName, address, city, state, pinCode, phone, pan, gstin } = props;
    this.props.submitSellerCompany({
      representativeName: fullName,
      name: companyName,
      address, city, state, pinCode, phone, pan, gstin,
      redirect: this.handleRedirect.bind(this)
    });
  }

  handleRedirect = () => {
    NotificationManager.success("Verification process will be completed within 2 working days", "Business Detail Received", 5000)
    this.props.history.push('/seller/dashboard')
  }

  renderAlert = () => {
    if (this.props.submitSellerCompanyError) {
      return (
        <div className="alert alert-danger">
          <strong>Oops!</strong> {this.props.submitSellerCompanyError}
        </div>
      )
    }
  }

  render() {
    return (
      <RegisterForm
        {...this.props}
        handleFormSubmit={this.handleFormSubmit}
        renderAlert={this.renderAlert}
      />
    );
  }
}

export default SellerCompanyRegisterContainer(SellerCompanyRegister);

