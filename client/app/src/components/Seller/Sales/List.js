import React, { Component } from 'react';
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  Table,
} from 'reactstrap';

import SalesContainer from '../../../containers/sales';
import {getAuthHeaders, readableDate} from "../../../constants"

class Sales extends Component {

  componentDidMount() {
    this.props.getSellerSales(getAuthHeaders())
  }

  visitAuction = (auctionId) => {
    this.props.history.push(`/seller/auctions/${auctionId}`)
  }

  render() {
    const { sellerSales } = this.props;
    if (!sellerSales) return <div/>
    return (
      <Row>
        <Col>
          <Card>
            <CardHeader>
              Sales Report
            </CardHeader>
            <CardBody>
              <Table responsive>
                <thead>
                <tr>
                  <th>Auction ID</th>
                  <th>Lot No</th>
                  <th>Business</th>
                  <th>Product</th>
                  <th>Auction Date</th>
                  <th>Balance Amount</th>
                  <th>Balance Paid</th>
                  <th>Product Picked Up</th>
                  <th>Update</th>
                </tr>
                </thead>
                <tbody>
                {!sellerSales.length && <tr>
                  <td className="team-result__date">You have no sales yet</td>
                  <td/>
                  <td/>
                  <td/>
                  <td/>
                  <td/>
                  <td/>
                  <td/>
                  <td/>
                </tr>}
                {sellerSales.length > 0 && sellerSales.map(sale => (
                  <tr key={"row-data-" + sale._id}>
                    <td>
                      <button
                        onClick={() => this.visitAuction(sale.auction._id)}
                        className="btn btn-link"
                      >{sale.auction.auctionId}</button>
                    </td>
                    <td><button
                      onClick={() => this.visitAuction(sale.auction._id)}
                      className="btn btn-link"
                    >{sale.lot.lotNo}</button></td>
                    <td>-</td>
                    <td>{sale.lot.product.name}</td>
                    <td>{readableDate(sale.lot.auctionDate)}</td>
                    <td>-</td>
                    <td>{sale.balancePaid ? "Paid" : "Not Paid"}</td>
                    <td>{sale.pickup.done ? "Yes" : "No"}</td>
                    <td>
                      <button
                        onClick={() => this.visitAuction(sale.auction._id)}
                        className="btn btn-success"
                        disabled
                      >Update</button>
                    </td>
                  </tr>
                ))}
                </tbody>
              </Table>
            </CardBody>
          </Card>
        </Col>
      </Row>
    );
  }
}

export default SalesContainer(Sales);
