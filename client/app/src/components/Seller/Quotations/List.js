import React, { Component } from 'react';
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  Table,
} from 'reactstrap';

import QuotationsContainer from '../../../containers/quotations';
import {getAuthHeaders, price, readableDate} from "../../../constants"

class Quotations extends Component {

  componentDidMount() {
    this.props.getSellerQuotations(getAuthHeaders())
  }

  visitAuction = (auctionId) => {
    this.props.history.push(`/seller/auctions/${auctionId}`)
  }

  render() {
    const { quotations } = this.props;
    if (!quotations) return <div/>
    return (
      <Row>
        <Col>
          <Card>
            <CardHeader>
              Quotations
            </CardHeader>
            <CardBody>
              <Table responsive>
                <thead>
                <tr>
                  <th>Auction ID</th>
                  <th>Lot No</th>
                  <th>Business</th>
                  <th>Product</th>
                  <th>Auction Date</th>
                  <th>Highest Quotation</th>
                  <th>No of Quotations</th>
                  <th>Update</th>
                </tr>
                </thead>
                <tbody>
                {!quotations.length && <tr>
                  <td className="team-result__date">You have no sales yet</td>
                  <td/>
                  <td/>
                  <td/>
                  <td/>
                  <td/>
                  <td/>
                  <td/>
                </tr>}
                {quotations.length > 0 && quotations.map(lot => (
                  <tr key={"row-data-" + lot._id}>
                    <td>
                      <button
                        onClick={() => this.visitAuction(lot.auction._id)}
                        className="btn btn-link"
                      >{lot.auction.auctionId}</button>
                    </td>
                    <td><button
                      onClick={() => this.visitAuction(lot.auction._id)}
                      className="btn btn-link"
                    >{lot.lotNo}</button></td>
                    <td>-</td>
                    <td>{lot.product.name}</td>
                    <td>{readableDate(lot.auctionDate)}</td>
                    <td>{lot.quotations[0] ? price(lot.quotations[0].price) : "-"}</td>
                    <td>{lot.quotations.length}</td>
                    <td>
                      <button
                        onClick={() => this.visitAuction(lot.auction._id)}
                        className="btn btn-success"
                        disabled
                      >Update</button>
                    </td>
                  </tr>
                ))}
                </tbody>
              </Table>
            </CardBody>
          </Card>
        </Col>
      </Row>
    );
  }
}

export default QuotationsContainer(Quotations);
