import React from 'react';
import {Col, Row, Button} from 'reactstrap';
import { Field } from 'redux-form';

import { renderDateTimePicker, RenderTimePicker} from "../../Common/index"

export default props => (
  <form onSubmit={props.handleSubmit(props.handleFormSubmit)}>
    <fieldset className="form-group">
      <h5>Inspection Details</h5>
      <fieldset>
        <Field
          label="Enter the dates when the place is open for product inspection"
          name="inspectionDates"
          component={renderDateTimePicker}
          range
          placeholder="Select dates..." />
      </fieldset>
      <fieldset>
        <RenderTimePicker
          label="Enter the timings when visitors are allowed for product inspection"
          startValue={props.inspectionTimeFromValue}
          firstFieldName="inspectionTimeFrom"
          secondFieldName="inspectionTimeTo"
          firstLabel="Open"
          secondLabel="Close"
        />
      </fieldset>
    </fieldset>
    {/*<fieldset className="form-group">*/}
    {/*<h5>Preferred Auction Date</h5>*/}
    {/*<div className="row">*/}
    {/*<div className="col-md-6">*/}
    {/*<Field*/}
    {/*label="Auction date will be allocated based on availability, this is just to know your preference"*/}
    {/*id="auctionDate"*/}
    {/*name="auctionDate"*/}
    {/*component={renderDateTimePicker}*/}
    {/*placeholder="Select preferred auction date..." />*/}
    {/*</div>*/}
    {/*</div>*/}
    {/*</fieldset>*/}
    {props.renderAlert()}
    <Row>
      <Col className="text-center">
        <Button action="submit" color="primary" className="px-4">{props.actionText}</Button>
      </Col>
      <Col className="text-center">
        <Button onClick={props.cancelAction} color="secondary" className="px-4">Cancel</Button>
      </Col>
    </Row>
  </form>
)

