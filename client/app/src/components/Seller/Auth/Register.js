import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Card, CardBody, CardFooter, Col, Container, Row } from 'reactstrap';

import RegisterContainer from '../../../containers/auth/seller/registerSeller'
import NotifyModal from '../../Common/NotifyModal';
import AuthLayout from "../../AuthLayout"
import RegisterForm from "../../../components/Common/RegisterForm";
import {sellerExplainer} from "../../../constants"

class Register extends Component {
  state = {modal: false}

  handleFormSubmit = ({ email, mobile, password }) => {
    this.props.registerSeller({
      email,
      password,
      mobile,
      redirect: this.confirmRedirection.bind(this)
    });
  }

  confirmRedirection = () => {
    this.setState({ modal: true })
  }

  renderAlert = () => {
    if (this.props.registerSellerError) {
      return (
        <div className="alert alert-danger">
          <strong>Oops!</strong> {this.props.registerSellerError}
        </div>
      )
    }
  }

  componentDidMount() {
    this.props.clearAuthErrors()
  }

  render() {
    const { modal } = this.state;
    return (
      <AuthLayout>
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <Card>
                <CardBody className="p-4">
                  <h1 className="text-center">Register as Seller</h1>
                  <p className="lead text-center mb-4">{sellerExplainer} Or else register as <Link tag="button" to="/buyer/register">Buyer</Link></p>
                  <RegisterForm
                    handleFormSubmit={this.handleFormSubmit.bind(this)}
                    renderAlert={this.renderAlert.bind(this)}
                    btnClassName="btn-yellow"
                  />
                </CardBody>
                <CardFooter className="p-4">
                  <Row>
                    <Col xs="6" className="text-left">
                      <Link className="btn btn-outline-primary" tag="button" to="/seller/login">Login</Link>
                    </Col>
                    <Col xs="6" className="text-right">
                      <Link className="btn btn-link px-0" tag="button" to="/seller/forgot-password">Forgot password?</Link>
                    </Col>
                  </Row>
                </CardFooter>
              </Card>
              <div className="text-center">
                <Link className="btn btn-link m-auto" tag="button" to="/">Back to Home</Link>
              </div>
            </Col>
          </Row>
        </Container>
        <NotifyModal
          onClose={() => {
            this.props.history.push('/seller/login')
          }}
          modal={modal}
          title="Confirm Email"
          body="You’ll receive a confirmation email in your inbox with a link to activate your account"
          showConfirmBtn
        />
      </AuthLayout>
    );
  }
}

export default RegisterContainer(Register);
