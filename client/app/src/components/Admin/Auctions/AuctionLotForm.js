import React, { Component } from 'react';
import { CardBody, Table, Col, Row, Button } from 'reactstrap';
import { Field } from 'redux-form';

import AuctionLotFormContainer from '../../../containers/auctions/reviewForm';
import {price, readableDate, readableTime} from "../../../constants"
import {regularField, renderDateTimePicker, RenderTimePicker} from "../../Common"

class AuctionLotForm extends Component {

  render() {
    const {
      company, inspectionDetails: { from, to, timings: [time]}, lots
    } = this.props.auction;
    if (!lots || lots.length === 0) return <div/>
    return (
      <form onSubmit={this.props.handleSubmit(this.props.handleFormSubmit)}>
        <CardBody>
          <Table responsive>
            <thead>
            <tr>
              <th>Company Name</th>
              <th>Inspection Dates</th>
              <th>Inspection Timings</th>
              <th>Auction Date</th>
              <th>Auction Timings</th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td>{company}</td>
              <td>{readableDate(from)} - {readableDate(to)}</td>
              <td>{readableTime(time.from)} - {readableTime(time.to)}</td>
              <td>
                <fieldset className="form-group">
                  <Field
                    hideLabel
                    name="auctionDate"
                    component={renderDateTimePicker}
                    placeholder="Select date..." />
                </fieldset>
              </td>
              <td>
                <fieldset className="form-group">
                  <RenderTimePicker
                    hideLabel
                    onlyStart
                    firstFieldName="auctionStartsFrom"
                  />
                </fieldset>
              </td>
            </tr>
            </tbody>
          </Table>
          <h3>Lots</h3>
          <Table responsive>
            <thead>
            <tr>
              <th>Product Name</th>
              <th>Starting Price</th>
              <th>Increment Value</th>
              <th>Qty (Approx)</th>
              <th>Product Location</th>
              <th>Details</th>
              <th>EMD Amount</th>
            </tr>
            </thead>
            <tbody>
            {
              lots.map(lot => (
                <tr key={lot._id}>
                  <td>{lot.product.name}</td>
                  <td>{price(lot.startingPrice)} (Per {lot.bidPer})</td>
                  <td>{price(lot.minimumIncrement)}</td>
                  <td>{lot.quantity} {lot.unit}</td>
                  <td>{lot.product.location}</td>
                  <td><button className="btn btn-secondary">Details</button></td>
                  <td>
                    <fieldset className="form-group">
                      <Field hideLabel name={lot._id} component={regularField} type="number" placeholder="Enter EMD..." />
                    </fieldset>
                  </td>
                </tr>
              ))
            }
            </tbody>
          </Table>
          {this.props.renderAlert()}
          <Row>
            <Col className="text-center">
              <Button action="submit" color="primary" className="px-4">Approve</Button>
            </Col>
            <Col className="text-center">
              <Button onClick={this.props.redraft} color="danger" className="px-4">Redraft</Button>
            </Col>
            <Col className="text-center">
              <Button onClick={() => this.props.history.push('/admin/auctions')} color="warning" className="px-4">Cancel</Button>
            </Col>
          </Row>
        </CardBody>
      </form>
    )
  }
}

export default AuctionLotFormContainer(AuctionLotForm)
