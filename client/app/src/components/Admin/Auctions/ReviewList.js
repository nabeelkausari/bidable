import React, { Component } from 'react';
import { sortBy } from 'lodash'

import AuctionsContainer from '../../../containers/auctions/admin';
import AuctionList from '../../Buyer/Auction/AuctionList';
import {getAuthHeaders} from "../../../constants"

class AuctionsReview extends Component {
  state = {
    auctionsLoading: true
  }

  componentDidMount() {
    this.props.getAuctionsToReview(getAuthHeaders());
  }

  componentDidUpdate(prevProps) {
    let auctionsTriggered = prevProps.auctionsLoading !== this.props.auctionsLoading;
    let auctionsLoaded = this.props.auctionsLoading === false;
    if (auctionsTriggered && auctionsLoaded) {
      this.setState({ auctionsLoading: false })
    }
  }

  filterAuctions = () => {
    return sortBy(this.props.auctions, 'updatedAt')
  }

  render() {
    const { auctionsLoading } = this.state;
    return (
      <AuctionList
        {...this.props}
        auctions={this.filterAuctions()}
        auctionsLoading={auctionsLoading}
        detailPath="/admin/auctions"
        title="Auctions To Review"
      />
    );
  }
}

export default AuctionsContainer(AuctionsReview);
