import React, { Component } from 'react';
import {Button, Card, CardBody, CardHeader, Col, Row, Table, Badge } from 'reactstrap';
import {NotificationManager} from 'react-notifications';

import NotifyModal from '../../Common/NotifyModal';
import { BuyerStatus } from '../../Common/Status';
import AuctionDetailContainer from '../../../containers/auctions/admin';
import AuctionLotForm from './AuctionLotForm';
import {getAuthHeaders, getDateTime} from "../../../constants"

class AuctionDetail extends Component {
  state = {
    submitInProcess: false,
    createLotVisible: false,
    editLotVisible: false,
    modal: false,
    auctionLoading: true
  }

  componentDidMount() {
    this.loadAuction()
  }

  loadAuction = () => {
    const { match, getAuctionToReview} = this.props;
    getAuctionToReview(match.params.auctionId, getAuthHeaders())
  }

  componentDidUpdate(prevProps) {
    let auctionTriggered = prevProps.auctionLoading !== this.props.auctionLoading;
    let auctionLoaded = this.props.auctionLoading === false;
    if (auctionTriggered && auctionLoaded) {
      this.setState({ auctionLoading: false })
    }
  }

  handleFormSubmit = props => {
    const { auctionDate, auctionStartsFrom, ...emds } = props;
    if (Object.keys(emds).length !== this.props.auction.lots.length) {
      this.props.raiseApproveError("Enter all EMDs")
    } else {
      this.props.approveAuction({
          _id: this.props.auction._id,
          auctionDate: auctionDate[0],
          auctionStartsFrom: getDateTime(auctionDate[0], auctionStartsFrom),
          emds
      }, () => {
          this.props.history.push('/admin/auctions');
          setTimeout(NotificationManager.success("", "Approved Successfully"))
        },
        getAuthHeaders()
      )
    }
  }

  renderAlert = () => {
    if (this.props.approveAuctionError) {
      return (
        <div className="alert alert-danger">
          <strong>Oops!</strong> {this.props.approveAuctionError}
        </div>
      )
    }
  }

  redraft = () => {
    this.props.redraftAuction(
      this.props.auction._id,
      () => {
        this.props.history.push('/admin/auctions');
        setTimeout(NotificationManager.success("", "Redrafted Successfully"))
      },
      getAuthHeaders()
    )
  }

  render() {
    const { auctionLoading } = this.state;
    if (!this.props.auction) return (
      <div className="animated fadeIn">
        <Row>
          <div className="col-md-6">
            <h2>{auctionLoading ? "Loading..." : "Auction Not Found"}</h2>
          </div>
        </Row>
      </div>
    )
    const { auctionId, liveStatus } = this.props.auction;
    return (
      <div className="animated fadeIn">
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                Auction ID : <strong>{auctionId}</strong>
                <span className="float-right mb-0">{BuyerStatus({ liveStatus })}</span>
              </CardHeader>
              <AuctionLotForm
                {...this.props}
                handleFormSubmit={this.handleFormSubmit.bind(this)}
                renderAlert={this.renderAlert.bind(this)}
                redraft={this.redraft}
              />
            </Card>
          </Col>

          <NotifyModal
            onClose={this.processAuctionSubmit}
            modal={this.state.modal}
            title="Submit Auction"
            body="Your auction is being submitted to our verification department to review. Once it is verified and approved, your auction will be listed on the platform and it will go live"
            showConfirmBtn
            showCancelBtn
            onCancel={() => this.setState({modal: false, submitInProcess: false})}
            confirmBtnText="Submit"
            confirmBtnClass="primary"
          />
        </Row>

      </div>
    );
  }
}


export default AuctionDetailContainer(AuctionDetail);
