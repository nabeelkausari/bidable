import React, { Component } from 'react';
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  Table,
} from 'reactstrap';

import EMDContainer from '../../../containers/emd/admin';
import {getAuthHeaders, readableDate, price} from "../../../constants"

class EMDList extends Component {

  visitAuction = (auctionId) => {
    this.props.history.push(`/admin/auctions-approved/${auctionId}`)
  }

  componentDidMount() {
    this.props.getEMDs('rejected', getAuthHeaders())
  }
  render() {
    const { EMDs } = this.props;
    if (!EMDs) return <div/>
    return (
      <Row>
        <Col>
          <Card>
            <CardHeader>
              Rejected EMD List
            </CardHeader>
            <CardBody>
              <Table responsive>
                <thead>
                <tr>
                  <th>Auction ID</th>
                  <th>Lot No</th>
                  <th>Seller Name</th>
                  <th>Product</th>
                  <th>Auction Date</th>
                  <th>Ref No</th>
                  <th>EMD Amount</th>
                  <th>Approval Status</th>
                  <th>Submit Notes</th>
                  <th>Reject Notes</th>
                </tr>
                </thead>
                <tbody>
                {!EMDs.length && <tr>
                  <td className="team-result__date">No rejected EMDs</td>
                  <td/>
                  <td/>
                  <td/>
                  <td/>
                  <td/>
                  <td/>
                </tr>}
                {EMDs.length > 0 && EMDs.map(emd => (
                  <tr key={"row-data-" + emd._id}>
                    <td><button
                      onClick={() => this.visitAuction(emd.lot.auction._id)}
                      className="btn btn-link"
                    >{emd.lot.auction.auctionId}</button></td>
                    <td><button
                      onClick={() => this.visitAuction(emd.lot.auction._id)}
                      className="btn btn-link"
                    >{emd.lot.lotNo}</button></td>
                    <td>{emd.lot.sellerName}</td>
                    <td>{emd.lot.product.name}</td>
                    <td>{readableDate(emd.lot.auctionDate)}</td>
                    <td>{emd.refNo}</td>
                    <td>{price(emd.amount)}</td>
                    <td>{emd.approval}</td>
                    <td>{emd.submitNote || "-"}</td>
                    <td>{emd.rejectNote || "-"}</td>
                  </tr>
                ))}
                </tbody>
              </Table>
            </CardBody>
          </Card>
        </Col>
      </Row>
    );
  }
}

export default EMDContainer(EMDList);
