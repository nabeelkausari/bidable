import React, { Component } from 'react';
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  Table,
} from 'reactstrap';
import {NotificationManager} from 'react-notifications';

import FormModal from '../../Common/FormModal';
import EMDContainer from '../../../containers/emd/admin';
import {getAuthHeaders, readableDate, price} from "../../../constants"
import RejectionForm from './RejectionForm';

class ReviewEMDList extends Component {
  state = {modal: false}

  showModal = () => this.setState({ modal: true })
  hideModal = () => this.setState({ modal: false })

  handleEMDRejection = ({ rejectNote }) => {
    this.hideModal()
    this.props.rejectEMD({
      rejectNote, _id: this.props.emdToReject,
      redirect: () => {
        this.props.history.push(`/admin/emds-rejected`)
        setTimeout(() => NotificationManager.success("EMD Rejected"))
      }
    }, getAuthHeaders())
  }

  startEMDRejection = (id) => {
    this.showModal()
    this.props.selectEMDToReject(id)
  }

  handleApprove = id => {
    this.props.approveEMD(id, () => {
      this.props.history.push(`/admin/emds-approved`)
      setTimeout(() => NotificationManager.success("EMD Approved"))
    }, getAuthHeaders())
  }

  renderAlert = () => {
    if (this.props.rejectEMDError) {
      return (
        <div className="alert alert-danger">
          <strong>Oops!</strong> {this.props.rejectEMDError}
        </div>
      )
    }
  }

  visitAuction = (auctionId) => {
    this.props.history.push(`/admin/auctions-approved/${auctionId}`)
  }

  componentDidMount() {
    this.props.getEMDs('pending', getAuthHeaders())
  }
  render() {
    const { EMDs } = this.props;
    if (!EMDs) return <div/>
    return (
      <Row>
        <Col>
          <Card>
            <CardHeader>
              EMD List to Review
            </CardHeader>
            <CardBody>
              <Table responsive>
                <thead>
                <tr>
                  <th>Auction ID</th>
                  <th>Lot No</th>
                  <th>Seller Name</th>
                  <th>Product</th>
                  <th>Auction Date</th>
                  <th>Ref No</th>
                  <th>EMD Amount</th>
                  <th>Approval Status</th>
                  <th>Submit Notes</th>
                  <th>Approve</th>
                  <th>Reject</th>
                </tr>
                </thead>
                <tbody>
                {!EMDs.length && <tr>
                  <td className="team-result__date">No EMDs to review</td>
                  <td/>
                  <td/>
                  <td/>
                  <td/>
                  <td/>
                  <td/>
                </tr>}
                {EMDs.length > 0 && EMDs.map(emd => (
                  <tr key={"row-data-" + emd._id}>
                    <td><button
                      onClick={() => this.visitAuction(emd.lot.auction._id)}
                      className="btn btn-link"
                    >{emd.lot.auction.auctionId}</button></td>
                    <td><button
                      onClick={() => this.visitAuction(emd.lot.auction._id)}
                      className="btn btn-link"
                    >{emd.lot.lotNo}</button></td>
                    <td>{emd.lot.sellerName}</td>
                    <td>{emd.lot.product.name}</td>
                    <td>{readableDate(emd.lot.auctionDate)}</td>
                    <td>{emd.refNo}</td>
                    <td>{price(emd.amount)}</td>
                    <td>{emd.approval}</td>
                    <td>{emd.submitNote || "-"}</td>
                    <td>
                      {emd.approval === "pending" ? <button
                        onClick={() => this.handleApprove(emd._id)}
                        className="btn btn-primary"
                      >Approve</button> : "-"}
                    </td>
                    <td>
                      {emd.approval === "pending" ? <button
                        onClick={() => this.startEMDRejection(emd._id)}
                        className="btn btn-danger"
                      >Reject</button>: "-"}
                    </td>
                  </tr>
                ))}
                </tbody>
              </Table>
            </CardBody>
          </Card>
        </Col>
        <FormModal
          onModalFormSubmit={this.handleEMDRejection}
          onCancelForm={this.hideModal}
          renderAlert={this.renderAlert}
          modal={this.state.modal}
          title="Rejection Reason"
          FormBody={RejectionForm}
        />
      </Row>
    );
  }
}

export default EMDContainer(ReviewEMDList);
