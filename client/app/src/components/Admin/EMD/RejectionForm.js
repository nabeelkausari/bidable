import React from 'react';
import { Field, reduxForm } from 'redux-form';
import {regularField} from "../../Common/index.js"

const validate = formProps => {
  const errors = {};
  if (!formProps.rejectNote) errors.rejectNote = 'Rejection reason is required'
  return errors;
}

const EMDRejectionForm = props => (
  <form onSubmit={props.handleSubmit(props.handleFormSubmit)}>
    <fieldset className="form-group">
      <Field hideLabel name="rejectNote" component={regularField} type="textarea" placeholder="Enter the reason why EMD is being rejected..." />
    </fieldset>
    {props.renderAlert()}
    <fieldset>
      <button type="submit" className="btn btn-primary">Submit</button>
      <button type="button" onClick={props.onCancelForm} className="btn btn-warning float-right">Cancel</button>
    </fieldset>
  </form>
)


export default reduxForm({
  form: 'emdRejectionForm',
  validate
})(EMDRejectionForm)
