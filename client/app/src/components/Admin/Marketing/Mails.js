import React, { Component } from 'react';
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  Table,
  Button
} from 'reactstrap';
// import striptags from 'striptags';
import { truncate } from 'lodash';

import {getAuthHeaders, regularDateTime} from "../../../constants"
import MailsContainer from "../../../containers/marketing";

class MailList extends Component {

  componentDidMount() {
    this.props.getMails(getAuthHeaders());
  }

  render() {
    const { mails } = this.props;
    if (!mails) return <div/>
    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            <Card>
              <CardHeader>
                Sent Mails
              </CardHeader>
              <CardBody>
                <Table responsive>
                  <thead>
                  <tr>
                    <th>From</th>
                    <th>To</th>
                    <th>Subject</th>
                    <th>Body</th>
                    <th>Date</th>
                  </tr>
                  </thead>
                  <tbody>
                  {!mails.length && <tr>
                    <td>No mails have been sent yet</td>
                    <td/>
                    <td/>
                    <td/>
                    <td/>
                  </tr>}
                  {mails.length > 0 && mails.map(mail => (
                    <tr key={"row-data-" + mail._id}>
                      <td>{mail.from}</td>
                      <td>{mail.to}</td>
                      <td>{mail.subject}</td>
                      <td>{truncate(mail.body, { length: 80 })}</td>
                      <td>{regularDateTime(mail.createdAt)}</td>
                    </tr>
                  ))}
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default MailsContainer(MailList)
