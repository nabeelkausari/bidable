import React, { Component } from 'react';
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
} from 'reactstrap';
import {NotificationManager} from 'react-notifications';
// import striptags from 'striptags';

import MarketingContainer from '../../../containers/marketing'
import MailForm from './MailForm';
import {getAuthHeaders} from "../../../constants"

class SendMail extends Component {
  state = {
    sendingMail: false
  }

  handleFormSubmit = ({ body, cc, ...props}) => {
    this.props.sendMail({
      ...props,
      body: body.map(para => `<p>${para}</p>`).join(" "),
      cc: cc ? cc.toString() : '',
      redirect: this.handleRedirect.bind(this)
    }, getAuthHeaders())
  }

  handleRedirect = () => {
    this.props.history.push('/admin/mails');
    setTimeout(() => NotificationManager.success("", "Mail sent successfully", 5000))
  }

  componentDidMount() {
    this.props.clearSendMailErrors()
  }

  componentDidUpdate(prevProps) {
    const sendMailTriggered = this.props.sendMailLoading !== prevProps.sendMailLoading;
    if(sendMailTriggered && this.props.sendMailLoading === true) {
      this.setState({ sendingMail: true })
    }
    if(sendMailTriggered && this.props.sendMailLoading === false) {
      this.setState({ sendingMail: false })
    }
  }

  renderAlert = () => {
    if (this.props.sendMailError) {
      return (
        <div className="alert alert-danger">
          <strong>Oops!</strong> {this.props.submitEMDError}
        </div>
      )
    }
  }

  render() {
    if (!this.props.user) return <div/>
    return (
      <div className="animated fadeIn">
        <Row>
          <Col md={6}>
            <Card>
              <CardHeader>
                Send Mail
              </CardHeader>
              <CardBody>
                <MailForm
                  {...this.props}
                  handleFormSubmit={this.handleFormSubmit}
                  renderAlert={this.renderAlert}
                  submitLoading={this.state.sendingMail}
                />
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default MarketingContainer(SendMail);

