import React from 'react';
import { Field, FieldArray } from 'redux-form';
import { FormGroup, Label, FormFeedback } from 'reactstrap';
import {regularField} from "../../Common/index.js"
import MailFormContainer from '../../../containers/marketing/form';

const MailForm = props => (
  <form onSubmit={props.handleSubmit(props.handleFormSubmit)}>
    <fieldset className="form-group">
      <div className="row">
        <div className="col-md-12">
           <h5>Sender Name: <strong>{props.user.mailDetails.name}</strong></h5>
        </div>
        <div className="col-md-12">
          <h5>Email: <strong>{props.user.mailDetails.email}</strong></h5>
        </div>
        <div className="col-md-12">
          <h5>Designation: <strong>{props.user.mailDetails.designation}</strong></h5>
        </div>
      </div>
    </fieldset>
    <fieldset className="form-group">
      <div className="row">
        <div className="col-md-6">
          <Field label="Recipient Email" name="recipient" component={regularField} type="text" placeholder="Enter the recipient email address..." />
          <FieldArray label="CC" name="cc" component={renderCC} />
        </div>
        <div className="col-md-6">
          <Field label="Subject" name="subject" component={regularField} type="text" placeholder="Enter the mail subject..." />
        </div>
      </div>
    </fieldset>
    <fieldset className="form-group">
      <FieldArray label="Body" name="body" component={renderParagraphs} />
    </fieldset>
    {props.renderAlert()}
    <fieldset>
      <button disabled={props.submitLoading} type="submit" className="btn btn-primary">Send</button>
      <button type="button" onClick={() => props.history.push('/admin/mails')} className="btn btn-warning float-right">Back</button>
    </fieldset>
  </form>
)

export default MailFormContainer(MailForm)


const renderParagraphs = ({ fields, id, label, meta: { touched, error } }) => {
  if (fields.length === 0) fields.push()
  return (
    <FormGroup>
      <Label htmlFor={id}>{label}</Label>
      <button className="btn btn-secondary float-right" type="button" onClick={() => fields.push()}>
        Add Paragraph
      </button>
      <ul className="paragraphs">
        {fields.map((paragraph, index) => (
          <li key={index}>
            <Field
              name={paragraph}
              type="textarea"
              component={regularField}
              placeholder="Start writing..."
              label={`Paragraph #${index + 1}`}
            />
            {index !== 0 && <button
              className="btn btn-danger float-right mb-2"
              type="button"
              title="Remove Paragraph"
              onClick={() => fields.remove(index)}
            >Remove #{index + 1}</button>}
          </li>
        ))}
      </ul>
      <FormFeedback style={{ display: touched && error ? 'block' : 'none' }} className="help-block">{error}</FormFeedback>
    </FormGroup>
  )
}

const renderCC = ({ fields, id, label, meta: { touched, error } }) => {
  return (
    <FormGroup>
      <div className="mb-3">
        <button className="btn btn-secondary" type="button" onClick={() => fields.push()}>
          Add CC
        </button>
      </div>
      <ul className="paragraphs">
        {fields.map((email, index) => (
          <li key={index}>
            <Field
              name={email}
              type="text"
              component={regularField}
              placeholder="Enter email..."
              label={`Email #${index + 1}`}
            />
            <button
              className="btn btn-danger float-right mb-2"
              type="button"
              title="Remove Email"
              onClick={() => fields.remove(index)}
            >Remove #{index + 1}</button>
          </li>
        ))}
      </ul>
      <FormFeedback style={{ display: touched && error ? 'block' : 'none' }} className="help-block">{error}</FormFeedback>
    </FormGroup>
  )
}
