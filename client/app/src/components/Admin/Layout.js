import React from 'react';
import Layout from '../Common/Layout';

import AdminLayoutContainer from '../../containers/auth/admin'
import sideBarNav from '../../adminNav';
import adminRoutes from '../../adminRoutes';

const AdminLayout = props => (
  <Layout
    name="Bidable Admin"
    navConfig={sideBarNav}
    routes={adminRoutes}
    type="admin"
    handleLogout={() => {props.logoutAdmin({ redirect: () => props.history.push('/admin/login')})}}
    {...props}
  />
)

export default AdminLayoutContainer(AdminLayout)
