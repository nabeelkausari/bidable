import React, { Component } from 'react';
import { Field } from 'redux-form';
import {NotificationManager} from 'react-notifications';

import { renderField } from './index';
import Container from '../../containers/auth/common/forgot'
import NotifyModal from './NotifyModal';

class ForgotForm extends Component {
  state = {
    modal: false
  }
  handleFormSubmit = ({ email }) => {
    this.props.requestResetPassword({
      email,
      redirect: this.confirmRedirection.bind(this)
    })
  }

  confirmRedirection = () => {
    this.setState({ modal: true })
  }

  getUserType = () => {
    const { location: { pathname } } = this.props.history
    return pathname.split("/")[1]
  }

  redirect = () => {
    const type = this.getUserType()
    this.props.history.push(`/${type}/login`);
    setTimeout(() => NotificationManager.success("", "Password Reset Email Sent", 5000), 1000)
  }

  renderAlert = () => {
    if (this.props.requestResetError) {
      return (
        <div className="alert alert-danger" style={{ marginTop: 10 }}>
          <strong>Oops!</strong> {this.props.requestResetError}
        </div>
      )
    }
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.requestResetLoading !== this.props.requestResetLoading) {
      if (nextProps.requestResetLoading) {
        this.setState({ modalTitle: 'Requesting', modalMessage: 'Hold on, your password reset is being requested', modalLoading: true })
      } else if (nextProps.requestResetSuccess) {
        this.setState({ modalTitle: 'One more step!', modalMessage: 'You’ll receive a confirmation email in your inbox with a link to reset your password.', modalLoading: false })
      } else {
        this.setState({ modalTitle: 'Oops!', modalMessage: 'Something went wrong, try again or contact our customer support', modalLoading: false })
      }
    }
  }

  componentDidMount() {
    this.props.clearAuthErrors()
  }

  render() {
    const { modal, modalTitle, modalMessage, modalLoading } = this.state;
    const btnClass = this.getUserType() === "seller" ? "btn-yellow" : "btn-blue";
    return (
      <form onSubmit={this.props.handleSubmit(this.handleFormSubmit)}>
        <fieldset className="form-group">
          <Field icon="icon-envelope" label="Email" name="email" component={renderField} type="email" placeholder="Enter your email address..." />
        </fieldset>
        <fieldset className="form-group text-center">
          <button action="submit" className={`btn btn-primary btn-lg ${btnClass}`}>
            Request Password Reset
          </button>
        </fieldset>
        {this.renderAlert()}
        <div className="form-text text-muted">You’ll receive a confirmation email in your inbox with a link to reset your password.</div>
        <NotifyModal
          onClose={this.redirect.bind(this)}
          modal={modal}
          title={modalTitle}
          body={modalMessage}
          showConfirmBtn
          loading={modalLoading}
        />
      </form>
    )
  }
}

export default Container(ForgotForm);
