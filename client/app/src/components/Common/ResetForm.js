import React, { Component } from 'react';
import { Field } from 'redux-form';
import {NotificationManager} from 'react-notifications';

import {renderField} from './index';
import Container from '../../containers/auth/common/reset'

class ForgotForm extends Component {
  handleFormSubmit = ({ password }) => {
    let token = this.props.match.params.token;
    this.props.resetPassword({
      password, token,
      redirect: this.redirect.bind(this)
    })
  }

  getUserType = () => {
    const { location: { pathname } } = this.props.history
    return pathname.split("/")[1]
  }

  redirect = () => {
    const type = this.getUserType()
    this.props.history.push(`/${type}/login`);
    NotificationManager.success("Your password has been changed, login with your new password", "Password Changed!", 5000)
  }

  renderAlert = () => {
    if (this.props.resetError) {
      return (
        <div className="alert alert-danger" style={{ marginTop: 10 }}>
          <strong>Oops!</strong> {this.props.resetError}
        </div>
      )
    }
  }

  componentDidMount() {
    this.props.clearAuthErrors()
  }

  render() {
    const btnClass = this.getUserType() === "seller" ? "btn-yellow" : "btn-blue";
    return (
      <form onSubmit={this.props.handleSubmit(this.handleFormSubmit)}>
        <fieldset className="form-group">
          <Field icon="icon-lock" label="Password" name="password" component={renderField} type="password" placeholder="Enter your password..." />
        </fieldset>
        <fieldset className="form-group">
          <Field icon="icon-lock" label="Confirm Password" name="confirmPassword" component={renderField} type="password" placeholder="Repeat your password..." />
        </fieldset>
        <fieldset className="form-group text-center">
          <button action="submit" className={`btn btn-primary m-auto btn-lg ${btnClass}`}>
            Update Password
          </button>
        </fieldset>
        {this.renderAlert()}
      </form>
    )
  }
}

export default Container(ForgotForm);
