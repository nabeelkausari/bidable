import React, { Component } from 'react';
import { Badge, DropdownItem, DropdownMenu, DropdownToggle, Nav, NavItem, NavLink } from 'reactstrap';
import PropTypes from 'prop-types';

import { AppAsideToggler, AppHeaderDropdown, AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
import logo from '../../../assets/img/brand/logo.png'
import icon from '../../../assets/img/brand/icon.png'

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component {

  render() {
    const { name } = this.props;
    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand
          href="/app"
          full={{ src: logo, height: 35, alt: name }}
          minimized={{ src: icon, height: 40, alt: name }}
        />
        {/*<AppSidebarToggler className="d-md-down-none" display="lg" />*/}

        {/*<Nav className="d-md-down-none" navbar>*/}
        {/*<NavItem className="px-3">*/}
        {/*<NavLink href="/">Dashboard</NavLink>*/}
        {/*</NavItem>*/}
        {/*<NavItem className="px-3">*/}
        {/*<NavLink href="#">Users</NavLink>*/}
        {/*</NavItem>*/}
        {/*<NavItem className="px-3">*/}
        {/*<NavLink href="#">Settings</NavLink>*/}
        {/*</NavItem>*/}
        {/*</Nav>*/}
        <Nav className="ml-auto" navbar>
          {/*<NavItem className="d-md-down-none">*/}
          {/*<NavLink href="#"><i className="icon-bell"></i><Badge pill color="danger">5</Badge></NavLink>*/}
          {/*</NavItem>*/}
          {/*<NavItem className="d-md-down-none">*/}
          {/*<NavLink href="#"><i className="icon-list"></i></NavLink>*/}
          {/*</NavItem>*/}
          {/*<NavItem className="d-md-down-none">*/}
          {/*<NavLink href="#"><i className="icon-location-pin"></i></NavLink>*/}
          {/*</NavItem>*/}
          <AppHeaderDropdown direction="down">
            <DropdownToggle nav>
              <i className="icon-user"/>
            </DropdownToggle>
            <DropdownMenu right style={{ right: 'auto' }}>
              <DropdownItem header tag="div" className="text-center">User ID: <strong>{this.props.user && this.props.user.userId}</strong></DropdownItem>

              {/*<DropdownItem><i className="fa fa-user"></i> Profile</DropdownItem>*/}
              {/*<DropdownItem><i className="fa fa-wrench"></i> Settings</DropdownItem>*/}
              {/*<DropdownItem divider />*/}
              <DropdownItem onClick={this.props.handleLogout}><i className="fa fa-lock"/> Logout</DropdownItem>
            </DropdownMenu>
          </AppHeaderDropdown>
        </Nav>
        {/*<AppAsideToggler className="d-md-down-none" />*/}
        {/*<AppAsideToggler className="d-lg-none" mobile />*/}
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default DefaultHeader;
