import React from 'react';
import { Badge } from 'reactstrap';

export default ({ status }) => {
  switch (status) {
    case "draft":
      return <Badge size="lg">Draft</Badge>
    case "review":
      return <Badge size="lg" color="warning">In Review</Badge>
    case "live":
      return <Badge size="lg" color="success">Live</Badge>
    default:
      return <div/>
  }
}

export const BuyerStatus = ({ liveStatus }) => {
  switch (liveStatus) {
    case "upcoming":
      return <Badge size="lg" color="warning">Upcoming</Badge>
    case "past":
      return <Badge size="lg">Completed</Badge>
    case "live":
      return <Badge size="lg" color="success">Live</Badge>
    default:
      return <div/>
  }
}
