import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { Button, Col, Row, InputGroupText } from 'reactstrap';
import { validate, asyncValidate } from "../../containers/auth/validations"

import {renderField} from "./index.js"

const RegisterForm = props => {
  const { handleSubmit, handleFormSubmit, renderAlert, btnClassName } = props;
  return (
    <form onSubmit={handleSubmit(handleFormSubmit)}>
      <fieldset className="form-group">
        <Field icon="icon-envelope" label="Email" name="email" component={renderField} type="email" placeholder="Enter your email address..." />
      </fieldset>
      <fieldset className="form-group">
        <Field
          prepend={() => <InputGroupText>+91</InputGroupText>}
          icon="icon-phone" label="Mobile No." name="mobile" component={renderField} type="number" placeholder="Enter mobile no..." />
      </fieldset>
      <fieldset className="form-group">
        <Field icon="icon-lock" label="Password" name="password" component={renderField} type="password" placeholder="Enter your password..." />
      </fieldset>
      <fieldset className="form-group">
        <Field icon="icon-lock" label="Confirm Password" name="confirmPassword" component={renderField} type="password" placeholder="Repeat your password..." />
      </fieldset>
      {renderAlert()}
      <Row>
        <Col className="text-center">
          <Button action="submit" color="primary" className={`px-4 btn-lg ${btnClassName}`}>Register</Button>
        </Col>
      </Row>
    </form>
  )
}

export default reduxForm({
  form: 'registerForm',
  validate,
  asyncValidate,
  asyncBlurFields: ['email', 'mobile']
})(RegisterForm);
