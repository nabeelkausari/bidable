import React, { Component } from 'react';
import { Card, CardBody, Col, Container, Row, CardFooter } from 'reactstrap';
import { Link } from 'react-router-dom';

import AuthLayout from "../AuthLayout"
import ResetForm from "./ResetForm";

class ResetPassword extends Component {

  render() {
    if (!this.props.match) return <div/>
    const { path } = this.props.match
    const type = path.split("/")[1]
    return (
      <AuthLayout>
        <Container>
          <Row className="justify-content-center">
            <Col md="6">
              <Card>
                <CardBody className="p-4">
                  <h1 className="text-center">Reset Password</h1>
                  <p className="text-muted text-center">Enter the email address of your account</p>
                  <ResetForm {...this.props}/>
                </CardBody>
                <CardFooter className="p-4">
                  <Row>
                    <Col xs="6" className="text-left">
                      <Link className="btn btn-outline-primary" tag="button" to={`/${type}/login`}>Login</Link>
                    </Col>
                    <Col xs="6" className="text-right">
                      <Link className="btn btn-outline-primary" tag="button" to={`/${type}/register`}>Register</Link>
                    </Col>
                  </Row>
                </CardFooter>
              </Card>
            </Col>
          </Row>
        </Container>
      </AuthLayout>
    );
  }
}

export default ResetPassword;
