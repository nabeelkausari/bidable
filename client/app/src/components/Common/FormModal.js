import React from 'react';
import { Button, Modal, ModalHeader, ModalBody } from 'reactstrap';

import Spinner from "./Spinner"

class NotifyModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: props.modal
    };
  }

  onFormSubmit = props => {
    this.setState({ modal: false });
    if (this.props.onModalFormSubmit) this.props.onModalFormSubmit(props)
  }

  toggle = () => {
    if (this.props.toggle) {
      this.setState({ modal: !this.state.modal })
      this.props.toggle()
    }
  }

  cancel = () => {
    this.setState({ modal: false })
    if (this.props.onCancelForm) this.props.onCancelForm()
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.modal !== this.props.modal){
      this.setState({modal:nextProps.modal});
    }
  }

  render() {
    const { title, FormBody, loading } = this.props;
    return (
      <div style={{ margin: 5, display: "inline-block" }}>
        <Modal isOpen={this.state.modal} toggle={this.toggle} className="modal--login">
          <ModalHeader />
          <ModalBody>
            <div className="modal-account-holder">
              <div className="modal-account__item" style={{ flexBasis: "100%", textAlign: 'center'}}>
                <h3>{title}{loading && <Spinner theme='dark'/>}</h3>
                <div className="mb-4">
                  <FormBody
                    {...this.props}
                    handleFormSubmit={this.onFormSubmit}
                    onCancelForm={this.cancel}
                  />
                </div>
              </div>
            </div>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

export default NotifyModal;
