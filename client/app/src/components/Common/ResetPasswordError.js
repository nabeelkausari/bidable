import React, { Component } from 'react';
import { Card, CardBody, CardFooter, Col, Container, Row } from 'reactstrap';
import { Link } from 'react-router-dom';
import AuthLayout from "../AuthLayout"

class ResetPassword extends Component {

  render() {
    if (!this.props.match) return <div/>
    const { path } = this.props.match
    const type = path.split("/")[1]
    return (
      <AuthLayout>
        <Container>
          <Row className="justify-content-center">
            <Col md="6">
              <Card>
                <CardBody className="p-4">
                  <h2 className="text-center">Reset Password Error!!</h2>
                  <p className="text-lead text-center">
                    Something has went wrong while resetting your password, please contact our support team at <a href="mailto:support@bidable.in">support@bidable.in</a>
                  </p>
                </CardBody>
                <CardFooter className="p-4">
                  <Row>
                    <Col xs="6" className="text-left">
                      <Link className="btn btn-outline-primary" tag="button" to={`/${type}/login`}>Login</Link>
                    </Col>
                    <Col xs="6" className="text-right">
                      <Link className="btn btn-outline-primary" tag="button" to={`/${type}/register`}>Register</Link>
                    </Col>
                  </Row>
                </CardFooter>
              </Card>
            </Col>
          </Row>
        </Container>
      </AuthLayout>
    );
  }
}

export default ResetPassword;
