import React from 'react';
import { Button, Modal, ModalHeader, ModalBody } from 'reactstrap';

import Spinner from "./Spinner"

class NotifyModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: props.modal
    };
  }

  close = () => {
    this.setState({ modal: false });
    if (this.props.onClose) this.props.onClose()
  }

  toggle = () => {
    if (this.props.toggle) {
      this.setState({ modal: !this.state.modal })
      this.props.toggle()
    }
  }

  cancel = () => {
    this.setState({ modal: false })
    if (this.props.onCancel) this.props.onCancel()
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.modal !== this.props.modal){
      this.setState({modal:nextProps.modal});
    }
  }

  timeOutModal = duration => {
    setTimeout(() => this.close(), duration)
  }

  render() {
    const { title, body, showConfirmBtn, showCancelBtn, confirmBtnText, confirmBtnClass, cancelBtnText, timeout, loading } = this.props;
    return (
      <div style={{ margin: 5, display: "inline-block" }}>
        <Modal isOpen={this.state.modal} toggle={this.toggle} className="modal--login">
          <ModalHeader />
          <ModalBody>
            <div className="modal-account-holder">
              <div className="modal-account__item" style={{ flexBasis: "100%", textAlign: 'center'}}>
                <h3>{title}{loading && <Spinner theme='dark'/>}</h3>
                <div className="mb-4">{body}</div>
                {showConfirmBtn && <Button color={confirmBtnClass} disabled={loading} onClick={this.close}>{confirmBtnText || 'OK'}</Button>}
                {showCancelBtn && <Button className="ml-2" disabled={loading} onClick={this.cancel}>{cancelBtnText || 'Cancel'}</Button>}
                {!showConfirmBtn && this.state.modal && this.timeOutModal(timeout || 3000)}
              </div>
            </div>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

export default NotifyModal;
