import React, {Component} from 'react';
import { Field } from 'redux-form';
import { InputGroup, InputGroupAddon, InputGroupText, FormGroup, Label, Input, FormFeedback } from 'reactstrap';
import Flatpickr from 'react-flatpickr';
import Select from 'react-select';

import {generateTimeIncrement, generateEndList} from "./Time";

export const select = ({input, label, placeholder, multi, disabled, options, id, meta: { touched, error }})  => (
  <FormGroup>
    <Label htmlFor={id}>{label}</Label>
    <Select
      value={input.value}
      onChange={(value) => input.onChange(value)}
      onBlur={() => input.onBlur(input.value)}
      options={options}
      disabled={disabled}
      multi={multi}
    />
    <FormFeedback style={{ display: touched && error ? 'block' : 'none' }} className="help-block">{error}</FormFeedback>
  </FormGroup>
);

export const renderField = ({input, label, placeholder, prepend, disabled, type, icon, meta: { asyncValidating, touched, error, warning }}) => (
  <div className="row mb-3" style={{ alignItems: "flex-start" }}>
    <div className="col-md-3 p-0 render-field-label"><h6>{label}</h6></div>
    <div className="col-md-9">
      <InputGroup className={asyncValidating ? 'async-validating' : ''}>
        <InputGroupAddon addonType="prepend">
          <InputGroupText>
            <i className={icon}/>
          </InputGroupText>
        </InputGroupAddon>
        {prepend && prepend()}
        <input {...input} disabled={disabled} placeholder={placeholder} type={type} className="form-control" />
      </InputGroup>
      {touched &&
      ((error && <div className="error">{error}</div>) ||
        (warning && <span>{warning}</span>))}
    </div>
  </div>
)

export const regularField = ({
    input, label, placeholder, disabled, type, options, id, hideLabel,
    meta: { asyncValidating, touched, error }
  }) => (
  <FormGroup>
    {!hideLabel && <Label htmlFor={id}>{label}</Label>}
    {
      type === "select"
        ? <select
            {...input}
            className={`form-control form-control-warning ${asyncValidating ? 'async-validating' : ''}`}
            disabled={disabled}
            id={id}
          >
          <option/>
          {options.map((val, i) => <option key={i}>{val}</option>)}
          </select>
        : <Input
          {...input}
          className={`form-control-warning ${asyncValidating ? 'async-validating' : ''}`}
          disabled={disabled}
          placeholder={placeholder}
          type={type}
          id={id}
        />
    }

    <FormFeedback style={{ display: touched && error ? 'block' : 'none' }} className="help-block">{error}</FormFeedback>
  </FormGroup>
)

export const renderDateTimePicker = ({
    input, range, time, label, placeholder, disabled, id, hideLabel,
    meta: { touched, error }
}) => {
  let dateOptions = {
    altInput: true,
    altFormat: "F j, Y",
    dateFormat: "Y-m-d",
    minDate: "today"
  }
  let customOptions = time
    ? {enableTime: true, noCalendar: true, dateFormat: "H:i"}
    : range ? {...dateOptions, mode: 'range', showMonths: 2} : {...dateOptions}

  return (
    <FormGroup>
      {!hideLabel && <Label htmlFor={id}>{label}</Label>}
      <Flatpickr
        {...input}
        options={customOptions}
        className="form-control form-control-warning picker-input"
        disabled={disabled}
        placeholder={placeholder}
        id={id}
      />
      <FormFeedback style={{ display: touched && error ? 'block' : 'none' }} className="help-block">{error}</FormFeedback>
    </FormGroup>
  )
}

export class RenderTimePicker extends Component {
  state = { hasError1: false, hasError2: false, error1: null, error2: null }

  setHasError1 = (hasError1, error1) => this.setState({ hasError1, error1 })
  setHasError2 = (hasError2, error2) => this.setState({ hasError2, error2 })

  render() {
    const { label, startValue, firstLabel, secondLabel, firstFieldName, secondFieldName, hideLabel, onlyStart } = this.props;
    const { hasError1, hasError2, error1, error2 } = this.state;
    let timeListStart = firstTwelveHours(generateTimeIncrement(30))
    let timeListEnd = startValue ? generateEndList(timeListStart, startValue) : timeListStart
    return (
      <FormGroup>
        {!hideLabel && <Label>{label}</Label>}
        <div className="react-time-range form-control form-control-warning picker-input">
          <Field
            name={firstFieldName}
            label={firstLabel || "Start"}
            component={timePicker}
            timeList={timeListStart}
            setHasError={this.setHasError1}
          />
          {!onlyStart && <Field
            name={secondFieldName}
            label={secondLabel || "End"}
            component={timePicker}
            timeList={timeListEnd}
            setHasError={this.setHasError2}
          />}
        </div>
        {(hasError1 || hasError2) && <FormFeedback style={{ display: 'block'}} className="help-block">{error1 || error2}</FormFeedback>}
      </FormGroup>
    )
  }
}


function firstTwelveHours(timeArray) {
  return timeArray.splice(0, timeArray.length/2 + 1)
}

export const timePicker = ({ input, label, timeList, setHasError, meta: { touched, error } }) => {

  if (touched && error) setHasError(true, error)
  else setHasError(false)

  return (
    <div className="component">
      <span className="label">{label}</span>
      <select
        {...input}
      >
        {timeList.map((resp, index) => (
          [
            <option key="start" value="" disabled hidden>--:--</option>,
            <option key={index} value={resp.value} disabled={!resp.active}>
              {`${resp.hh}:${resp.mm} ${resp.period}`}
            </option>
          ]
        ))}
      </select>
    </div>
  )
}
