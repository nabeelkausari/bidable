import moment from 'moment';
import { some } from 'lodash';
export const devUrl = 'http://localhost:5000';
export const isProd = process.env.NODE_ENV === "production";
export const isDev = process.env.NODE_ENV === "development";

export const HOST = isProd ? 'https://www.bidable.in' :  devUrl;
export const getAuthHeaders = () => {
  let jwt = localStorage.getItem('token');
  return { headers: { authorization: jwt }}
}

export const isMobile = window.innerWidth < 992;

export const regularTime = date => moment(date).format("hh:mm:ss A")
export const regularDateTime = date => moment(date).format("DD/MM/YYYY LT")
export const readableDate = date => moment(date).format("ddd ll")
export const shortDate = date => moment(date).format("Do MMM")
export const readableTime = time => moment(time, "hmm").format("LT")
export const price = number => Number(number).toLocaleString("en-IN") + " INR"

export const hasPaidEMD = (lot, user) => {
  if (!user || !user.buyer) return true;
  return some(lot.emdList, ['user', user._id])
}

export const isBeforeStart = lot => {
  let { auctionDate, auctionTime } = lot;
  if (!auctionDate || !auctionTime) return false;
  let start = getDateTime(auctionDate, auctionTime.start)
  return start.isAfter();
}

export const getDateTime = (date, time) => {
  let hours = parseInt(time.substr(0, 2));
  let minutes = parseInt(time.substr(2));
  let dateStr = moment(date).format("YYYY-MM-DD");
  return moment(dateStr).hours(hours).minutes(minutes)
}

export const anyLotLive = lots => {
  return some(lots, lot => {
    let { auctionDate, auctionTime } = lot;
    let end = getDateTime(auctionDate, auctionTime.end)
    return end.isAfter()
  })
}

export const getAuctionLiveStatus = ({ lots }) => {
  if (!lots || !lots.length) return;
  const { auctionDate, auctionTime } = lots[0];
  if (!auctionDate || !auctionTime) return;

  let start = getDateTime(auctionDate, auctionTime.start);
  if (start.isAfter()) {
    return 'upcoming'
  } else if (anyLotLive(lots)) {
    return 'live'
  } else {
    return 'past'
  }
}

export const getLiveStatus = payload => {
  const { auctionDate, auctionTime } = payload;
  if (!auctionDate || !auctionTime) return;

  let start = getDateTime(auctionDate, auctionTime.start);
  let end = getDateTime(auctionDate, auctionTime.end);
  if (start.isAfter()) {
    return 'upcoming'
  } else if (end.isAfter()) {
    return 'live'
  } else {
    return 'past'
  }
}

export const getAuctionFilter = () => {
  let key = encodeURI('filter');
  let value;

  let queries = document.location.search.substr(1).split('&');

  let i=queries.length; let x; while(i--) {
    x = queries[i].split('=');

    if (x[0]===key) {
      value = encodeURI(x[1]);
      break;
    }
  }
  return value;
}

export const emailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const sellerExplainer = "If you are an industry and want to SELL your industrial surplus, scraps and recyclables through conducting an e-auction, then this is for you.";
export const buyerExplainer = "If you are a business and want to BUY industrial surplus, scraps and recyclables through bidding in online auctions, then this is for you.";
