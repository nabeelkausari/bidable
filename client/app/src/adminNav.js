export default {
  items: [
    {
      name: 'Dashboard',
      url: '/admin/dashboard',
      icon: 'icon-speedometer'
    },
    {
      title: true,
      name: 'Auctions',
      wrapper: {            // optional wrapper object
        element: '',        // required valid HTML5 element tag
        attributes: {}        // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
      },
      class: ''             // optional class names space delimited list for title item ex: "text-center"
    },
    {
      name: 'Auctions To Review',
      url: '/admin/auctions',
      // icon: 'icon-drop',
    },
    {
      name: 'Approved Auctions',
      url: '/admin/auctions-approved',
      // icon: 'icon-drop',
    },
    {
      title: true,
      name: 'EMDs',
      wrapper: {
        element: '',
        attributes: {}
        },
      class: ''
    },
    {
      name: 'EMDs to Review',
      url: '/admin/emds',
      // icon: 'icon-envelope',
    },
    {
      name: 'Rejected EMDs',
      url: '/admin/emds-rejected',
      // icon: 'icon-list',
    },
    {
      name: 'Approved EMDs',
      url: '/admin/emds-approved',
      // icon: 'icon-list',
    },
    {
      name: 'Cleared EMDs',
      url: '/admin/emds-cleared',
      // icon: 'icon-list',
    },
    {
      title: true,
      name: 'Marketing',
      wrapper: {
        element: '',
        attributes: {}
        },
      class: ''
    },
    {
      name: 'Send Mail',
      url: '/admin/send-mail',
      icon: 'icon-envelope',
    },
    {
      name: 'Mail List',
      url: '/admin/mails',
      icon: 'icon-list',
    },
    //   {
    //     title: true,
    //     name: 'My Account',
    //     wrapper: {
    //       element: '',
    //       attributes: {},
    //     },
    //   },
    //   {
    //     name: 'Personal Details',
    //     icon: 'icon-user',
    //     children: [
    //       {
    //         name: 'Identity',
    //         url: '/admin/identity',
    //         // icon: 'icon-puzzle',
    //       },
    //       {
    //         name: 'Renewal',
    //         url: '/admin/renewal',
    //         // icon: 'icon-puzzle',
    //       },
    //       {
    //         name: 'Certificate Enroll',
    //         url: '/admin/certificate-enroll',
    //         // icon: 'icon-puzzle',
    //       },
    //       {
    //         name: 'GST Registration',
    //         url: '/admin/gst-registration',
    //         // icon: 'icon-puzzle',
    //       },
    //       {
    //         name: 'Change Password',
    //         url: '/admin/change-password',
    //         // icon: 'icon-puzzle',
    //       },
    //       {
    //         name: 'Update Profile',
    //         url: '/admin/updated-profile',
    //         // icon: 'icon-puzzle',
    //       }
    //     ],
    //   }
  ],
};
