import {
  BuyerDashboard,
  AllAuctions,
  AuctionDetail,
  LotDetail,
  PastAuctions,
  BuyerMyBids,
  BuyerSales,
  SubmitEMD,
  SelectLotEMD,
  MyEMDs,
  BankDetails,
  BuyerMyQuotations
} from './components';

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/buyer/dashboard', name: 'Buyer Dashboard', component: BuyerDashboard },
  { exact: true, path: '/buyer/auctions', name: 'Active Auctions List', component: AllAuctions },
  { exact: true, path: '/buyer/past-auctions', name: 'Past Auctions', component: PastAuctions },
  { exact: true, path: '/buyer/auctions/:auctionId', name: 'Auction Detail', component: AuctionDetail },
  { exact: true, path: '/buyer/auctions/:auctionId/:lotNo', name: 'Lot Detail', component: LotDetail },
  { exact: true, path: '/buyer/past-auctions/:auctionId', name: 'Past Auction Detail', component: AuctionDetail },
  { exact: true, path: '/buyer/past-auctions/:auctionId/:lotNo', name: 'Past Lot Detail', component: LotDetail },
  { exact: true, path: '/buyer/my-bids', name: 'My Bids', component: BuyerMyBids },
  { exact: true, path: '/buyer/my-quotations', name: 'My Quotations', component: BuyerMyQuotations },
  { exact: true, path: '/buyer/sales', name: 'My Sales', component: BuyerSales },
  { exact: true, path: '/buyer/submit-emd', name: 'Submit EMD', component: SelectLotEMD },
  { exact: true, path: '/buyer/submit-emd/:lotId', name: 'Submit EMD', component: SubmitEMD },
  { exact: true, path: '/buyer/my-emd-list', name: 'My EMD List', component: MyEMDs },
  { exact: true, path: '/buyer/bank-details', name: 'Bank Details', component: BankDetails },
];

export default routes;
