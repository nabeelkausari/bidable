import React, { Component } from 'react';
import { connect } from 'react-redux';

export default ComposedComponent => {
  class Authentication extends Component {

    checkBuyerAuth = () => {
      let token = localStorage.getItem('token');
      let buyer = localStorage.getItem('type') === 'buyer';
      if (!token) {
        this.props.history.push('/buyer/login')
      } else if (!buyer) {
        this.props.history.push('/seller/login')
      }
    }

    componentDidMount() {
      this.checkBuyerAuth()
    }

    componentDidUpdate() {
      this.checkBuyerAuth()
    }

    render() {
      return <ComposedComponent {...this.props} />
    }
  }

  return connect(null)(Authentication)
}

