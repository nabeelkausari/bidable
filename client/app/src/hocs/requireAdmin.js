import React, { Component } from 'react';
import { connect } from 'react-redux';

export default ComposedComponent => {
  class Authentication extends Component {

    checkAdminAuth = () => {
      let token = localStorage.getItem('token');
      let admin = localStorage.getItem('type') === 'admin';
      if (!token) {
        this.props.history.push('/admin/login')
      } else if (!admin) {
        this.props.history.push('/buyer/login')
      }
    }

    componentDidMount() {
      this.checkAdminAuth()
    }

    componentDidUpdate() {
      this.checkAdminAuth()
    }

    render() {
      return <ComposedComponent {...this.props} />
    }
  }

  function mapStateToProps(state) {
    return {...state.auth}
  }

  return connect(mapStateToProps)(Authentication)
}

