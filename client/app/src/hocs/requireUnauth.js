import React, { Component } from 'react';
import { connect } from 'react-redux';

export default ComposedComponent => {
  class UnAuth extends Component {

    checkUnAuth = () => {
      let token = localStorage.getItem('token');
      let type = localStorage.getItem('type');
      if (token && type) {
        this.props.history.push(`/${type}/dashboard`);
      }
    }

    componentDidMount() {
      this.checkUnAuth()
    }

    componentDidUpdate() {
      this.checkUnAuth()
    }

    render() {
      return <ComposedComponent {...this.props} />
    }
  }

  return connect(null)(UnAuth)
}

