import React, { Component } from 'react';
import { connect } from 'react-redux';

export default ComposedComponent => {
  class Authentication extends Component {

    checkSellerAuth = () => {
      let token = localStorage.getItem('token');
      let seller = localStorage.getItem('type') === 'seller';
      if (!token) {
        this.props.history.push('/seller/login')
      } else if (!seller) {
        this.props.history.push('/buyer/login')
      }
    }

    componentDidMount() {
      this.checkSellerAuth()
    }

    componentDidUpdate() {
      this.checkSellerAuth()
    }

    render() {
      return <ComposedComponent {...this.props} />
    }
  }

  return connect(null)(Authentication)
}

