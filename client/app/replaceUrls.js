var find = require("find-and-replace");

find
  .src('./build/index.html')
  .dest('./build/index.html')
  .replace({
    'static': 'app/static'
  })
  // fires when find and replace is finished and gives you the replaced text from either the file or the raw text
  .complete(function (txt) {
    console.log('Finished! Here is the completed text: ' + txt);
  })
  // add an error callback
  .error(function (err) {
    console.log("this is ERR", err);
  });
