import express from 'express';
import { createServer } from 'http';
import env from 'node-env-file';
import path from 'path';
import socketIO from 'socket.io';

import setupApis from './api';
import setupDb from './config/db';
import setupMiddlewares from './config/middlewares';
import setupPassport from './config/passport';
// import './services/mocks'
// import './mails/test';
// import setupCounters from './config/counters';
// setupCounters().then(() => console.log('counters checked'))

const prod = process.env.NODE_ENV === "production"
if (prod) {
  env('./conf.ini');
} else {
  env('./conf.dev.ini');
}
env('./.keys')
const port = parseInt(process.env.PORT, 10) || 5000

const app = express();
const server = createServer(app)
const io = socketIO(server)

setupDb();
// testDeploy();

setupPassport();
setupMiddlewares(app);
setupApis(app, io);

// app.use(express.static(path.join(__dirname + '/../client/build')));
//
// app.get('*', function(req, res) {
//   res.sendFile(path.join(__dirname + '/../client/build/index.html'));
// });

server.listen(port, err => {
  if (err) return console.log(err);
  console.log(`server listening on port ${port}`)
})
