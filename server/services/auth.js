import jwt from 'jwt-simple';
import shortid from 'shortid';

import User from '../models/User';
import constants from '../config/constants';
import { verificationMail, sendMail } from "../mails";

export const tokenForUser = user => {
  const timestamp = new Date().getTime();
  return jwt.encode({ sub: user.id, iat: timestamp }, constants.JWT_SECRET);
}

export const getMe = async (req, res) => {
  try {
    const decoded = jwt.decode(req.body.jwt, constants.JWT_SECRET);
    const user = await User.findById(decoded.sub)
      .select('-password -otp -createdAt -updatedAt -__v').exec();
    res.json({ user });
  } catch (error) {
    res.status(422).send({ error });
  }
}

export const signIn = async (req, res, next) => {
  let { email, firstName, lastName, confirmed, bonusClaimed, bonusApproved, hadTransacted, isAdmin } = req.user;
  let subject = 'You’ve just signed in to your Bidable account';
  let mailContent = {
    title: 'Login Success!',
    hiddenMessage: 'You’ve successfully logged in to your Bidable account just now.',
    body: `
<div>
    <p>You’ve successfully logged in to your Bidable account just now.</p>
    <p>We send this email as a security measure each time you login to your account. If this was not you, please contact us immediately from <a href="mailto:contact@bidable.in">contact@bidable.in</a> - so we can look into it.</p>
</div>`
  }
  sendMail({ email, subject, mailContent });
  res.send({ jwt: tokenForUser(req.user), user: { email, firstName, lastName, confirmed, bonusClaimed, bonusApproved, hadTransacted, isAdmin } });
}

export const signUp = async (req, res, next) => {
  try {
    const { email, password, username, firstName, lastName, referralUsername } = req.body;
    if (!email || !password || !username) {
      let missingField;
      if (!email) missingField = 'Email';
      else if (!username) missingField = 'Username';
      else if (!password) missingField = 'Password';

      return res.status(422).send({ error: `${missingField} is required`});
    }
    const existingUser = await User.findOne({ email }).exec();
    if (existingUser) {
      return res.status(422).send({ error: 'Email already exists'});
    }
    const existingUsername = await User.findOne({ username }).exec();
    if (existingUsername) {
      return res.status(422).send({ error: 'Username already exists'});
    }
    const user = await new User({ email, password, username, firstName, lastName }).save();
    let jwt = tokenForUser(user);
    verificationMail({ username, email, jwt });
    res.json({ jwt, user: { email, username, firstName, lastName } })
  } catch (err) {
    return next(err)
  }
}

export const confirmEmail = async (req, res) => {
  try {
    const decoded = jwt.decode(req.params.jwt, constants.JWT_SECRET);
    const user = await User.findById(decoded.sub).select('email confirmed seller buyer').exec();
    await user.update({ confirmed: true });
    if (user.seller) {
      res.redirect('/app/seller/email-confirmed')
    } else {
      res.redirect('/app/buyer/email-confirmed')
    }
  } catch (error) {
    res.redirect('/app/email-error')
  }
}

export const requestPasswordReset = async (req, res) => {
  try {
    const { email } = req.body;
    const user = await User.findOne({ email }).exec();

    if (user) {
      let jwt = tokenForUser(user);
      let type = user.seller ? 'seller' : 'buyer';
      let link = `https://bidable.in/app/${type}/reset-password/${jwt}`
      let subject = 'Password reset confirmation';
      let mailContent = {
        title: 'Confirm Password Reset!',
        hiddenMessage: subject,
        body: `
        <div>
            <p>We have received a request to reset your password. If this was you then press the button below.</p>
            <p>This link will expire in 30 minutes.</p>
            <div style="margin: 15px auto; text-align: center;">
               <a href="${link}" target="_blank" style="font-size: 20px; font-family: 'Rubik', Helvetica, Arial, sans-serif; color: #000; text-decoration: none; padding: 15px 25px; border-radius: 2px; background-color: #fc0; display: inline-block;">Confirm Password Reset</a>
            </div>
            <p>If this was not you, please contact us immediately from <a href="mailto:support@bidable.in">support@bidable.in</a> - so we can look into it.</p>
        </div>`
      }
      sendMail({ email, subject, mailContent });
    }

    res.json({message: `Password reset confirmation mail sent to ${email}`})
  } catch (error) {
    res.json({message: `Something went wrong while requesting password reset`, error })
  }
}

export const resetPassword = async (req, res) => {
  try {
    const { password, token } = req.body;
    const decoded = jwt.decode(token, constants.JWT_SECRET);
    const tokenExpired = 30 < Math.floor(((Date.now() - decoded.iat) / 1000) / 60)
    if (tokenExpired) throw "Password link expired"
    const user = await User.findById(decoded.sub).select('password').exec();
    await user.update({ password: user._hashPassword(password) });
    res.json({message: `Password reset successfully`})
  } catch (error) {
    res.json({message: `Something went wrong while resetting the password`, error })
  }
}


export const forgotPasswordOTP = async (req, res) => {
  try {
    const { email } = req.body;
    if (!email) throw "Please provide the email address"
    const user = await User.findOne({ email }).exec();

    if (user) {
      let otp = shortid.generate();
      await user.update({ otp: { text: otp, createdOn: Date.now() }})
      let subject = 'Password Reset OTP';
      let mailContent = {
        title: 'Password Reset OTP!',
        hiddenMessage: subject,
        body: `
        <div>
            <p>We have received a request to reset your password. Enter this OTP to proceed.</p>
            <p>This OTP will expire in 15 minutes.</p>
            <div style="margin: 15px auto; text-align: center;">
               <p style="font-size: 24px; font-weight: bold; letter-spacing: 2px; font-family: 'Rubik', Helvetica, Arial, sans-serif; color: #fff; text-decoration: none; padding: 15px 25px; background-color: #333; display: inline-block;">
                 ${otp}
                </p>
            </div>
            <p>If this was not you, please contact us immediately from <a href="mailto:support@bidable.in">support@bidable.in</a> - so we can look into it.</p>
        </div>`
      }
      sendMail({ email, subject, mailContent });
    }

    res.json({message: `Password reset OTP sent to ${email}`})
  } catch (error) {
    res.json({message: `Something went wrong while sending password reset OTP`, error })
  }
}

export const resetPasswordOTP = async (req, res) => {
  try {
    const { password, otp } = req.body;
    if (!password || !otp) throw "Incomplete parameters";
    const [user] = await User.find({ "otp.text": otp }).exec();
    if (!user) throw "Invalid OTP";
    if (!user.otp) throw "OTP not generated yet";
    const otpExpired = 15 < Math.floor(((Date.now() - new Date(user.otp.createdOn)) / 1000) / 60)
    if (otpExpired) throw "OTP expired"
    await user.update({ password: user._hashPassword(password), otp: {} });
    res.json({message: `Password reset successfully`})
  } catch (error) {
    res.json({message: `Something went wrong while resetting the password`, error })
  }
}
