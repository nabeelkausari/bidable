import Auction from "../../models/Auction"
import moment from 'moment';
import Lot from "../../models/Lot"
import Bid from "../../models/Bid"
import Quotation from "../../models/Quotation"
import Sale from "../../models/Sale"

export const approveAuction = async (req, res) => {
  try {
    const { auctionDate, auctionStartsFrom, emds } = req.body
    if (!emds) throw "EMDs required";
    if (!auctionDate) throw "Auction date required";
    if (!auctionStartsFrom) throw "Start time required";

    let auctionInMinutes = 30 // 30 minutes each
    let totalAuctionDuration = req.auction.lots.length * auctionInMinutes
    let auctionUpdate = {
      auctionDate,
      auctionTime: {
        start: moment(auctionStartsFrom).format("HHmm"),
        end: moment(auctionStartsFrom).add(totalAuctionDuration, 'm').format("HHmm")
      },
      status: 'live'
    }

    req.auction.lots.map(async (lotItem, i) => {
      let lot = await Lot.findById(lotItem._id).exec();
      let lotUpdate = {
        lotNo: i + 1,
        auctionDate,
        auctionTime: {
          start: moment(auctionStartsFrom).add(i * auctionInMinutes, 'm').format("HHmm"),
          end: moment(auctionStartsFrom).add((i + 1) * auctionInMinutes, 'm').format("HHmm")
        },
        highestBid: {},
        quotations: [],
        emdAmount: emds[lotItem._id]
      }

      // add timings, lotNo, EMDs to lots plus reset highestBid
      await lot.update(lotUpdate).exec()
    })

    // delete all bids or quotations or sales (if any)
    await Sale.deleteMany({ auction: req.auction._id })
    await Bid.deleteMany({ auction: req.auction._id })
    await Quotation.deleteMany({ auction: req.auction._id })

    // update auction
    await req.auction.update(auctionUpdate).exec();

    res.json({ done: true });
  } catch (error) {
    res.status(422).send({ error });
  }
}

export const redraftAuction = async (req, res) => {
  try {
    await req.auction.update({ status: 'draft' }).exec();
    res.json({ done: true });
  } catch (error) {
    res.status(422).send({ error });
  }
}

export const cancelAuction = async (req, res) => {
  try {
    await req.auction.update({ status: 'review' }).exec();
    res.json({ done: true });
  } catch (error) {
    res.status(422).send({ error });
  }
}

export const getAuctionsToReview = async (req, res, next) => {
  try {
    let auctions = await Auction.find({status: 'review'})
      .populate({
        path: 'lots',
        select: 'product.name auctionDate auctionTime lotNo emdList',
        populate: {
          path: 'emdList',
          select: 'user'
        }
      })
      .exec();
    res.json(auctions)
  } catch (error) {
    res.status(422).send({ error });
  }
}
