import { getUsers } from './users';
import { addCategory } from "./categories"
import {requireAdmin, requireAdminLogin} from '../../config/passport';
import {approveAuction, cancelAuction, redraftAuction} from "./auctions";
import {adminLogin} from "./auth"
import { sendMarketingMail, getMails } from "./marketing";
import {getAuctionsToReview} from "./auctions"
import {getAuction} from "../buyer/auctions"
import { getEMDs, rejectEMD, refundEMD, withholdEMD, approveEMD, emdId } from "./emd";

export default (server, baseUrl) => {
  // admin routes
  let preText = baseUrl + '/admin';
  server.post(preText + '/login', requireAdminLogin, adminLogin)

  server.get(preText + '/getUsers', requireAdmin, getUsers)
  server.post(preText + '/categories', requireAdmin, addCategory)

  server.post(preText + '/mails', requireAdmin, sendMarketingMail)
  server.get(preText + '/mails', requireAdmin, getMails)

  server.get(preText + '/auctions', requireAdmin, getAuctionsToReview)
  server.get(`${preText}/auctions/:auctionId`, requireAdmin, getAuction)
  server.put(preText + '/auctions/:auctionId/approve', requireAdmin, approveAuction)
  server.put(preText + '/auctions/:auctionId/redraft', requireAdmin, redraftAuction)
  server.put(preText + '/auctions/:auctionId/cancel', requireAdmin, cancelAuction)

  server.get(preText + '/emds', requireAdmin, getEMDs)
  server.put(preText + '/emds/:emdId/approve', requireAdmin, approveEMD)
  server.put(preText + '/emds/:emdId/reject', requireAdmin, rejectEMD)
  server.put(preText + '/emds/:emdId/refund', requireAdmin, refundEMD)
  server.put(preText + '/emds/:emdId/withhold', requireAdmin, withholdEMD)

  server.param('emdId', emdId);
}

