import { some } from 'lodash';

import EMD from "../../models/EMD"

export const getEMDs = async (req, res, next) => {
  try {
    let { filter } = req.query;
    let queries;
    switch (filter) {
      case "rejected":
        queries = { approval: "rejected" }
        break;
      case "approved":
        queries = { approval: "success", status: "paid" }
        break;
      case "cleared":
        queries = { status: { $in: ["withheld", "refunded"]} }
        break;
      default:
        queries = { approval: "pending" }
        break;
    }
    let EMDs = await EMD.find(queries)
      .populate({
        path: 'lot',
        populate: {
          path: 'auction',
          select: 'auctionId'
        }
      })
      .exec();
    res.json(EMDs)
  } catch (error) {
    res.status(422).send({ error });
  }
}

export const rejectEMD = async (req, res, next) => {
  try {
    let EMD = await req.emd.update({ approval: 'rejected', rejectNote: req.body.rejectNote }).exec()
    res.json(EMD)
  } catch (error) {
    res.status(422).send({ error });
  }
}

export const approveEMD = async (req, res, next) => {
  try {
    let EMD = await req.emd.update({ approval: 'success', status: 'paid' }).exec()
    res.json(EMD)
  } catch (error) {
    res.status(422).send({ error });
  }
}

export const refundEMD = async (req, res, next) => {
  try {
    let EMD = await req.emd.update({ status: 'refunded' }).exec()
    res.json(EMD)
  } catch (error) {
    res.status(422).send({ error });
  }
}

export const withholdEMD = async (req, res, next) => {
  try {
    let EMD = await req.emd.update({ status: 'withheld' }).exec()
    res.json(EMD)
  } catch (error) {
    res.status(422).send({ error });
  }
}

export const emdId = async (req, res, next, _id) => {
  try {
    req.emd = await EMD.findOne({ _id })
      .populate({
        path: 'lot',
        populate: {
          path: 'auction',
          select: 'auctionId'
        }
      })
      .select('-__v')
      .exec();
    next();
  } catch (err) {
    next(err);
  }
};
