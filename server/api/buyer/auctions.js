import Auction from "../../models/Auction"
import {getAuctionLiveStatus, getLiveStatus} from "../../config/constants"

export const liveAuctionId = async (req, res, next, _id) => {
  try {
    req.auction = await Auction.findOne({ _id, status: 'live' })
      .populate({
        path: 'lots',
        populate: {
          path: 'emdList',
          select: 'user'
        }
      })
      .select('-__v')
      .exec();
    next();
  } catch (err) {
    next(err);
  }
};

export const getAuction = async (req, res) => {
  try {
    let auction = {
      ...req.auction._doc,
      liveStatus: getAuctionLiveStatus(req.auction._doc),
      lots: req.auction._doc.lots.map(lot => ({
        ...lot._doc,
        liveStatus: getLiveStatus(lot._doc),
      }))
    }
    res.json(auction)
  } catch (error) {
    res.status(422).send({ error });
  }
};

export const getAuctions = async (req, res, next) => {
  try {
    let response = await Auction.find({status: 'live'})
      .populate({
        path: 'lots',
        select: 'product.name auctionDate auctionTime lotNo emdList',
        populate: {
          path: 'emdList',
          select: 'user'
        }
      })
      .exec();

    let auctions = response.map(auction => ({
      ...auction._doc,
      liveStatus: getAuctionLiveStatus(auction._doc),
      lots: auction._doc.lots.map(lot => ({
        ...lot._doc,
        liveStatus: getLiveStatus(lot._doc),
      }))
    }))

    res.json(auctions)
  } catch (error) {
    res.status(422).send({ error });
  }
}
