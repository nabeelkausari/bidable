import Bank from "../../models/Bank"

export const submitBankDetails = async (req, res, next) => {
  try {
    const { name, account, code, branch, address } = req.body
    if (!name) throw 'Please enter bank name'
    if (!account) throw 'Please enter account number'
    if (!code) throw 'Please enter IFSC code'
    if (!branch) throw 'Please enter branch'
    if (!address) throw 'Please enter bank address'

    // check whether user has submitted the bank details
    const bankSubmitted = await Bank.findOne({ user: req.user.id }).exec();
    if (bankSubmitted) throw "user has already submitted the bank details";

    // create a new EMD
    const bank = await new Bank({ ...req.body, user: req.user.id }).save()

    res.json(bank)
  } catch (error) {
    res.status(422).send({ error });
  }
}

export const getBankDetails = async (req, res, next) => {
  try {
    let bankDetails = await Bank.findOne({user: req.user.id}).exec();
    res.json(bankDetails)
  } catch (error) {
    res.status(422).send({ error });
  }
}
