import moment from 'moment';
import { find } from 'lodash';
import {Decimal} from 'decimal.js';

import Lot from "../../models/Lot";
import Bid from "../../models/Bid";
import {getDateTime, getLiveStatus, secondsLeft} from "../../config/constants";
import Sale from "../../models/Sale"

export const createBid = async (req, res, next, io) => {
  try {
    let { lotId, bidPrice } = req.body;
    if (!lotId) throw "Lot Id is required"
    if (!bidPrice) throw "Bid price is required"
    let lot = await Lot.findById(lotId).exec();
    if (!lot) throw "Lot is required"
    let highestBid =  lot.highestBid && lot.highestBid.price;

    // check whether bid is created within the lot's auction time
    let startTime = getDateTime(lot.auctionDate, lot.auctionTime.start)
    let endTime = getDateTime(lot.auctionDate, lot.auctionTime.end)

    if (startTime.isAfter()) throw "Auction has not started yet"
    if (endTime.isBefore()) throw "Auction concluded"

    // first bid
    if (!highestBid) {
      if (Number(bidPrice) !== Number(lot.startingPrice)) {
        throw "First bid should be equal to starting price"
      }
    } else {
      // Other bids
      const hb = Decimal(highestBid);
      const bp = Decimal(bidPrice)
      if (!bp.equals(hb.add(Number(lot.minimumIncrement)))) {
        throw "Your bid should be equal to the given increment value"
      }
    }

    // Create Bid
    let bid = await new Bid({
      price: bidPrice,
      lot: lot._id,
      auction: lot.auction,
      bidder: req.user.id
    }).save()

    let lotUpdates = {
      highestBid: { price: bidPrice, id: bid._id },
      auctionTime: extendGracePeriod(endTime, lot.auctionTime)
    }

    // Update lot's highest price
    await lot.update(lotUpdates).exec()

    await createOrUpdateSale(req.user.id, {...lot._doc, ...lotUpdates});

    let { bidder, createdAt, price, _id } = bid;
    // Broadcast event to connected clients
    io.emit('bidCreated', {
      lotUpdates ,
      lotId: lot._id,
      bidUpdates: { bidder, createdAt, price, _id }
    })

    res.json(bid)
  } catch (error) {
    res.status(422).send({ error });
  }
}

const createOrUpdateSale = async (buyer, lot) => {
  let sale = await Sale.findOne({ lot: lot._id }).exec();
  if (sale) {
    await sale.update({ buyer }).exec()
  } else {
    await new Sale({
      buyer,
      seller: lot.creator,
      auction: lot.auction,
      lot: lot._id,
      bid: lot.highestBid.id,
    }).save()
  }
}

const extendGracePeriod = (endTime, auctionTime) => {
  let endDiff = endTime.diff(moment(), 's');
  let end = moment().add(3, 'm').format('HHmm');

  if (endDiff < 180 && endDiff >= 0) {
    if (!auctionTime.extended) {
      return {
        ...auctionTime,
        end,
        extended: true,
        extendedFrom: auctionTime.end
      }
    } else {
      return {
        ...auctionTime,
        end
      }
    }
  } else {
    return { ...auctionTime }
  }
}

export const getLotById = async (req, res) => {
  try {
    res.json(req.lot)
  } catch (error) {
    res.status(422).send({ error });
  }
};


export const getLot = async (req, res) => {
  try {
    const { lotNo } = req.params;
    const { auction } = req;
    let lot = find(auction._doc.lots, lot => {
      return parseInt(lot._doc.lotNo) === parseInt(lotNo)
    })
    res.json({
      ...lot._doc,
      liveStatus: getLiveStatus(lot._doc),
      secondsLeft: secondsLeft(lot._doc),
      auction: {
        _id: auction._id,
        auctionId: auction.auctionId,
      }
    })
  } catch (error) {
    res.status(422).send({ error });
  }
};
