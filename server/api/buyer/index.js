import {buyerLogin, buyerRegister, submitBusiness} from './auth';
import {requireBuyerLogin, requireBuyer, requireAuth} from '../../config/passport';
import {liveAuctionId, getAuction, getAuctions} from "./auctions"
import {createBid, getLot, getLotById} from "./lots"
import {getLotBids, getMyBids} from "./bids"
import {getSaleIntimations} from "./sales"
import {getMyEMDs, submitEMD, deleteEMD} from "./emd"
import {getBankDetails, submitBankDetails} from "./bank"
import { getStats } from "./dashboard"
import { createQuotation, getMyQuotation, getMyQuotations } from "./quotations";

export default (server, baseUrl, io) => {
  let preText = baseUrl + '/buyer';
  server.post(preText + '/register', buyerRegister)
  server.post(preText + '/login', requireBuyerLogin, buyerLogin)
  server.post(preText + '/submit-business', requireBuyer, submitBusiness)
  server.get(preText + '/stats', requireBuyer, getStats)
  server.get(`${preText}/sales`, requireBuyer, getSaleIntimations)

  let auctionPreText = baseUrl + "/auctions"
  server.get(`${auctionPreText}`, requireAuth, getAuctions)
  server.get(`${auctionPreText}/:liveAuctionId`, requireAuth, getAuction)
  server.param('liveAuctionId', liveAuctionId);

  let lotPreText = baseUrl + "/lots"
  server.get(`${lotPreText}/:auctionId/:lotNo`, requireBuyer, getLot)
  server.get(`${baseUrl}/lotId/:lotId`, requireBuyer, getLotById)

  let bidPreText = baseUrl + "/bids"
  server.post(`${bidPreText}`, requireBuyer, (req, res, next) => createBid(req, res, next, io))
  server.get(`${bidPreText}`, requireBuyer, getMyBids)
  server.get(`${bidPreText}/:lotId`, requireBuyer, getLotBids)

  let emdPreText = baseUrl + "/emd"
  server.post(`${emdPreText}`, requireBuyer, submitEMD)
  server.delete(`${emdPreText}/:emdId`, requireBuyer, deleteEMD)
  server.get(`${emdPreText}`, requireBuyer, getMyEMDs)

  let bankPreText = baseUrl + "/bank"
  server.post(`${bankPreText}`, requireBuyer, submitBankDetails)
  server.get(`${bankPreText}`, requireBuyer, getBankDetails)

  server.get(`${baseUrl}/quotations`, requireBuyer, getMyQuotations)
  server.get(`${baseUrl}/quotations/:lotId`, requireBuyer, getMyQuotation)
  server.post(`${baseUrl}/quotations`, requireBuyer, createQuotation)

}
