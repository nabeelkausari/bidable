import { groupBy } from 'lodash'

import Bid from "../../models/Bid";
import Sale from "../../models/Sale";
import EMD from "../../models/EMD";

export const getStats = async (req, res, next) => {
  try {
    let [bidCount, saleCount, emdCount ] = await Promise.all([
      Bid.count({ bidder: req.user.id }),
      Sale.count({ buyer: req.user.id }),
      EMD.count({ user: req.user.id }),
    ])
    res.json({ bidCount, saleCount, emdCount })
  } catch (error) {
    res.status(422).send({ error });
  }
}
