import User from "../../models/User"
import { tokenForUser } from "../../services/auth";
import { verificationMail, sendMail } from "../../mails";
import Business from "../../models/Business"

export const buyerLogin = async (req, res, next) => {
  let { email, firstName, lastName, confirmed, isAdmin } = req.user;
//   let subject = 'You’ve just signed in to your Bidable account';
//   let mailContent = {
//     title: 'Login Success!',
//     hiddenMessage: 'You’ve successfully logged in to your Bidable account just now.',
//     body: `
// <div>
//     <p>You’ve successfully logged in to your Bidable account just now.</p>
//     <p>We send this email as a security measure each time you login to your account. If this was not you, please contact us immediately from <a href="mailto:contact@bidable.in">contact@bidable.in</a> - so we can look into it.</p>
// </div>`
//   }
//   sendMail({ email, subject, mailContent });
  res.send({ jwt: tokenForUser(req.user), user: { email, firstName, lastName, confirmed, isAdmin } });
}

export const buyerRegister = async (req, res, next) => {
  try {
    const { email, password, mobile, firstName, lastName } = req.body;
    if (!email || !password || !mobile) {
      let missingField;
      if (!email) missingField = 'Email';
      else if (!mobile) missingField = 'Mobile';
      else if (!password) missingField = 'Password';

      return res.status(422).send({ error: `${missingField} is required`});
    }
    const existingEmail = await User.findOne({ email }).exec();
    if (existingEmail) {
      return res.status(422).send({ error: 'Email already exists'});
    }
    const existingMobile = await User.findOne({ mobile }).exec();
    if (existingMobile) {
      return res.status(422).send({ error: 'Mobile Number already exists'});
    }
    const user = await new User({ email, password, mobile, firstName, lastName, buyer: true }).save();
    let jwt = tokenForUser(user);
    verificationMail({ email, jwt });
    res.json({ jwt, user: { email, mobile, firstName, lastName } })
  } catch (err) {
    return next(err)
  }
}


export const submitBusiness = async (req, res, next) => {
  try {
    const {name, categories, ...restBody} = req.body;
    if (!name || categories.length === 0) {
      let missingField;
      if (!name) missingField = 'Business Name';
      else if (categories.length === 0) missingField = 'Categories';

      return res.status(422).send({ error: `${missingField} is required`});
    }

    const user = await User.findOne({ _id: req.user.id }).exec();
    if (user.businessSubmitted) {
      return res.status(422).send({ error: 'User had already submitted business details'});
    }
    await new Business({ ...restBody, name, categories, user: req.user.id }).save();
    await user.update({ businessSubmitted: true });

    submitBusinessEmail(user.email);
    res.json({ businessSubmitted: true })
  } catch (err) {
    return next(err)
  }
}

const submitBusinessEmail = (email) => {
  let subject = 'Business Details Received';
  let message = 'We have received your company details. Our team is verifying the details. This process will be completed within 2 working days.'
  let mailContent = {
    title: 'Verification Pending!',
    hiddenMessage: message,
    body: `
<div>
    <p>${message}</p>
</div>`
  }
  sendMail({ email, subject, mailContent });
}
