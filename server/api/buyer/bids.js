import {groupBy, orderBy} from 'lodash';

import Lot from "../../models/Lot";
import Bid from "../../models/Bid";
import {getLiveStatus} from "../../config/constants"

export const getMyBids = async (req, res) => {
  try {
    let bids = await Bid.find({ bidder: req.user.id })
      .select('price lot createdAt bidder')
      .exec()

    let grouped = groupBy(bids, 'lot');

    let lots = await Lot.find({ '_id': { $in: Object.keys(grouped) }})
      .populate('auction', 'auctionId')
      .lean()
      .exec()
    let nestedLots = lots.map(lot => ({
      ...lot,
      liveStatus: getLiveStatus(lot),
      bids: orderBy(grouped[lot._id], 'createdAt', 'desc')
    }))

    res.json(nestedLots)
  } catch (error) {
    res.status(422).send({ error });
  }
};

export const getLotBids = async (req, res) => {
  try {
    let bids = await Bid.find({ lot: req.params.lotId })
      .select('price createdAt bidder')
      .sort({createdAt: -1})
      .limit(5)
      .exec()

    res.json(bids)
  } catch (error) {
    res.status(422).send({ error });
  }
};
