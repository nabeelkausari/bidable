import { some } from 'lodash';

import Lot from "../../models/Lot";
import { getDateTime } from "../../config/constants";
import EMD from "../../models/EMD"

export const submitEMD = async (req, res, next) => {
  try {
    const { lotId, refNo, amount, submitNote } = req.body;

    if (!lotId) throw "Lot Id is required";
    if (!refNo) throw "Reference Number is required";
    if (!amount) throw "Amount is required";

    // check whether auction has not started yet
    const lot = await Lot.findById(lotId).populate('emdList').exec();
    let startTime = getDateTime(lot.auctionDate, lot.auctionTime.start);
    if (startTime.isBefore()) throw "EMD payment time expired";

    // check whether user has already submitted emd for current lot
    let userAlreadySubmitted = some(lot.emdList, e => e.user.toString() === req.user.id)
    if (userAlreadySubmitted) throw "User has already submitted the EMD form for the selected auction and lot"

    // create a new EMD
    const emd = await new EMD({ lot: lotId, refNo, amount, submitNote, user: req.user.id }).save()

    // add EMD to Lot's document
    await lot.update({ emdList: [ ...lot.emdList, emd._id ]}).exec()

    res.json(emd)
  } catch (error) {
    res.status(422).send({ error });
  }
}

export const deleteEMD = async (req, res, next) => {
  try {
    const { approval, lot, _id } = req.emd;
    if (approval !== "rejected") throw "Not allowed"

    // check whether auction has not started yet
    const lotItem = await Lot.findById(lot).exec();

    // remove EMD from Lot's document
    await lotItem.update({ emdList: [
        ...lotItem.emdList.slice(0, lotItem.emdList.indexOf(_id.toString())),
        ...lotItem.emdList.slice(lotItem.emdList.indexOf(_id.toString()) + 1)
      ]}).exec()

    // remove EMD
    await req.emd.remove();

    res.json({ done: true })
  } catch (error) {
    res.status(422).send({ error });
  }
}

export const getMyEMDs = async (req, res, next) => {
  try {
    let myEMDs = await EMD.find({user: req.user.id})
      .populate({
        path: 'lot',
        populate: {
          path: 'auction',
          select: 'auctionId'
        }
      })
      .exec();
    res.json(myEMDs)
  } catch (error) {
    res.status(422).send({ error });
  }
}
