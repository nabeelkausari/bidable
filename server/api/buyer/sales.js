import {filter} from 'lodash';
import moment from 'moment';

import Sale from "../../models/Sale"
import {getDateTime} from "../../config/constants"

export const getSaleIntimations = async (req, res) => {
  try {
    let sales = await Sale.find({ buyer: req.user.id })
      .populate('auction', 'auctionId')
      .populate('lot')
      .exec()

    let completedSales = filter(sales, ({ lot }) => {
      const { end } = lot.auctionTime
      return moment(getDateTime(lot.auctionDate, end)).isBefore()
    })

    res.json(completedSales)
  } catch (error) {
    res.status(422).send({ error });
  }
};
