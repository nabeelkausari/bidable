import Quotation from "../../models/Quotation"
import Lot from "../../models/Lot"
import {getDateTime} from "../../config/constants"

export const createQuotation = async (req, res) => {
  try {
    let { quotePrice, lotId } = req.body;

    if (!quotePrice) throw "Quote price is required"
    if (!lotId) throw "Lot Id is required"
    let lot = await Lot.findById(lotId).exec();
    if (!lot) throw "Lot not found"
    let highestBid =  lot.highestBid && lot.highestBid.price;

    // check whether quote is created within the lot's auction time
    let startTime = getDateTime(lot.auctionDate, lot.auctionTime.start)
    let endTime = getDateTime(lot.auctionDate, lot.auctionTime.end)

    if (startTime.isAfter()) throw "Auction has not started yet"
    if (endTime.isBefore()) throw "Auction concluded"

    // first bid
    if (!highestBid) {
      if (Number(quotePrice) >= Number(lot.startingPrice)) {
        throw "Quotations can't be equal or more than starting price"
      }
    } else {
      throw "Bid has been placed, quotations won't be accepted now"
    }

    // Create or Update Quotation
    let previousQuotation = await Quotation.findOne({ quoter: req.user.id, lot: lot._id }).exec();
    let quotation;
    if (previousQuotation) {
      await previousQuotation.update({ price: quotePrice }).exec()
      quotation = {
        ...previousQuotation._doc,
        price: quotePrice
      }
    } else {
      quotation = await new Quotation({
        price: quotePrice,
        lot: lot._id,
        auction: lot.auction,
        quoter: req.user.id
      }).save()

      let lotUpdates = {
        quotations: [
          ...lot.quotations,
          quotation._id
        ]
      }
      await lot.update(lotUpdates).exec()
    }

    res.json(quotation)
  } catch (error) {
    res.status(422).send({ error });
  }
}

export const getMyQuotation = async (req, res) => {
  try {
    let { user, lot } = req;
    let quotation = await Quotation
      .findOne({ quoter: user.id, lot: lot._id })
      .exec()
    res.json(quotation)
  } catch (error) {
    res.status(422).send({ error });
  }
}

export const getMyQuotations = async (req, res) => {
  try {
    let { user } = req;
    let quotations = await Quotation
      .find({ quoter: user.id })
      .populate('auction lot')
      .exec()
    res.json(quotations)
  } catch (error) {
    res.status(422).send({ error });
  }
}
