import Referral from '../models/Referral';
import Wallet from '../models/Wallet';
import Bonus from '../models/Bonus';
import { requireAuth, requireAdmin } from '../config/passport';
import User from "../models/User"
import { claimReceivedMail, claimApprovedMail, referralSPOReceivedMail } from "../mails/bonus";

export default server => {
  server.post('/api/claimBonus', requireAuth, claimBonus)
  server.post('/api/confirmBonus', requireAdmin, confirmBonus)
  server.get('/api/claims', requireAdmin, getClaims)
}

const getClaims = async (req, res) => {
  try {
    const claims = await Bonus.find({})
      .populate('userId', 'email username bonusClaimed bonusApproved')
      .sort('-createdAt')
      .exec();
    res.json(claims.filter(claim => claim.userId !== null));
  } catch (error) {
    res.status(422).send({ error });
  }
}

const claimBonus = async (req, res) => {
  try {
    let { facebook, twitter, telegram } = req.body;
    const bonus = await new Bonus({ userId: req.user.id, facebook, twitter, telegram }).save();
    const user = await User.findOne({ _id: req.user.id}).exec();
    await user.update({ bonusClaimed: true });
    claimReceivedMail(user.email)
    res.json(bonus);
  } catch (error) {
    res.status(422).send({ error });
  }
}

const confirmBonus = async (req, res) => {
  try {
    let { userId } = req.body
    const referral = await Referral.findOne({ userId })
      .populate('parent', 'email')
      .populate('userId', 'email username')
      .exec();
    if (referral.parent) {
      await addSPOs(referral.parent._id, 200); // add SPO to parent wallet
      await recordSPOBonus(referral.parent._id, userId, 200)
      referralSPOReceivedMail(referral.parent.email, referral.userId.username)
    }
    await addSPOs(userId, 300); // add SPO to users wallet
    claimApprovedMail(referral.userId.email)

    const user = await User.findOne({ _id: userId }).exec();
    await user.update({ bonusApproved: true });
    res.json({ message: "Bonuses added successfully" });
  } catch (error) {
    res.status(422).send({ error });
  }
}

const addSPOs = async (userId, units) => {
  let wallet = await Wallet.findOne({ userId }).exec()
  await wallet.update({ spoReserved: wallet.spoReserved + units})
}

const recordSPOBonus = async (bonusReceiver, bonusFrom, units) => {
  let referral = await Referral.findOne({ userId: bonusReceiver }).exec();
  referral.children.forEach(child => {
    if (child.user.toString() === bonusFrom) {
      child.spoBonus += units
    }
  });
  await referral.update({ children: [...referral.children]})
}
