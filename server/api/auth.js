import { getMe, confirmEmail, requestPasswordReset, resetPassword, forgotPasswordOTP, resetPasswordOTP } from '../services/auth';
import User from '../models/User';
import {getCategories} from "./admin/categories"

export default (server, baseUrl) => {
  server.post(baseUrl + '/getMe', getMe)
  server.get(baseUrl + '/categories', getCategories)
  server.get(baseUrl + '/confirmEmail/:jwt', confirmEmail)

  server.post(baseUrl + '/requestResetPassword', requestPasswordReset)
  server.post(baseUrl + '/resetPassword', resetPassword)
  server.post(baseUrl + '/forgotPasswordOTP', forgotPasswordOTP)
  server.post(baseUrl + '/resetPasswordOTP', resetPasswordOTP)

  server.get(baseUrl + '/checkMobile', async (req, res) => {
    try {
      let user = await User.findOne({ mobile: req.query.mobile }).exec()
      let taken = user !== null;
      res.json({ taken })
    } catch (error) {
      res.send({ error })
    }
  })

  server.get(baseUrl + '/checkEmail', async (req, res) => {
    try {
      let user = await User.findOne({ email: req.query.email }).exec()
      let taken = user !== null;
      res.json({ taken })
    } catch (error) {
      res.send({ error })
    }
  })
}

