import Lot from "../../models/Lot"

export const getSellerQuotations = async (req, res) => {
  try {
    // find all lots where highest bid is not placed
    // populate quotations
    // order by highest price
    const lots = await Lot
      .find({ creator: req.user.id, 'highestBid.price': { $exists: false }})
      .populate('auction', 'auctionId')
      .populate('quotations', null, null, { sort: { 'price': -1 } })
      .exec();
    res.json(lots)
  } catch (error) {
    res.status(422).send({ error });
  }
}
