import {sellerLogin, sellerRegister, submitCompany} from './auth';
import {
  submitAuction,
  auctionId,
  createAuction,
  getMyAuctions, updateAuction, deleteAuction, getMyAuction
} from "./auctions";
import {createLot, deleteLot, lotId, updateLot} from "./lots";
import {requireSeller, requireSellerLogin} from '../../config/passport';
import {getStats} from "./dashboard"
import { getSellerSales } from "./sales";
import { getSellerQuotations } from "./quotations";

export default (server, baseUrl) => {
  let authPreText = baseUrl + '/seller';
  server.post(authPreText + '/register', sellerRegister)
  server.post(authPreText + '/login', requireSellerLogin, sellerLogin)
  server.post(authPreText + '/submit-company', requireSeller, submitCompany)
  server.get(authPreText + '/stats', requireSeller, getStats)

  let auctionPreText = baseUrl + '/auctions'
  server.post(`${auctionPreText}`, requireSeller, createAuction)

  server.put(`${auctionPreText}/:auctionId`, requireSeller, updateAuction)
  server.put(`${auctionPreText}/:auctionId/submit`, requireSeller, submitAuction)
  server.delete(`${auctionPreText}/:auctionId`, requireSeller, deleteAuction)

  server.get(`${auctionPreText}/mine`, requireSeller, getMyAuctions)
  server.get(`${auctionPreText}/mine/:auctionId`, requireSeller, getMyAuction)
  server.param('auctionId', auctionId);

  let lotPreText = baseUrl + '/lots'
  server.post(`${lotPreText}/:auctionId`, requireSeller, createLot)
  server.put(`${lotPreText}/:lotId`, requireSeller, updateLot)
  server.delete(`${lotPreText}/:lotId`, requireSeller, deleteLot)
  server.param('lotId', lotId);

  server.get(`${baseUrl}/seller/sales`, requireSeller, getSellerSales)
  server.get(`${baseUrl}/seller/quotations`, requireSeller, getSellerQuotations)

}
