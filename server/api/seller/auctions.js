import Auction from "../../models/Auction";
import Lot from "../../models/Lot";
import Company from "../../models/Company"

export const createAuction = async (req, res, next) => {
  try {
    let { inspectionDetails, preferredDate } = req.body;
    let creator = req.user.id;

    let company = await Company.findOne({user: creator}).exec();
    if (!company) throw "User has not submitted the company yet"

    let auction = await new Auction({ company: company.name, inspectionDetails, preferredDate, creator }).save();
    res.json(auction)
  } catch (error) {
    res.status(422).send({ error });
  }
}
export const updateAuction = async (req, res, next) => {
  try {
    if (req.user.id.toString() !== req.auction.creator.toString()) {
      throw new Error("Not authorized")
    }
    if (req.auction.status !== "draft") {
      throw new Error("Not allowed")
    }
    let auction = await req.auction.update(req.body).exec()
    res.json(auction)
  } catch (error) {
    res.status(422).send({ error });
  }
}
export const submitAuction = async (req, res, next) => {
  try {
    if (req.user.id.toString() !== req.auction.creator.toString()) {
      throw new Error("Not authorized")
    }
    if (req.auction.status !== "draft") {
      throw new Error("Not allowed")
    }
    if (req.auction.lots.length === 0) {
      throw new Error("No lots created")
    }
    let update = { status: 'review' };
    await req.auction.update(update).exec()
    res.json({ ...req.auction._doc, ...update })
  } catch (error) {
    res.status(422).send({ error });
  }
}

export const getMyAuctions = async (req, res, next) => {
  try {
    let auctions = await Auction.find({creator: req.user.id}).exec();
    res.json(auctions)
  } catch (error) {
    res.status(422).send({ error });
  }
}

export const deleteAuction = async (req, res, next) => {
  try {
    if (!req.auction) throw "Auction Id is required"
    if (req.user.id.toString() !== req.auction.creator.toString()){
      throw "Not authorized"
    }
    if (req.auction.status !== "draft") {
      throw "Not allowed"
    }
    await Lot.remove({
      '_id': { $in: req.auction.lots.map(lot => lot._id) }
    });
    let auction = await req.auction.remove()
    res.json(auction)
  } catch (error) {
    res.status(422).send({ error });
  }
}

export const auctionId = async (req, res, next, _id) => {
  try {
    req.auction = await Auction.findOne({ _id })
      .populate({
        path: 'lots',
        populate: {
          path: 'emdList'
        }
      })
      .select('-__v')
      .exec();
    next();
  } catch (err) {
    next(err);
  }
};

export const getMyAuction = async (req, res) => {
  try {
    if (req.user.id.toString() !== req.auction.creator.toString()){
      throw "Not authorized"
    }
    res.json(req.auction)
  } catch (error) {
    res.status(422).send({ error });
  }
};
