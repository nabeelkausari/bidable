import { groupBy } from 'lodash'

import Lot from "../../models/Lot";
import Auction from "../../models/Auction"

export const getStats = async (req, res, next) => {
  try {
    let [lotCount, auctionDraft, auctionReview, auctionLive ] = await Promise.all([
      Lot.count({ creator: req.user.id }),
      Auction.count({ creator: req.user.id, status: "draft" }),
      Auction.count({ creator: req.user.id, status: "review" }),
      Auction.count({ creator: req.user.id, status: "live" }),
    ])
    res.json({ lotCount, auctionDraft, auctionReview, auctionLive })
  } catch (error) {
    res.status(422).send({ error });
  }
}
