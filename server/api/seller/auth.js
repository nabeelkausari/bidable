import User from "../../models/User"
import Company from "../../models/Company"
import { tokenForUser } from "../../services/auth";
import { verificationMail, sendMail } from "../../mails";

export const sellerLogin = async (req, res, next) => {
  let { email, username, firstName, lastName, confirmed, isAdmin } = req.user;
//   let subject = 'You’ve just signed in to your Bidable account';
//   let mailContent = {
//     title: 'Login Success!',
//     hiddenMessage: 'You’ve successfully logged in to your Bidable account just now.',
//     body: `
// <div>
//     <p>You’ve successfully logged in to your Bidable account just now.</p>
//     <p>We send this email as a security measure each time you login to your account. If this was not you, please contact us immediately from <a href="mailto:contact@bidable.in">contact@bidable.in</a> - so we can look into it.</p>
// </div>`
//   }
//   sendMail({ email, subject, mailContent });
  res.send({ jwt: tokenForUser(req.user), user: { email, username, firstName, lastName, confirmed, isAdmin } });
}

export const submitCompany = async (req, res, next) => {
  try {
    const {
      representativeName, name, address, city, state, pinCode, phone, pan, gstin
    } = req.body;
    if (!name || !pan || !gstin) {
      let missingField;
      if (!name) missingField = 'Company Name';
      else if (!pan) missingField = 'PAN Number';
      else if (!gstin) missingField = 'GSTIN Number';

      return res.status(422).send({ error: `${missingField} is required`});
    }

    const user = await User.findOne({ _id: req.user.id }).exec();
    if (user.businessSubmitted) {
      return res.status(422).send({ error: 'User had already submitted business details'});
    }
    await new Company({ representativeName, name, address, city, state, pinCode, phone, pan, gstin, user: req.user.id }).save();
    await user.update({ businessSubmitted: true });

    submitCompanyEmail(user.email);
    res.json({ businessSubmitted: true })
  } catch (err) {
    return next(err)
  }
}

const submitCompanyEmail = (email) => {
  let subject = 'Company Details Received';
  let message = 'We have received your company details. Our team is verifying the details. This process will be completed within 2 working days.'
  let mailContent = {
    title: 'Verification Pending!',
    hiddenMessage: message,
    body: `
<div>
    <p>${message}</p>
</div>`
  }
  sendMail({ email, subject, mailContent });
}

export const sellerRegister = async (req, res, next) => {
  try {
    const { email, password, mobile, firstName, lastName } = req.body;
    if (!email || !password || !mobile) {
      let missingField;
      if (!email) missingField = 'Email';
      else if (!mobile) missingField = 'Mobile';
      else if (!password) missingField = 'Password';

      return res.status(422).send({ error: `${missingField} is required`});
    }
    const existingEmail = await User.findOne({ email }).exec();
    if (existingEmail) {
      return res.status(422).send({ error: 'Email already exists'});
    }
    const existingMobile = await User.findOne({ mobile }).exec();
    if (existingMobile) {
      return res.status(422).send({ error: 'Mobile number already registered'});
    }

    const user = await new User({ email, password, mobile, firstName, lastName, seller: true }).save();
    let jwt = tokenForUser(user);
    verificationMail({ email, jwt });
    res.json({ jwt, user: { email, mobile, firstName, lastName } })
  } catch (err) {
    return next(err)
  }
}
