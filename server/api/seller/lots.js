import Lot from "../../models/Lot";
import Auction from "../../models/Auction"
import Company from "../../models/Company"

export const createLot = async (req, res, next) => {
  try {
    if (!req.auction) throw "Auction Id is required"
    if (req.user.id.toString() !== req.auction.creator.toString()) {
      throw new Error("Not authorized")
    }
    if (req.auction.status !== "draft") {
      throw new Error("Not allowed")
    }

    let company = await Company.findOne({user: req.user.id}).exec();
    if (!company) throw "User has not submitted the company yet"

    let lot = await new Lot({
      ...req.body,
      auction: req.auction._id,
      creator: req.user.id,
      sellerName: company.name
    }).save();
    await addLotToAuction(req.auction._id, lot._id);
    res.json(lot)
  } catch (error) {
    res.status(422).send({ error });
  }
}

const addLotToAuction = async (auctionId, lotId) => {
  let auction = await Auction.findOne({ _id: auctionId })
  await auction.update({ lots: [...auction.lots, lotId] })
}

export const updateLot = async (req, res, next) => {
  try {
    if (!req.lot) throw "Lot Id is required"
    if (req.user.id.toString() !== req.lot.creator.toString()){
      throw "Not authorized"
    }
    if (req.lot.auction && req.lot.auction.status !== "draft") {
      throw new Error("Not allowed")
    }
    let lot = await req.lot.update(req.body).exec()
    res.json(lot)
  } catch (error) {
    res.status(422).send({ error });
  }
}

export const deleteLot = async (req, res, next) => {
  try {
    if (!req.lot) throw "Lot Id is required"
    if (req.user.id.toString() !== req.lot.creator.toString()){
      throw "Not authorized"
    }
    if (req.lot.auction && req.lot.auction.status !== "draft") {
      throw new Error("Not allowed")
    }
    let auction = await Auction.findOne({ _id: req.lot.auction });
    await auction.update({
      lots: [
        ...auction.lots.slice(0, auction.lots.indexOf(req.lot._id)),
        ...auction.lots.slice(auction.lots.indexOf(req.lot._id) + 1)
      ]
    })
    let lot = await req.lot.remove()
    res.json(lot)
  } catch (error) {
    res.status(422).send({ error });
  }
}

export const lotId = async (req, res, next, _id) => {
  try {
    req.lot = await Lot.findOne({ _id })
      .populate('auction')
      .populate('lots')
      .select('-__v')
      .exec();
    next();
  } catch (err) {
    next(err);
  }
};
