import authApi from './auth';
import adminApis from './admin/index';
import sellerApis from './seller/index';
import buyerApis from './buyer/index';

const baseUrl = '/api'

export default (app, io) => {
  authApi(app, baseUrl);
  adminApis(app, baseUrl);
  sellerApis(app, baseUrl);
  buyerApis(app, baseUrl, io);
}
