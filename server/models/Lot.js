import mongoose, { Schema } from 'mongoose';

const LotSchema = new Schema({
  creator: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  auction: {
    type: Schema.Types.ObjectId,
    ref: 'Auction'
  },
  sellerName: String,
  product: {
    location: String,
    name: String,
    description: String,
  },
  lotNo: Number,
  startingPrice: Number,
  bidPer: String,
  minimumIncrement: Number,
  emdAmount: Number,
  quantity: String,
  unit: String,
  highestBid: {
    price: Number,
    id: {
      type: Schema.Types.ObjectId,
      ref: 'Bid'
    }
  },
  pcbRequired: Boolean,
  serviceFee: Boolean,
  gst: Number,
  auctionDate: Date,
  auctionTime: {
    start: String,
    end: String,
    extended: Boolean,
    extendedFrom: String
  },
  emdList: [{
      type: Schema.Types.ObjectId,
      ref: 'EMD'
  }],
  quotations: [{
      type: Schema.Types.ObjectId,
      ref: 'Quotation'
  }]
}, { timestamps: true });

export default mongoose.model('Lot', LotSchema)
