import mongoose, { Schema } from 'mongoose';
import { hashSync, compare } from 'bcrypt-nodejs';
import Counter from './Counter';

const UserSchema = new Schema({
  userId: { type: String, unique: true },
  firstName: String,
  lastName: String,
  avatar: String,
  password: String,
  email: { type: String, unique: true },
  mobile: { type: String, unique: true },
  isAdmin: { type: Boolean, default: false },
  confirmed: { type: Boolean, default: false },
  businessVerified: { type: Boolean, default: false },
  businessSubmitted: { type: Boolean, default: false },
  canBid: { type: Boolean, default: false },
  seller: Boolean,
  buyer: Boolean,
  otp: {
    text: String,
    createdOn: Date
  },
  mailDetails: Object
}, { timestamps: true });

UserSchema.pre('save', function(next) {
  if (this.isModified('password')) {
    this.password = this._hashPassword(this.password);
  }

  let user = this;
  Counter.findOneAndUpdate({ id: 'userId' }, {$inc: { seq: 1 } }, function(error, counter)   {
    if(error)
      return next(error);
    let pre = user.seller ? "SBD" : "UBD";
    user.userId = pre + counter.seq;
    return next();
  });
})

UserSchema.methods = {
  _hashPassword(password) {
    return hashSync(password)
  },
  comparePassword (candidatePassword, callback) {
    compare(candidatePassword, this.password, function (err, isMatch) {
      if (err) return callback(err);

      return callback(null, isMatch);
    })
  }
}

export default mongoose.model('User', UserSchema);
