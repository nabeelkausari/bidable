import mongoose, { Schema } from 'mongoose';

const BidSchema = new Schema({
  bidder: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  price: Number,
  lot: {
    type: Schema.Types.ObjectId,
    ref: 'Lot'
  },
  auction: {
    type: Schema.Types.ObjectId,
    ref: 'Auction'
  },
}, { timestamps: true });

BidSchema.index({ price: 1, lot: 1 }, { unique: true });

export default mongoose.model('Bid', BidSchema)
