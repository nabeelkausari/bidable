import mongoose, { Schema } from 'mongoose';

const SaleSchema = new Schema({
  seller: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  buyer: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  auction: {
    type: Schema.Types.ObjectId,
    ref: 'Auction'
  },
  lot: {
    type: Schema.Types.ObjectId,
    ref: 'Lot'
  },
  bid: {
    type: Schema.Types.ObjectId,
    ref: 'Bid'
  },
  invoice: String,
  amount: Number,
  emdDifference: Number,
  balancePaid: {
    type: Boolean,
    default: false
  },
  pickup: {
    note: String,
    done: {type: Boolean, default: false}
  }
}, { timestamps: true });

export default mongoose.model('Sale', SaleSchema)
