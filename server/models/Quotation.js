import mongoose, { Schema } from 'mongoose';

const QuotationSchema = new Schema({
  quoter: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  price: Number,
  lot: {
    type: Schema.Types.ObjectId,
    ref: 'Lot'
  },
  auction: {
    type: Schema.Types.ObjectId,
    ref: 'Auction'
  },
  accepted: Boolean
}, { timestamps: true });

export default mongoose.model('Quotation', QuotationSchema)
