import mongoose, { Schema } from 'mongoose';

const EMDSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  refNo: {type: String },
  amount: {type: Number },
  toBank: {
    name: String,
    account: String,
    code: String
  },
  submitNote: String,
  approveNote: String,
  rejectNote: String,
  refundNote: String,
  withheldNote: String,
  lot: {
    type: Schema.Types.ObjectId,
    ref: 'Lot'
  },
  approval: { type: String, enum: [ 'pending', 'success', 'rejected'], default: 'pending' },
  status: { type: String, enum: [ 'paid', 'refunded', 'withheld'] }
}, { timestamps: true });

export default mongoose.model('EMD', EMDSchema)
