import mongoose, { Schema } from 'mongoose';

const CompanySchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  representativeName: String,
  name: String,
  address: String,
  city: String,
  state: String,
  pinCode: String,
  phone: String,
  pan: String,
  gstin: String,
}, { timestamps: true });

export default mongoose.model('Company', CompanySchema)
