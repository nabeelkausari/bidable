import mongoose, { Schema } from 'mongoose';

const MailSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  from: String,
  to: String,
  body: String,
  subject: String
}, { timestamps: true });

export default mongoose.model('Mail', MailSchema)
