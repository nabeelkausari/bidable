import mongoose, { Schema } from 'mongoose';
import Counter from './Counter';

const AuctionSchema = new Schema({
  auctionId: { type: String, unique: true },
  creator: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  company: String,
  inspectionDetails: {
    from: Date,
    to: Date,
    timings: [{
      from: String,
      to: String
    }],
    except: String
  },
  preferredDate: Date,
  auctionDate: Date,
  auctionTime: {
    start: String,
    end: String
  },
  lots: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Lot'
    }
  ],
  status: {type: String, enum: ['draft', 'review', 'live'], default: 'draft'}
}, { timestamps: true });

AuctionSchema.pre('save', function(next) {
  let auction = this;
  Counter.findOneAndUpdate({ id: 'auctionId' }, {$inc: { seq: 1 } }, function(error, counter)   {
    if(error)
      return next(error);
    auction.auctionId = "ABD" + counter.seq;
    return next();
  });
});

export default mongoose.model('Auction', AuctionSchema)
