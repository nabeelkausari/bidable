import mongoose, { Schema } from 'mongoose';

const RecordSchema = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  type: { type: String, enum: ['deposit', 'withdraw']},
  amount: Number,
  estimatedGas: Number,
  txnHash: String,
  gasUsed: Number,
  cumulativeGasUsed: Number
}, { timestamps: true });

export default mongoose.model('Record', RecordSchema)
