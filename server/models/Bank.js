import mongoose, { Schema } from 'mongoose';

const BankSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  name: String,
  account: String,
  code: String,
  branch: String,
  address: String
}, { timestamps: true });

export default mongoose.model('Bank', BankSchema)
