import passport from 'passport';

import User from '../models/User';
import constants from './constants';
import { Strategy, ExtractJwt } from 'passport-jwt';
import LocalStrategy from 'passport-local';

const localOptions = { usernameField: 'email' };
const localLogin = new LocalStrategy(localOptions, async (email, password, done) => {
  try {
    let user = await User.findOne({ email }).exec();
    if (!user) return done(null, false, { message: 'Incorrect username.' });
    if (!user.confirmed) return done(null, false, { message: 'Email not confirmed yet' });

    user.comparePassword(password, (err, isMatch) => {
      if (!isMatch) return done(null, false, { message: 'Incorrect password.' });

      return done(null, user);
    });

  } catch (err) {
    return done(err)
  }
})

const adminLogin = new LocalStrategy(localOptions, async (email, password, done) => {
  try {
    let user = await User.findOne({ email }).exec();
    if (!user) return done(null, false);
    if (!user.isAdmin) done(null, false)

    user.comparePassword(password, (err, isMatch) => {
      if (!isMatch) return done(null, false);

      return done(null, user);
    });

  } catch (err) {
    return done(err)
  }
})

const sellerLogin = new LocalStrategy(localOptions, async (email, password, done) => {
  try {
    let user = await User.findOne({ email }).exec();
    if (!user) return done(null, false, { message: 'Incorrect username.' });
    if (!user.confirmed) return done(null, false, { message: 'Email not confirmed yet' });
    if (!user.seller) return done(null, false, { message: 'User has not registered as seller' })

    user.comparePassword(password, (err, isMatch) => {
      if (!isMatch) return done(null, false, { message: 'Incorrect password'});

      return done(null, user);
    });

  } catch (err) {
    return done(err)
  }
})

const buyerLogin = new LocalStrategy(localOptions, async (email, password, done) => {
  try {
    let user = await User.findOne({ email }).exec();
    if (!user) return done(null, false);
    if (!user.confirmed) return done(null, false, { message: 'Email not confirmed yet' });
    if (!user.buyer) return done(null, false);

    user.comparePassword(password, (err, isMatch) => {
      if (!isMatch) return done(null, false);

      return done(null, user);
    });

  } catch (err) {
    return done(err)
  }
})


const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromHeader('authorization'),
  secretOrKey: constants.JWT_SECRET
};

// Create JWT strategy
const jwtLogin = new Strategy(jwtOptions, async (payload, done) => {
  try {
    const user = await User.findById(payload.sub).exec();
    if (user) done(null, user)
    else done(null, false)
  } catch (err) {
    return done(err, false)
  }
});

// Create Admin JWT strategy
const adminJWT = new Strategy(jwtOptions, async (payload, done) => {
  try {
    const user = await User.findById(payload.sub).exec();
    if (user.isAdmin) done(null, user)
    else done(null, false)
  } catch (err) {
    return done(err, false)
  }
});

// Create Seller JWT strategy
const sellerJWT = new Strategy(jwtOptions, async (payload, done) => {
  try {
    const user = await User.findById(payload.sub).exec();
    if (user.seller) done(null, user)
    else done(null, false)
  } catch (err) {
    return done(err, false)
  }
});

// Create Buyer JWT strategy
const buyerJWT = new Strategy(jwtOptions, async (payload, done) => {
  try {
    const user = await User.findById(payload.sub).exec();
    if (user.buyer) done(null, user)
    else done(null, false)
  } catch (err) {
    return done(err, false)
  }
});

export const requireAuth = passport.authenticate('jwt', { session: false })
export const requireSignin = passport.authenticate('local', { session: false })
export const requireAdmin = passport.authenticate('admin-jwt', { session: false })
export const requireAdminLogin = passport.authenticate('admin-login', { session: false })
export const requireSeller = passport.authenticate('seller-jwt', { session: false })
export const requireSellerLogin = passport.authenticate('seller-login', { session: false })
export const requireBuyer = passport.authenticate('buyer-jwt', { session: false })
export const requireBuyerLogin = passport.authenticate('buyer-login', { session: false })

export default () => {
  passport.use('jwt', jwtLogin);
  passport.use('local', localLogin);
  passport.use('admin-jwt', adminJWT);
  passport.use('admin-login', adminLogin);
  passport.use('seller-jwt', sellerJWT);
  passport.use('seller-login', sellerLogin);
  passport.use('buyer-jwt', buyerJWT);
  passport.use('buyer-login', buyerLogin);
}
